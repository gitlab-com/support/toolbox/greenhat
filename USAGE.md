# Usage

Place to track and add examples, tips, and other tricks

---

[[_TOC_]]

# CLI

## HotKeys

| Hotkey              | Description             |
| ------------------- | ----------------------- |
| Ctrl + U            | Clear                   |
| Ctrl + A            | Go to beginning         |
| Ctrl + E            | Go to End               |
| Ctrl + Left/Right   | Move left/right by word |
| Ctrl + D, Ctrl + Z  | Exit                    |
| Ctrl + C, Shift Tab | Up one module           |

# Log

## Filtering

Show just error messages from production logs

```
log filter --status=500 --slice=exception.message gitlab-rails/production_json.log
```

Sidekiq specifically down by project

```
log filter --job_status=fail --slice=error_class,error_message,meta.project --error_class=RepositoryUpdateMirrorWorker::UpdateError --slice=meta.project --uniq=meta.project --pluck=meta.project
```
