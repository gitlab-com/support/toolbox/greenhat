#!/usr/bin/env ruby

require 'action_view'
require 'active_support'
require 'active_support/core_ext'
require 'amazing_print'
require 'benchmark'
require 'dotenv'
require 'find'
require 'hash_dot'
require 'oj'
require 'pastel'
require 'pry'
require 'require_all'
require 'semantic'
require 'slim'
require 'tty-cursor'
require 'tty-pager'
require 'tty-progressbar'
require 'tty-prompt'
require 'tty-reader'
require 'tty-spinner'
require 'tty-table'
require 'tty-which'
# require 'warning' # Ruby 3
require 'ruby_native_statistics'
require 'gitlab_chronic_duration'

# Custom Gem
require 'teron'

# Oj Settings
Oj.default_options = { symbol_keys: true, mode: :compat }

# Amazing Print
AmazingPrint.defaults = {
  indent: -2,
  ruby19_syntax: true,
  index: false,
  sort_keys: true,
  sort_vars: true
}

# HashDot Instead of Struct
Hash.use_dot_syntax = true
Hash.hash_dot_use_default = true

# Load Required Files
require 'greenhat/version'
require 'greenhat/entrypoint'
require 'greenhat/motd'
require 'greenhat/cli'
require 'greenhat/archive'
require 'greenhat/host'
require 'greenhat/logbot'
require 'greenhat/settings'
require 'greenhat/color'

# Printer Helpers
require 'greenhat/paper/paper_helper'
require 'greenhat/paper/flag_helper'
require 'greenhat/paper'

# Formatters - Loads Required Files Automatically
require 'greenhat/thing/file_types'
require 'greenhat/thing/kind'
require 'greenhat/thing/history'

# Thing
require 'greenhat/thing/helpers'
require 'greenhat/thing/spinner'
require 'greenhat/thing'

# Shell - Loads Required Files Automatically
require 'greenhat/shell'

## TTY Shims
require 'greenhat/tty/line'
require 'greenhat/tty/reader'
require 'greenhat/tty/custom_line'
require 'greenhat/tty/columns'

# Warning.ignore(/The table size exceeds the currently set width/) # Ruby 3
