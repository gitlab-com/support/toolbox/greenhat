module GreenHat
  # CLI Helper
  module Shell
    # Logs
    module Gitlab
      def self.ls
        help
      end

      def self.help
        puts "\u2500".pastel(:cyan) * 20
        puts "#{'GitLab'.pastel(:yellow)} - Tanuki power"
        puts "\u2500".pastel(:cyan) * 20

        puts 'Command Summary'.pastel(:blue)

        puts '  services'.pastel(:green)
        puts '    Show services status and version'
        puts

        puts '  status'.pastel(:green)
        puts '    Show gitlab-ctl status'
        puts

        puts '  architecture'.pastel(:green)
        puts '    Show node responsibility. E.g. Webservice, Gitaly, and etc'
        puts

        puts '  version'.pastel(:green)
        puts '    Show GitLab software version'
        puts
      end

      def self.architecture
        results = Archive.all.map { |x| GitLab.identify_node(x) }

        GitLab.node_types.each do |entry|
          list = results.select { |x| (x.services & entry.pattern).any? }.map(&:host).join("\n")
          next if list.blank?

          puts entry.name.pastel(:bright_green)
          puts list
          puts
        end
      end

      def self.version
        Thing.where(type: 'gitlab/version-manifest.json').each do |file|
          next unless file.data

          puts file.friendly_name
          puts Semantic::Version.new(file.data.build_version).to_s.pastel(:yellow)
          puts
        end
      end

      # Print service info / shared report method
      def self.services(raw = {})
        _files, flags, _args = Args.parse(raw, [:truncate])

        results = {}

        Archive.all.each do |archive|
          result = GitLab.services(archive)
          next unless result

          # Generated Output
          results[archive.friendly_name.pastel(:blue)] = result
        end

        # Print
        ShellHelper.show(results, flags)
      end

      def self.status
        Thing.where(type: 'gitlab_status').each do |file|
          next unless file.data

          puts file.friendly_name

          file.data.each_value do |services|
            output = services.each_with_index.flat_map do |service, i|
              color = service.status == 'run' ? :green : :red

              pad = i.zero? ? 18 : 0

              [
                "#{service.status}:",
                service.name.ljust(pad).pastel(color),
                "#{service.pid_uptime};".ljust(pad)
              ]
            end

            puts output.join(' ')
          end

          puts
        end
      end
    end

    # -----
  end
end
