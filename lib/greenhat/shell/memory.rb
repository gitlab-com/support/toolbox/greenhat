module GreenHat
  # CLI Helper
  module Shell
    # Logs
    module Memory
      # Easy Show All
      def self.free
        ShellHelper.file_output GreenHat::Memory.free
      end

      # Pretty Show
      def self.show
        results = ShellHelper.file_process(GreenHat::Memory.free) do |file|
          list = [
            file.friendly_name
          ]

          file.data.each do |mem|
            list.push GreenHat::Memory.memory_row mem
          end

          list.push "\n"

          list
        end

        ShellHelper.show(results.flatten)
      end
    end
  end
end
