module GreenHat
  # Module for Argument Parsing
  # Variable Breakdown
  # Args:Array supplied or default values for entry looping action
  # Flags:Hash Key/Value filter Actionables
  # Files:Array/String are any non start `-` dash entries
  module Args
    def self.parse(raw, skip_flags = [])
      # Don't affect original object / Better deep clone?
      cmd = raw.clone

      # Handle Pipe Params
      pipe = nil
      if cmd.include?('|')
        list = cmd.split('|')
        cmd = list.shift
        pipe = list.map { |x| x.join(' ') }.join('|')
      end

      # Extract Files
      files = cmd.grep_v(/^-+([^=]+)/)

      # Collect and Naturalize Arguments
      args = cmd.flat_map { |arg| arg_scan(arg) }

      # Collect all Flags to Hash
      flags = cmd.flat_map { |flag| flag_scan(flag) }.to_h

      # Move Flags to Args
      arg_to_flag(flags, args)

      # Logic
      # And / Or
      flag_defaults(flags, skip_flags)

      # Set command includes
      flags[:pipe] = pipe if pipe

      [files, flags, args]
    end

    def self.flag_defaults(flags, skip_flags)
      # Update other user defaults
      Settings.default_log_flags(flags, skip_flags)

      # FastStats / Don't set default flags
      return if skip_flags.include? :logic

      # Default Logic
      if flags.key?(:or)
        flags.logic = :any?
        flags.delete(:or)
      else
        flags.logic = :all?
      end
    end

    # Move Valued Flags to arguments (--truncate=5)
    def self.arg_to_flag(flags, args)
      args.reject! do |arg|
        # Entries specifically to move to Args
        if arg_to_flag_list.include?(arg.field)
          flags[arg.field] = arg.value

          true
        # Special Flags -- Send Full Thing
        elsif arg_to_flag_raw_list.include?(arg.field)
          flags[arg.field] ||= []
          flags[arg.field].push arg

          true
          # Ignore Good Entries
        else
          false
        end
      end
    end

    # Arguments that Accept multiple options / Comma Delimted
    def self.arg_special_split
      %i[
        slice except uniq pluck sort archive stats exists transform
      ]
    end

    # Flags Anything that isn't sent as a key/filter
    def self.arg_to_flag_list
      %i[
        archive end except exists json limit pluck reverse round slice sort start total
        stats truncate uniq page time_zone table_style percentile interval percentile transform
      ]
    end

    # Don't parse the flag value, just give it to the flag
    def self.arg_to_flag_raw_list
      %i[
        stats_limit
      ]
    end

    # Arg Scan (Split -- values into keys)
    def self.arg_scan(arg)
      arg.scan(/^-+([^=]+)=(.*)/).map do |field, value|
        logic = :include?
        bang = false

        # Exclude Logic
        if field.include? '!'
          field.delete!('!')
          bang = true
        end

        # Numeric Magic Logic
        logic = :<= if field.include? '<'
        logic = :>= if field.include? '>'
        field.delete!('<')
        field.delete!('>')

        # Symbolize
        field = field.to_sym

        {
          field:,
          value: arg_normalize(field, value),
          bang:,
          logic:
        }
      end
    end

    # Collect All Flags
    def self.flag_scan(arg)
      arg.scan(/^-+([^=]+)$/).map do |field, _val|
        # Symbolize
        field = field.to_sym

        [field, flag_arg_defaults(field)]
      end
    end

    # Naturlalize Values, Manipluate Special Values
    def self.arg_normalize(field, value)
      # Special Comma Params With Symbol
      return value.split(',').map(&:to_sym) if arg_special_split.include?(field)

      # Integer Arguments
      return value.to_i if value.numeric?

      # Other Field Manipulation
      case field
      when :page then value == 'true'
      else

        # Default Original
        value
      end
    end

    # Arg Defaults
    def self.flag_arg_defaults(field)
      case field
      when :round then 2
      when :limit then (TTY::Screen.height / 3) - 3
      when :truncate then TTY::Screen.width * 4
      # when :page, :case, :exact then :true # Override Specials
      when *arg_special_split then []
      else
        true
      end
    end
  end
end
