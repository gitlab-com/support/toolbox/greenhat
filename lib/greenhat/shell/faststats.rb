module GreenHat
  # CLI Helper
  module Shell
    # Logs
    module Faststats
      # rubocop:disable Metrics/MethodLength)
      def self.help
        puts "\u2500".pastel(:cyan) * 25
        puts "Gimme #{'Performance Stats'.pastel(:yellow)}"
        puts "\u2500".pastel(:cyan) * 25

        puts 'General'.pastel(:blue)
        puts "  Any double dash arguments (e.g. #{'--color-output'.pastel(:green)}) are passed directly to fast-stats"
        puts "  See #{'`fast-stats --help`'.pastel(:bright_black)} for all available options"
        puts

        puts 'Common Options'.pastel(:blue)
        puts '  --raw'.pastel(:green)
        puts '    Do not use less/paging'
        puts '  --search'.pastel(:green)
        puts '     Case-insensitive search of controller/method/worker field'
        puts '  --sort'.pastel(:green)
        puts '     count,fail,max,median,min,p95,p99,rps,score'
        puts '  --limit'.pastel(:green)
        puts '     The number of rows to print'
        puts '  --verbose'.pastel(:green)
        puts '     Prints the component details of the maximum, P99, P95, and median events by total duration for each'
        puts

        puts 'Commands'.pastel(:blue)
        puts 'ls'.pastel(:green)
        puts '  List available files'
        puts '  Options'
        puts '    -a, --all, show all files including source'
        puts

        puts '<file names+>'.pastel(:green)
        puts '  Print any file names'
        puts '  Ex: `gitaly/current`'
        puts '  Ex: `gitlab-rails/api_json.log gitlab-rails/production_json.log --raw`'
        puts

        puts 'top'.pastel(:green)
        puts '  Print a summary of top items in a category'
        puts

        puts 'errors'.pastel(:green)
        puts '  Show summary of errors captured in log'
        puts

        puts 'Examples'.pastel(:blue)
        puts '  Default'.pastel(:bright_white)
        puts '    gitaly/current'
        puts '    --raw gitlab-rails/production_json.log'
        puts '    gitlab-rails/api_json.log --sort=count'
        puts '    --search=pipeline sidekiq/current'
        puts

        puts '  Top'.pastel(:bright_white)
        puts '    top gitaly/current'
        puts '    top --limit=5 sidekiq/current'
        puts

        puts '  Errors'.pastel(:bright_white)
        puts '    errors gitaly/current'
        puts '    errors gitaly/current --no-border'
      end
      # rubocop:enable Metrics/MethodLength)

      def self.faststats_installation
        puts "#{'Unable to find'.pastel(:red)} #{'fast-stats'.pastel(:blue)}"
        puts '  Release Downloads here'
        puts '    - https://gitlab.com/gitlab-com/support/toolbox/fast-stats/-/releases'.pastel(:yellow)
        puts ''
      end

      # List Files Helpers
      def self.list(args = [])
        unless TTY::Which.exist? 'fast-stats'
          faststats_installation
          return false
        end

        all = false
        all = true if args.include?('-a') || args.include?('--all')

        files = ShellHelper::Faststats.things

        # Sort
        files.sort_by!(&:name)

        # Short & Uniq
        files.uniq!(&:name) unless all

        # Print
        files.each do |log|
          if all
            puts "- #{log.friendly_name}"
          else
            puts "- #{log.name.pastel(:yellow)}"
          end
        end

        return unless all

        puts "\n#{'Other / Unknown'.pastel(:red)}"
        (Thing.all - files).each do |file|
          puts "- #{file.name.pastel(:yellow)}"
        end
      end

      def self.ls(args = [])
        list(args)
      end

      # Vanilla Fast Stats
      def self.default(raw, _other = nil)
        unless TTY::Which.exist? 'fast-stats'
          faststats_installation
          return false
        end

        files, flags, cmd = ShellHelper::Faststats.parse(raw)

        LogBot.debug('FastStats CMD', cmd) if ENV['DEBUG']

        # binding.pry

        # Ignore Unknown Errors
        results = ShellHelper.file_process(files) do |file|
          output = `fast-stats #{cmd} #{file.file} 2>&1`
          result = $CHILD_STATUS.success?

          next unless result

          [
            file.friendly_name,
            output.split("\n"),
            "\n"
          ]
        end

        ShellHelper.show(results.compact.flatten, flags)
      end

      # ========================================================================
      # ===== [ Fast Stats - Top ] ====================
      # -------------------------------------------------
      # Options:
      # --display, value,perc,both
      #   Show percentage of totals, actual durations, or both. Defaults to both.
      # --limit=X
      # --sort=count,fail,max,median,min,p95,p99,rps,score
      # ========================================================================
      def self.top(raw = [], internal = false)
        unless TTY::Which.exist? 'fast-stats'
          faststats_installation
          return false
        end

        files, flags, cmd = ShellHelper::Faststats.parse(raw)

        LogBot.debug('FastStats CMD', cmd) if ENV['DEBUG']

        results = ShellHelper.file_process(files) do |file|
          [
            file.friendly_name,
            `fast-stats top #{cmd} #{file.file}`.split("\n"),
            "\n"
          ]
        end

        # Quick exit for internal processing
        return results.flatten if internal

        ShellHelper.show(results.flatten, flags)
      end
      # ========================================================================

      # ========================================================================
      # ===== [ Fast Stats - Errors ] ====================
      # ========================================================================
      def self.errors(raw = [], internal = false)
        unless TTY::Which.exist? 'fast-stats'
          faststats_installation
          return false
        end

        # Add Color Output
        raw.push '--color-output' if Settings.settings.color?
        files, flags, cmd = ShellHelper::Faststats.parse(raw)

        LogBot.debug('FastStats CMD', cmd) if ENV['DEBUG']

        results = ShellHelper.file_process(files) do |file|
          [
            file.friendly_name,
            `fast-stats errors #{cmd} #{file.file}`.split("\n"),
            "\n"
          ]
        end

        # Quick exit for internal processing
        return results.flatten if internal

        ShellHelper.show(results.flatten, flags)
      end
      # ========================================================================
    end
  end
end

module GreenHat
  module ShellHelper
    # Hide from Commands
    module Faststats
      # Default Settings from arg parse that won't work
      def self.invalid_settings
        %i[page round truncate logic fuzzy_file_match]
      end

      def self.parse(raw)
        file_list, flags, args = Args.parse(raw, invalid_settings)
        cmd = args.map { |opt| "--#{opt.field}=#{opt.value}" }.join(' ')
        cmd += flags.map do |flag, value|
          # Don't Include Raw
          next if flag == :raw

          case value
          when true then "--#{flag}"
          when Array then "--#{flag}=#{value.join}"
          else
            "--#{flag}=#{value}"
          end
        end.join(' ')

        # Prepare Log List / Allow attempting of parsing everything
        file_list = if file_list == ['*']
                      Thing.all.map(&:name)
                    else
                      ShellHelper.prepare_list(file_list, ShellHelper::Faststats.things)
                    end

        # Convert to Things
        files = ShellHelper.find_things(file_list, fuzzy_file_match: true)

        [files, flags, cmd]
      end

      def self.files
        %w[
          production_json
          api_json
          gitaly/current
          sidekiq/current
        ]
      end

      def self.things
        Thing.all.select do |thing|
          files.any? { |x| thing.name.include? x }
        end
      end

      # =====================================
      # Internal Helpers
      # =====================================
      def self.faststats?
        TTY::Which.exist?('fast-stats')
      end

      def self.run(thing, cmd = '')
        # Blank return if cannot possibly run
        return {} unless thing || !faststats?

        Oj.load `fast-stats #{cmd} #{thing.file} --format=json`
      end
    end
  end
end
