# Reporting
require 'greenhat/reports/shared' # Methods for Shell and Builder
require 'greenhat/reports/internal_methods' # Class / Execution Only
require 'greenhat/reports/runner' # Class execution
require 'greenhat/reports/builder' # CLI Reports
require 'greenhat/reports/shell_helper'

module GreenHat
  # CLI Helper
  module Shell
    # Make `report` a default top layer command
    def self.report(_raw = ['default'])
      # Move
      Cli.move_shell Shell::Reports

      # Run
      Reports.default ['full']
    end

    # Logs
    module Reports
      def self.auto_complete(_list, word)
        matches = GreenHat::ShellHelper::Reports.auto_complete_list.select do |x|
          x.include? word
        end

        if matches.count > 1
          puts "#{'Report Options:'.pastel(:bright_blue)} #{matches.join(' ').pastel(:bright_green)}"

          Cli.common_substr(matches.map(&:to_s))
        elsif matches.count == 1
          matches.first
        end
      end

      # Easy Show All
      def self.ls(raw = [])
        filter, _flags, _args = Args.parse(raw)

        list = GreenHat::ShellHelper::Reports.list
        list.select! { |x| x.include? filter.first } unless filter.blank?

        list.each do |x|
          puts "- #{File.basename(x, '.rb').pastel(:yellow)}"
        end
      end

      def self.default(raw = [])
        list, flags, args = Args.parse(raw)

        # Collect Reporst to Run
        run_list = GreenHat::ShellHelper::Reports.list.select do |entry|
          list.include? File.basename(entry, '.rb')
        end

        run_list.uniq!

        run_list.each do |file|
          raw_args = list.reject { |x| file.include? x }
          GreenHat::ShellHelper::Reports.run(file:, args:, flags:, raw_args:)
        end
      end
    end
  end
end
