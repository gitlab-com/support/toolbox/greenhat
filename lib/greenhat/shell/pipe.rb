module GreenHat
  # Helper for piping arguments
  module Pipe
    def self.show(results, arg)
      uuid = SecureRandom.uuid # File Placeholder
      write(results, uuid)
      command(uuid, arg)
      delete(uuid)
    end

    # Final execution into pipe
    def self.command(uuid, arg)
      puts `cat "#{file(uuid)}" | #{arg}`
    end

    # File path helper
    def self.file(uuid)
      "#{$TMP}/#{uuid}.txt"
    end

    # Helper to write all into split lines for pipe
    def self.write(results, uuid)
      File.write(file(uuid), results.map(&:flatten).flatten.join("\n"))
    end

    # Clean up created file
    def self.delete(uuid)
      File.delete file(uuid)
    end
  end
end
