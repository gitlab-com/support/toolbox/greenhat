module GreenHat
  module ShellHelper
    # Helper to handle listing of files
    module List
      # List Files Helpers
      def self.list(raw = [], files)
        filter, flags, _args = Args.parse(raw)

        # Sort
        files.sort_by!(&:name)

        # Simplified vs Full. Full file name/path / or just file kinds
        all = flags.key?(:all) || flags.key?(:a)

        # Short & Uniq
        files.uniq!(&:name) unless all

        # Filter / Pattern
        files.select! { |f| filter.any? { |x| f.name.include? x } } unless filter.empty?

        # Print
        files.each do |log|
          if all
            puts "- #{log.friendly_name}"
          else
            puts "- #{log.name.pastel(:yellow)}"
          end
        end
      end

      # Unified Help
      def self.help
        puts '  ls'.pastel(:green)
        puts '    List available files'
        puts '    Options'.pastel(:cyan)
        puts '      -a, --all, show full file name/path including source'
        puts '      <string> filter available'
        puts '    Examples'.pastel(:cyan)
        puts '      ls -a rails'
        puts '      ls sys'
        puts
      end
      # ----
    end
  end
end
