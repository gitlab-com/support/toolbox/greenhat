# Deprecated
# TODO: Remove
# module GreenHat
#   # Deprecating search stuff
#   module ShellHelper
#     # Main Entry Point for Searching
#     # def self.search_start(log_list, filter_type, args, opts)
#     def self.search_start(files, flags, args)
#       # Convert to Things
#       logs = ShellHelper.find_things(files, flags)

#       logs.each_with_object({}) do |log, obj|
#         # Ignore Empty Results / No Thing
#         next if log&.data.blank?

#         obj[log.friendly_name] = ShellHelper.search(log.data, flags, args)

#         obj
#       end
#     end

#     # Generic Search Helper / String/Regex
#     def self.search(data, flags = {}, args = {})
#       results = data.clone.flatten.compact
#       results.select! do |row|
#         args.send(flags.logic) do |arg|
#           search_row(row, arg, flags)
#         end
#       end

#       # Strip Results if Slice is defined
#       results.map! { |row| row.slice(*flags[:slice]) } if flags[:slice]

#       # Strip Results if Except is defined
#       results.map! { |row| row.except(*flags[:except]) } if flags[:except]

#       # Remove Blank from either slice or except
#       results.reject!(&:empty?)

#       results
#     end

#     # Break out filter row logic into separate method
#     def self.search_row(row, arg, flags)
#       # Sensitivity Check / Check for Match
#       included = filter_row_entry(row.to_s, arg, flags)

#       # Pivot of off include vs exclude
#       if arg.bang
#         !included
#       else
#         included
#       end
#     end
#   end
# end
