module GreenHat
  # Common Helpers
  module ShellHelper
    # Use File Process for Output
    def self.file_output(files, flags = {})
      results = file_process(files) do |file|
        [
          file.friendly_name,
          file.output(false),
          "\n"
        ]
      end

      ShellHelper.show(results.flatten, flags)
    end

    def self.file_process(files, &block)
      files.map do |file|
        next if file.output(false).empty?

        block.call(file)
      end.flatten
    end

    # Pagination Helper
    def self.page(data)
      TTY::Pager.page do |pager|
        data.flatten.each do |output|
          pager.write("\n#{output}") # write line to the pager
        end
      end
    end

    # Show Data / Auto Paginate Helper
    def self.show(data, flags = {})
      # If Block of String
      if data.instance_of?(String)
        TTY::Pager.page data
        return true
      end

      # If raw just print out
      if flags[:raw]
        puts data.join("\n")
        return true
      end

      # Check if content needs to paged, or if auto_height is off
      if Page.skip?(flags, data)
        create_ledger(data, flags).each { |entry| puts entry.render }
        return true
      end

      # Create Jobs
      # Generate List of Formatted Output
      ledger = create_ledger(data, flags)

      # Start Thread
      # TODO: Confirm / Test or Remove
      unless flags.no_paper_thread
        thread = Thread.new do
          pending = ledger.clone
          loop do
            pending = pending.reject(&:done)
            break if pending.empty?

            pending.first.async
          end
        end
      end

      TTY::Pager.page do |pager|
        ledger.each do |paper|
          # New Line is only needed on pager.write
          pager.write "#{paper.render}\n"
        end
      end

      # Terminate
      thread.kill unless flags.no_paper_thread

      # -----------------
    end

    # Collect Class Objects for output
    def self.create_ledger(data, flags)
      data.map do |x|
        Paper.new(data: x, flags:)
      end
    end

    def self.puts(text = '')
      $stdout.puts text
    end

    # Entry Shower / Top Level
    # def self.entry_show(flags, entry, key = nil)
    #   LogBot.debug('Entry Show', entry.class) if ENV['DEBUG']
    #   case entry
    #   when Hash then render_table(entry, flags)
    #   when Float, Integer
    #     format_table_entry(flags, entry, key)
    #     # Ignore Special Formatting for Strings / Usually already formatted
    #   when String
    #     entry
    #   when Array
    #     render_array_table(entry, flags)
    #   else
    #     LogBot.warn('Shell Show', "Unknown #{entry.class}")
    #     nil
    #   end
    # end

    # Format Table Entries
    # def self.format_table_entry(flags, entry, key = nil)
    #   formatted_entry = case entry
    #                     # Rounding
    #                     when Float, Integer || entry.numeric?
    #                       flags.key?(:round) ? entry.to_f.round(flags.round).ai : entry.ai

    #                     # General Inspecting
    #                     when Hash then entry.ai(ruby19_syntax: true)

    #                     # Arrays often contain Hashes. Dangerous Recursive?
    #                     when Array
    #                       entry.map { |x| format_table_entry(flags, x) }.join("\n")
    #                     when Time
    #                       entry.to_s.pastel(:bright_white)

    #                     # Default String Formatting
    #                     else
    #                       StringColor.do(key, entry)
    #                     end

    #   # Stats truncation handled separately
    #   if flags[:truncate] && !flags[:stats]
    #     entry_truncate(formatted_entry, flags[:truncate])
    #   else
    #     formatted_entry
    #   end
    # rescue StandardError => e
    #   if ENV['DEBUG']
    #     LogBot.warn('Table Format Entry', message: e.message)
    #     ap e.backtrace
    #   end
    # end

    # def self.render_array_table(entry, flags)
    #   table_style = flags[:table_style]&.to_sym || :unicode
    #   header, rows = entry
    #   table = TTY::Table.new(header: header, rows: rows)

    #   LogBot.debug('Rendering Entries') if ENV['DEBUG']
    #   output = table.render(table_style, padding: [0, 1, 0, 1], multiline: true) do |renderer|
    #     renderer.border.style = :cyan
    #   end

    #   # Line breaks for basic tables
    #   output += "\n" if flags[:table_style] == 'basic'

    #   output
    # end

    # # Print the Table in a Nice way
    # def self.render_table(entry, flags)
    #   entry = entry.to_h { |k, v| [k, format_table_entry(flags, v, k)] }
    #   # Pre-format Entry

    #   table_style = flags[:table_style]&.to_sym || :unicode

    #   table = TTY::Table.new(header: entry.keys, rows: [entry], orientation: :vertical)

    #   LogBot.debug('Rendering Entries') if ENV['DEBUG']
    #   output = table.render(table_style, padding: [0, 1, 0, 1], multiline: true) do |renderer|
    #     renderer.border.style = :cyan
    #   end

    #   # Line breaks for basic tables
    #   output += "\n" if flags[:table_style] == 'basic'

    #   output
    # rescue StandardError => e
    #   if ENV['DEBUG']
    #     LogBot.warn('Table', message: e.message)
    #     ap e.backtrace
    #   end

    #   [
    #     entry.ai,
    #     ('_' * (TTY::Screen.width / 3)).pastel(:cyan),
    #     "\n"
    #   ].join("\n")
    # end

    # def self.render_table_entry(val, col_index, flags)
    #   return val.to_s unless col_index == 1

    #   format_table_entry(flags, val)
    # end

    # Internal Query Helper
    # query = 'gitlab-rails/application_json.log --message!="Cannot obtain an exclusive lease" --severity=error'
    # ShellHelper.filter_internal(query)
    def self.filter_internal(search = '')
      files, flags, args = Args.parse(Shellwords.split(search))
      flags[:combine] = true

      # Default to everything
      files = Thing.all.map(&:name) if files.empty?

      Query.start(files, flags, args)
    end

    def self.entry_truncate(entry, truncate)
      # Ignore if Truncation Off
      return entry if truncate.zero?

      # Only truncate large strings
      return entry unless entry.instance_of?(String) && entry.size > truncate

      # Include  '...' to indicate truncation
      "#{entry.to_s[0..truncate]} #{'...'.pastel(:bright_blue)}"
    end

    # Total Count Helper
    def self.total_count(results, flags)
      # Option to only return the count
      return results.count if flags[:total] == 'simple'

      output = {}
      output[:total] = results.count
      output[:duration] = Query.calculate_duration(results)

      # Sort / Get first and Last
      list = results.select(&:time).map(&:time)
      unless list.blank?
        output[:start] = list.first
        output[:end] = list.last
      end

      # Hide empty results
      output.reject! { |_k, v| v.blank? }

      [output]
    end

    # Table Printer Helper
    def self.fields_print(files)
      fields = ShellHelper.find_things(files).map(&:fields).flatten.uniq

      # truncate = (TTY::Screen.width - columns) / columns
      # fields.map! { |f| f[0..truncate] }
      puts ShellHelper.field_table(fields)

      # ShellHelper.fields_print(files)
    end

    # 5 Columns cause issues with resizing
    def self.field_table(list, columns: 4)
      return nil if list.empty?

      # Split list into groups based on columns
      groups = list.each_slice((list.size / columns.to_f).ceil).to_a

      # Max Width for Table
      truncate = (TTY::Screen.width - groups.count) / groups.count

      # Process Truncation
      groups.each do |group|
        group.map! { |f| f[0..truncate]&.to_sym }
      end

      table = TTY::Table.new do |t|
        loop do
          break if groups.all?(&:empty?)

          t << groups.map(&:shift)
        end
      end

      table.render(:unicode, padding: [0, 1, 0, 1], resize: false)
    end

    # Unified Files Interface
    def self.files(file_list, base_list = nil, flags = {})
      base_list ||= Thing.all

      # Prepare Log List
      file_list = prepare_list(file_list, base_list)

      # Convert to Things
      find_things(file_list, flags, base_list)
    end

    # Total Log List Manipulator
    def self.prepare_list(log_list, base_list = nil)
      base_list ||= Thing.list

      # Assume all
      log_list.push '*' if log_list.empty?

      # Map for All
      log_list = base_list.map(&:name) if log_list == ['*']

      log_list
    end

    # Fuzzy match for things / List used for processable (Hash Entries)
    def self.thing_list
      @thing_list ||= Thing.list.map(&:name)

      @thing_list
    end

    # Shortcut to see if any things exist
    def self.any_things?(file_list, flags = {}, base_list = nil)
      !find_things(file_list, flags, base_list).empty?
    end

    # Shortcut find things
    def self.find_things(file_list, flags = {}, base_list = nil)
      base_list ||= Thing.all

      things = file_list.uniq.flat_map do |file|
        # If Thing, Return Thing
        return file if file.instance_of?(Thing)

        if flags.fuzzy_file_match
          base_list.select { |x| x.name.include?(file) || x.type.include?(file) }
        else
          Thing.where name: file
        end
      end.uniq

      # Host / Archive
      things.select! { |x| x.archive? flags.archive } if flags.key?(:archive)

      things
    end

    # Number Helper
    # https://gitlab.com/zedtux/human_size_to_number/-/blob/master/lib/human_size_to_number/helper.rb
    def self.human_size_to_number(string)
      size, unit = string.scan(/(\d*\.?\d+)\s?(Bytes?|KB|MB|GB|TB)/i).first
      number = size.to_f

      number = case unit.downcase
               when 'byte', 'bytes'
                 number
               when 'kb'
                 number * 1024
               when 'mb'
                 number * 1024 * 1024
               when 'gb'
                 number * 1024 * 1024 * 1024
               when 'tb'
                 number * 1024 * 1024 * 1024 * 1024
               end
      number.round
    end

    # General Helper for `show`
    def self.common_opts
      puts 'Common Options'.pastel(:blue)
      puts '  --raw'.pastel(:green)
      puts '    Do not use less/paging'
      puts

      puts '  --archive'.pastel(:green)
      puts '    Limit to specific archive name (inclusive). Matching SOS tar.gz name'
      puts '    Ex: --archive=dev-gitlab_20210622154626, --archive=202106,202107'
      puts
    end
  end
end
