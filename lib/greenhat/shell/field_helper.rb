module GreenHat
  # Common Helpers
  module FieldHelper
    def self.fields_find(files, word, flags = {})
      fields = ShellHelper.find_things(files, flags).map(&:fields).flatten.uniq

      if word.blank?
        puts 'Possible Fields:'.pastel(:bright_blue)
        puts ShellHelper.field_table(fields)

        return [] # Empty Result
      end

      list_select(fields, word)
    end

    def self.list_select(list, word)
      list.select! { |x| x[/^#{Regexp.escape(word)}/] }
    end

    def self.filter_flags(word)
      if word.blank?
        puts 'Filter Options:'.pastel(:bright_blue)
        puts ShellHelper.field_table(filter_opts)
        puts

        return []
      end

      list_select(filter_opts, word)
    end

    def self.filter_opts
      %w[
        archive case combine end exact except exists json limit or page pluck
        raw reverse round slice sort start stats table_style text time_zone
        total truncate uniq percentile transform
      ]
    end

    def self.filter_auto_completes
      %w[
        except exists pluck slice sort stats uniq
      ]
    end

    def self.field_auto_complete?(word)
      return false if word.blank?

      filter_auto_completes.include? word.split('=', 2).first
    end

    def self.field_auto_complete(word, files, flags = {})
      # Prevent weird dupes
      return nil if word[-1] == ','

      # Command Manipulation
      cmd, fields = word.split('=', 2)
      complete = fields.split(',')[0..-2]
      auto = fields.split(',').last

      # Field Finder
      matches = fields_find(files, auto, flags).uniq

      if matches.count == 1
        "--#{cmd}=#{(complete + matches).join(',')}"
      elsif matches.count > 1
        puts "#{'Field Options:'.pastel(:bright_blue)} #{matches.join(' ').pastel(:bright_green)}"

        list = [Cli.common_substr(matches.map(&:to_s))]
        "--#{cmd}=#{(complete + list).join(',')}"
      end
    end
  end
end
