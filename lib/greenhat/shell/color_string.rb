module GreenHat
  module ShellHelper
    # Helper to colorize and make outtput easier to read
    module StringColor
      def self.do(key, entry)
        LogBot.debug('Unknown Format', entry.class) if ENV.fetch('DEBUG', nil) && !entry.instance_of?(String)

        # Other Helpful colorizers
        if pastel?(key)
          pastel(key, entry)
        else
          entry.to_s
        end
      end

      # Add Color?
      def self.pastel?(key)
        [:severity].any? key
      end

      # General Key/Value Coloring
      def self.pastel(key, value)
        case key
        when :severity then severity(value)
        else
          value.to_s
        end
      end
      # ----

      def self.severity(value)
        case value.to_s.downcase.to_sym
        when :debug then value.pastel(:blue)
        when :info then value.pastel(:cyan)
        when :warn then value.pastel(:yellow)
        when :fatal, :error then value.pastel(:bright_red)
        else
          value.to_s
        end
      end
    end
  end
end
