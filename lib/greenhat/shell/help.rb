module GreenHat
  # Root Level Shell / Splitting Help into its own file
  module Shell
    # rubocop:disable Layout/LineLength
    def self.help
      puts 'Quickstart'.pastel(:blue)
      puts "  Commands are organized by submodule: #{'log'.pastel(:blue)}, #{'cat'.pastel(:blue)}, #{'faststats'.pastel(:blue)}"
      puts "  Use #{'help'.pastel(:bright_blue)} for available commands in each module"
      puts

      puts '  Example Commands'
      puts '    log filter sidekiq/current'.pastel(:yellow)
      puts '    disk free'.pastel(:yellow)
      puts

      puts 'Top Level Commands'.pastel(:blue)
      puts '  report'.pastel(:green)
      puts '    Show summary report of SOS Report. OS, CPU, Memory, Disk, and etc'
      puts '      --raw, no pagination'
      puts '      --archive=<redis/archive>, filter by archive name'
      puts

      puts '  ps,df,netstat,free,uptime,uname'.pastel(:green)
      puts '    Show common files from archives / Emulate terminal commands'
      puts

      puts '  Noisy Output'.pastel(:green)
      puts "    Use #{'quiet'.pastel(:blue)} or to #{'debug'.pastel(:blue)} to toggle greenhat logging"
      puts

      cli_shortcuts

      puts "See #{'about'.pastel(:bright_blue)} for more details about GreenHat"
    end
    # rubocop:enable Layout/LineLength

    def self.cli_shortcuts
      puts "\u2500".pastel(:cyan) * 25
      puts 'Nav / Keyboard Shortcuts'.pastel(:blue)
      puts "\u2500".pastel(:cyan) * 25
      puts <<~BLOCK
        | Hotkey              | Description             |
        | ------------------- | ----------------------- |
        | Ctrl + U            | Clear Input             |
        | Ctrl + A            | Go to beginning         |
        | Ctrl + E            | Go to End               |
        | Ctrl + Left/Right   | Move left/right by word |
        | Ctrl + D, Ctrl + Z  | Exit                    |
        | Ctrl + C, Shift Tab | Up one module           |
      BLOCK
      puts
    end

    def self.about
      puts "\u2500".pastel(:cyan) * 20
      puts "About GreenHat #{GreenHat::VERSION}".pastel(:yellow)
      puts "\u2500".pastel(:cyan) * 20

      puts 'TLDR; Put in SOS reports, run commands, and find stuffs'.pastel(:green)
      puts

      puts <<~BLOCK
        General overview (OS, Memory, Disk, GitLab)
        #{'report'.pastel(:bright_cyan)}

        Log Searching
        #{'log filter sidekiq/current --job_status=done --sort=duration_s,db_duration_s --slice=duration_s,db_duration_s --reverse'.pastel(:bright_cyan)}

        Read File(s) across SOS archives
        #{'cat uptime'.pastel(:bright_cyan)} or #{'cat mount etc/fstab'.pastel(:bright_cyan)}

      BLOCK

      puts 'What it does / How it works'.pastel(:blue)
      puts
      puts <<~BLOCK
        GreenHat is a support utility to enhance troubleshooting with GitLabSOS Reports and log files. Make it easy to find stuff

        Supplied input files are staged, unpacked, identified, and normalized.
        This enables other utilities to automatically find and present data. (Faststats, report, and etc)

      BLOCK

      puts 'Commands and Submodules'.pastel(:blue)
      puts
      puts <<~BLOCK
        Greenhat is broken down into different "modules". Each module has its own commands. For example: log, cat, and faststats.
        You can "cd" into or execute commands directly against with their names.

        - Direct: #{'log filter sidekiq/current'.pastel(:cyan)}
        - Or within: First #{'log'.pastel(:cyan)}, then #{'filter sidekiq/current'.pastel(:cyan)}

        You can find the list of commands and submodules of each with #{'help'.pastel(:yellow)}

      BLOCK
    end
  end
end
