module GreenHat
  # Common Helpers
  module Platform
    def self.platform
      if @platform
        @platform
      else
        require 'tty-platform'
        @platform ||= TTY::Platform.new

      end
    end

    def self.open(url = 'http://localhost:4567/chart/time')
      cmd = if platform.mac?
              'open'
            elsif platform.linux? || platform.unix?
              'xdg-open'
            end

      # See if anything was detected -- ignore system call if nothing was
      if cmd.nil?
        puts "Unknown OS! #{RbConfig::CONFIG['arch']}"
        puts url
        return
      end

      # platform.windows?  # => false
      # platform.unix?     # => true
      # platform.linux?    # => false
      # platform.mac?      # => true
      system("#{cmd} '#{url}'")
    end
  end
end
