module GreenHat
  # CLI Helper
  module Shell
    # Logs
    module Process
      # Easy Show All
      # Filter --archive

      def self.ls
        help
      end

      def self.help
        puts "\u2500".pastel(:cyan) * 22
        puts "#{'Process'.pastel(:yellow)} -  ps helper"
        puts "\u2500".pastel(:cyan) * 22

        ShellHelper.common_opts

        puts 'Command Summary'.pastel(:blue)
        puts '  ps'.pastel(:green)
        puts '    Raw `ps`'
        puts
        puts '  filter'.pastel(:green)
        puts "    Key/Field Filtering.  See #{'filter_help'.pastel(:blue)}"
        puts '    Examples'
        puts '      filter --sort=mem --reverse'.pastel(:green)
        puts '      filter --user=gitlab'.pastel(:green)

        puts
      end

      def self.filter_help(raw = {})
        args, flags, _args = Args.parse(raw)

        ShellHelper.show(ShellHelper::Filter.help(args.first), flags)
      end

      def self.ps(raw = {})
        # Extract Args
        files_list, flags, _args = Args.parse(raw)

        # Collect Files
        files = ShellHelper.files(files_list, GreenHat::Ps.things, flags)

        # Output
        ShellHelper.file_output(files, flags)
      end

      def self.default(raw_list)
        filter(raw_list)
      end

      def self.filter(raw = {})
        # Argument Parsing
        files, flags, args = Args.parse(raw)

        # Prepare Log List
        file_list = ShellHelper.prepare_list(files, GreenHat::Ps.things)

        results = Query.start(file_list, flags, args)

        # Check Search Results
        if results.instance_of?(Hash) && results.values.flatten.empty?
          puts 'No results'.pastel(:red)
        else

          ShellHelper.show(results, flags)
        end
      end
    end
  end
end
