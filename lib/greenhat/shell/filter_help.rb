module GreenHat
  # CLI Helper
  module ShellHelper
    # Unify Filter / Filter Help
    # rubocop:disable Metrics/MethodLength,Layout/LineLength,Metrics/ModuleLength
    module Filter
      def self.help(args = '')
        output = if args.blank?
                   help_index.values.map { |x| x.push '' }
                 else
                   list = help_index.select do |k, _v|
                     k.to_s.include? args
                   end

                   list.values
                 end

        output.flatten(2)
      end

      def self.help_index
        {
          title: [
            "\u2500".pastel(:cyan) * 20,
            'Filter'.pastel(:yellow),
            "\u2500".pastel(:cyan) * 20,
            '',
            'Options'.pastel(:blue)
          ],

          raw: [
            '--raw'.pastel(:green),
            '  Disable formatting and page/less'
          ],

          page: [
            '--page'.pastel(:green),
            '  Specifically enable or disable paging',
            '  E.g. --page (default to true), --page=true, --page=false'
          ],

          round: ['--round'.pastel(:green),
                  '  Attempt to round all integers. Default: 2.',
                  '  E.g. --round, --round=3, --round=0'],

          limit: [

            '--limit'.pastel(:green),
            '  Limit total output lines. Disabled by default. Default without value is based on screen height',
            '  E.g. --limit, --limit=5'
          ],

          json: [
            '--json'.pastel(:green),
            '  Print output back into JSON'
          ],

          or: [
            '--or'.pastel(:green),
            '  Filters will use OR instead of AND (all match vs any match)'
          ],

          total: [
            '--total'.pastel(:green),
            '  Show total count, duration, start/end time for matching entries',
            '  Add `simple` to only return counts. e.g. --total=simple'
          ],

          fields: [
            '--fields'.pastel(:green),
            '  Print only Available fields for selected files'
          ],

          slice: [
            '--slice'.pastel(:green),
            '  Extract specific fields from entries (slice multiple with comma)',
            '  Ex: --slice=path or --slice=path,params'
          ],

          except: [
            '--except'.pastel(:green),
            '  Exclude specific fields (except multiple with comma)',
            '  Ex: --except=params --except=params,path'
          ],

          transform: [
            '--transform'.pastel(:green),
            '  Copy key to another key within results. Useful for queries that require (timestamp => time)',
            '  Available Transforms, to_i, default is to copy',
            '  Ex: --transform=timestamp,time',
            '      --transform=cpu,cpu,to_i'
          ],

          exists: [
            '--exists'.pastel(:green),
            '  Ensure field exists regardless of contents',
            '  Ex: --exists=params --exists=params,path'

          ],

          stats: [
            '--stats'.pastel(:green),
            '  Order/Count occurrances by field. Combine with `truncate` for key names',
            '  Ex: --stats=params --except=params,path'

          ],

          stats_limit: [
            '--stats_limit'.pastel(:green),
            '  Companion to the `stats` and used to filters total output percentages. Supports multiple and logic params',
            '  Ex: --stats_limit>=1 will only show stats results with higher than 1% frequency',
            '  Ex: --stats_limit<=15 --stats_limit>=10 slice >10% and <15% frequency',
            '  Ex: --stats_limit=1, filter for greater than %1'

          ],

          uniq: [
            '--uniq'.pastel(:green),
            '  Show unique values only',
            '  Ex: --uniq=params --uniq=params,path'

          ],

          pluck: [
            '--pluck'.pastel(:green),
            '  Extract values from entries',
            '  Ex: --pluck=params --pluck=params,path'

          ],

          archive: [
            '--archive'.pastel(:green),
            '  Limit to specific archvie name (partial matching /inclusive). Matching SOS tar.gz name',
            '  Ex: --archive=dev-gitlab_20210622154626, --archive=202106,202107'

          ],

          sort: [
            '--sort'.pastel(:green),
            '  Sort by multiple fields',
            '  Ex: --sort=duration_s,db_duration_s'

          ],

          reverse: [
            '--reverse'.pastel(:green),
            '  Reverse all results',
            '  Ex: --reverse'

          ],

          combine: [
            '--combine'.pastel(:green),
            '  Omit archive identifier dividers. Useful with sort or time filters',
            '  Ex: --combine'

          ],

          case: [
            '--case'.pastel(:green),
            '  Exact case match. Defaults to case insensitive',
            '  Ex: --case; --name=Jon, --name=jane --case'

          ],

          exact: [
            '--exact'.pastel(:green),
            '  Exact parameter/value match. Defaults to partial match',
            '  Ex: --field=CommonPartial --exact'

          ],

          start: [
            '--start'.pastel(:green),
            '  Show events after specified time. Filtered by the `time` field',
            '  Use with `--end` for between selections',
            '  Ex: log filter --start="2021-06-22 14:44  UTC" --end="2021-06-22 14:45 UTC"'

          ],

          end: [
            '--end'.pastel(:green),
            '  Show events before specified time. Filtered by the `time` field',
            '  Use with `--start` for between selections',
            '  Ex: log filter --end="2021-06-22"'
          ],

          time_zone: [
            '--time_zone'.pastel(:green),
            '  Manipulate the `time` field into a specific timezone',
            '  Ex: log filter --time_zone=EDT'

          ],

          text: [
            '--text'.pastel(:green),
            '  Full entry text searching (slow)',
            '    --text="anything here"'
          ],

          table_style: [
            '--table_style'.pastel(:green),
            '  Renderer used for formatted output. basic, ascii, or unicode(default)',
            '  Without any params this will result in basic',
            '  Ex: log filter --table_style=basic'
          ],

          truncate: [
            '--truncate'.pastel(:green),
            '  Truncate field length. On by default (4 rows). Performance issues!',
            '  Disable with --truncate=0'.pastel(:bright_red),
            '  Ex: --truncate=200, --truncate=2048"'
          ],

          interval: [
            '--interval'.pastel(:green),
            '  Split results down into a interval (based on the time field)',
            '  Example Params: 5m, 1d, 3h, "30 minutes" (chronic duration)',
            '  Ex: --interval=15m, --interval=1h"'
          ],

          percentile: [
            '--percentile'.pastel(:green),
            '  Stats breakdown on any integer value within the results. No params',
            '  Best when combined with slice/round to limit/filter results',
            '  No parameters, Returns 99,95,mean,min,max and entry count',
            '  Ex: --percentile --round --slice=field',
            '  Query: gitlab-rails/api_json.log --percentile --round=2 --slice=cpu_s'
          ],

          field_search: [
            'General (Field) Searching'.pastel(:blue),
            "  There are different ways filter within specific keys/fields within log. The baseline is #{'--[key]=[value]'.pastel(:green)}",
            '  This will, by default, select any entries with any partial match (case insensitive) of [value] in the entry [key]',
            '  Example: --path=mirror/pull',
            '',
            "  You can modify logic with params like: #{'case'.pastel(:green)}, #{'exact'.pastel(:green)}",
            '  Ex Case turn on case sensitivity, exact will change from the inclusive to only exact matching.',
            '',
            "  You can also use multiple #{'--[key]=[value]'.pastel(:green)} to narrow down, or combine it with #{'or'.pastel(:green)} to be more inclusive",
            '  Ex nginx/gitlab_access.log --http_user_agent=gitlab-runner --path=/api/v4/jobs/request',
            '  This will look through the access logs for anything that is a runner (user agent) and hitting the request endpoint',
            '',
            " You can also modify whether a field search excluded or a integer (and technically string) comparison by changing the operator:  #{'(!= >= <=)'.pastel(:green)}",
            ' Ex "nginx/gitlab_access.log --http_user_agent=gitlab-runner  --http_user_agent!=13.12.0" will exclude any entries with 13.12.0/13.12 runners',
            ' Ex "sidekiq/current --duration_s>=5 --class!=LdapSyncWorker" Duration over 5 that is not an ldap job'
          ],

          pipe_to_shell: [
            'Pipe to Shell'.pastel(:blue),
            '  This is very experimental! The idea is to take all the output from the search and allow a pipe into a traditional shell command.',
            '  Due to the arg parsing its not a straight send to the terminal. You may need to escape quotes.',
            '  Order is relevant, no arguments after the pipe',
            '  <files> <params> | grep path'
          ],

          ls: [
            'Search specific logs'.pastel(:blue),
            '  Any non dash parameters will be the log list to search from',
            "  Ex: log filter --path=api sidekiq/current (hint: use  `#{'ls'.pastel(:yellow)}` for log names"
          ],

          examples: [
            'Example Queries'.pastel(:blue),
            "  Also see #{'examples'.pastel(:bright_blue)} for even more examples",
            '  log filter --class=BuildFinishedWorker sidekiq/current --slice=time,message',
            '  log filter gitlab-rails/api_json.log --slice=ua --uniq=ua --ua=gitlab-runner'
          ]

        }
      end
      # rubocop:enable Metrics/MethodLength,Layout/LineLength,Metrics/ModuleLength
    end
  end
end
