module GreenHat
  # CLI Helper
  module Shell
    # Logs
    module Log
      def self.auto_complete(list, word)
        # Argument Parsing
        files, flags, _args = Args.parse(list)

        # Don't try to autocomplete anything else
        return nil unless word =~ /^-/

        # Clean Up
        word.delete!('-')
        matches = FieldHelper.filter_flags(word)

        if matches.count == 1
          "--#{matches.first}"
        elsif matches.count > 1
          puts "#{'Filter Options:'.pastel(:bright_blue)} #{matches.join(' ').pastel(:bright_green)}"
          "--#{Cli.common_substr(matches)}"
          # -----------------------------------
          # TODO: Fix Icky Double Nesting
        elsif files.count.nonzero?
          matches = FieldHelper.fields_find(files, word, flags)

          return nil if matches.nil?

          if matches.count == 1
            "--#{matches.first}"
          elsif matches.count > 1
            puts "#{'Field Options:'.pastel(:bright_blue)} #{matches.join(' ').pastel(:bright_green)}"
            "--#{Cli.common_substr(matches.map(&:to_s))}"
          elsif FieldHelper.field_auto_complete?(word)
            FieldHelper.field_auto_complete(word, files, flags)
          end
          # -----------------------------------
        end
      end

      def self.help
        puts "\u2500".pastel(:cyan) * 20
        puts "#{'Logs'.pastel(:yellow)} find stuff"
        puts "\u2500".pastel(:cyan) * 20

        puts 'Command Summary'.pastel(:blue)
        puts '  filter'.pastel(:green)
        puts "    Primary way for log searching within greenhat. See #{'filter_help'.pastel(:blue)}"
        puts '    Time, round, slice/except, and/or, stats, uniq, sort'
        puts "    #{'filter_help'.pastel(:blue)} supports filtering Ex: #{'filter_help stats'.pastel(:blue)}"
        puts

        puts '  show'.pastel(:green)
        puts '    Just print selected logs'
        puts

        puts '  search'.pastel(:green)
        puts "    Deprecated! Use the #{'filter --text="search"'.pastel(:blue)} instead"
        puts

        puts '  save'.pastel(:green)
        puts '    Save the last query result into a new searchable object'
        puts

        puts '  write'.pastel(:green)
        puts '    Write the last query result into a local file'
        puts

        puts '  visualize'.pastel(:green)
        puts '    Load web services and formulate last query for the UI'
        puts

        puts ShellHelper::List.help

        puts "See #{'examples'.pastel(:bright_blue)} for query examples"
      end

      def self.filter_help(raw = {})
        args, flags, _args = Args.parse(raw)

        ShellHelper.show(ShellHelper::Filter.help(args.first), flags)
      end

      def self.ls(args = [])
        ShellHelper::List.list(args, ShellHelper::Log.list)
      end

      def self.show(raw = {})
        # Extract Args
        files_list, flags, _args = Args.parse(raw)

        # Collect Files
        files = ShellHelper.files(files_list, Thing.all, flags)

        ShellHelper.show files.map(&:data).flatten
      end

      def self.save(raw = [])
        if ShellHelper::Log.last.nil?
          puts 'No previous query found'.pastel(:red)
          puts 'Run a query first then save to store as a new log'
          puts
          puts "Try #{'nginx/gitlab_access.log --status!=200'.pastel(:green)} then #{'save'.pastel(:green)}"
          true
        end

        name = if raw.empty?
                 Cli.prompt.ask('Log entry to save the results to? '.pastel(:yellow))
               else
                 raw.first
               end

        if name.blank?
          puts 'Name required'.pastel(:red)
          return true
        end

        results = ShellHelper.filter_internal ShellHelper::Log.last

        # Don't save empty results
        if results.empty?
          puts 'No results'.pastel(:red)
          ShellHelper::Log.no_files_warning(files) if ShellHelper.find_things(files, flags).count.zero?
          return false
        end

        Thing.new.query_save(save_greenhat_query_info + results, name)
        puts "#{name.pastel(:green)} Saved!"
      end

      def self.save_greenhat_query_info
        [{
          time: Time.now,
          query: ShellHelper::Log.last,
          msg: 'GreenHat Save/Write'
        }]
      end

      def self.write(raw = [])
        if ShellHelper::Log.last.nil?
          puts 'No previous query found'.pastel(:red)
          puts 'Run a query first then write'
          puts
          puts "Try #{'nginx/gitlab_access.log --status!=200'.pastel(:green)} then #{'save'.pastel(:green)}"
          true
        end

        name = if raw.empty?
                 Cli.prompt.ask('Filename to save the results to? '.pastel(:yellow))
               else
                 raw.first
               end

        if name.blank?
          puts 'Name required'.pastel(:red)
          return true
        end

        results = ShellHelper.filter_internal ShellHelper::Log.last

        # Don't save empty results
        if results.empty?
          puts 'No results'.pastel(:red)
          ShellHelper::Log.no_files_warning(files) if ShellHelper.find_things(files, flags).count.zero?
          return false
        end

        all = (save_greenhat_query_info + results).map { |row| Oj.dump(row) }
        File.write(name, all.join("\n"))
        puts "#{name.pastel(:green)} File Written!"
      end

      def self.visualize
        if ShellHelper::Log.last.nil?
          puts 'No previous query found'.pastel(:red)
          puts 'Run a query first then visualize to load it in the chart web page'
          puts
          puts "Try #{'nginx/gitlab_access.log --status!=200'.pastel(:green)} then #{'visualize'.pastel(:green)}"
          return true
        end

        # Load Required Files
        require 'greenhat/web'

        unless GreenHat::Web.alive?
          GreenHat::Web.start
          sleep 0.2
        end

        url = "http://localhost:4567/chart/time?query=#{CGI.escape(ShellHelper::Log.last)}"

        GreenHat::Platform.open url
      end

      # ========================================================================
      # Filter (See Filter Help)
      # ========================================================================
      def self.default(raw_list)
        filter(raw_list)
      end

      def self.filter(raw)
        # Print Helper
        if raw == ['help']
          filter_help(raw)
          return true
        end

        ShellHelper::Log.last = raw

        # Argument Parsing
        files, flags, args = Args.parse(raw)

        # Prepare Log List
        files = ShellHelper.prepare_list(files, ShellHelper::Log.list)

        results = Query.start(files, flags, args)

        # Skip and Print Total if set
        if flags[:fields]
          ShellHelper.fields_print(files)
          return true
        end

        # Check Search Results
        if results.empty?
          puts 'No results'.pastel(:red)
          ShellHelper::Log.no_files_warning(files) if ShellHelper.find_things(files, flags).count.zero?
        elsif flags[:pipe]
          Pipe.show(results, flags[:pipe])
        else
          # Allow for array / multiple table outputs
          # This causes the key 'colorized' output to also be included
          ShellHelper.show(results, flags)
        end
      rescue StandardError => e
        LogBot.fatal('Filter', message: e.message)
        ap e.backtrace
      end
      # ========================================================================

      # rubocop:disable Layout/LineLength
      # TODO: Add a lot more examples
      def self.examples
        puts 'Find `done` job for sidekiq, sort by duration, only duration, and show longest first'.pastel(:bright_green)
        puts 'log filter sidekiq/current --job_status=done --sort=duration_s,db_duration_s --slice=duration_s,db_duration_s --reverse'
        puts

        puts 'Find 500s only show exceptions'.pastel(:bright_green)
        puts 'log filter --status=500 --slice=exception.message gitlab-rails/production_json.log'
        puts

        puts 'Show unique sidekiq queue namespaces. Exclude Specifics'.pastel(:bright_green)
        puts 'filter sidekiq/current --slice=queue_namespace --uniq=queue_namespace --queue_namespace!=jira_connect --queue_namespace!=hashed_storage'
        puts

        puts 'Show user,ip from API logs where `meta.user` field is present '.pastel(:bright_green)
        puts 'gitlab-rails/api_json.log --slice=meta.user,meta.remote_ip --exists=meta.user'
        puts

        puts 'Count/% occurences for both user and remote ip fields'.pastel(:bright_green)
        puts 'gitlab-rails/api_json.log --stats=meta.user,meta.remote_ip --exists=meta.user'
        puts

        puts 'Sidekiq jobs that took over 5 seconds excluding LdapSyncWorker jobs'.pastel(:bright_green)
        puts 'sidekiq/current --duration_s>=5 --class!=LdapSyncWorker'
        puts

        puts 'Search access logs for runner requests, exclude specific runner version'.pastel(:bright_green)
        puts 'nginx/gitlab_access.log --http_user_agent=gitlab-runner  --http_user_agent!=13.12.0'
        puts

        puts 'Get a list of unique Gitaly error messages for a specific project'.pastel(:bright_green)
        puts 'filter --level=error --grpc.request.glProjectPath=path/to/project  gitaly/current --slice=error --uniq=error'
        puts

        puts 'Show workhorse duration/URI. Filter by duration bounds'.pastel(:bright_green)
        puts 'gitlab-workhorse/current --duration_ms>=30000 --duration_ms<=45000 --slice=duration_ms,uri'
        puts

        puts 'Show runner statistics'.pastel(:bright_green)
        puts 'gitlab-rails/api_json.log --path=/api/v4/jobs/request --percentile --round=2'
        puts
      end
      # rubocop:enable Layout/LineLength

      # ========================================================================
      # Search (Full Text / String Search)
      # ========================================================================
      # Deprecated
      # TODO: Remove
      # def self.search(raw)
      #   # Extract Args
      #   files_list, flags, args = Args.parse(raw)

      #   # Prepare Log List
      #   files = ShellHelper.prepare_list(files_list, ShellHelper::Log.list)

      #   results = ShellHelper.search_start(files, flags, args)

      #   # Skip and Print Total if set
      #   if flags[:total]
      #     ShellHelper.total_count(results, flags)
      #     return true
      #   end

      #   # Check Search Results
      #   if results.values.flatten.empty?
      #     puts 'No results'.pastel(:red)
      #     ShellHelper::Log.no_files_warning(files) if ShellHelper.find_things(files, flags).count.zero?
      #   else
      #     # This causes the key 'colorized' output to also be included
      #     ShellHelper.show(results.to_a.compact.flatten, flags)
      #   end
      # rescue StandardError => e
      #   LogBot.fatal('Search', message: e.message)
      #   ap e.backtrace
      # end
      # ========================================================================

      # Supported Params
      # --text='asdf'
      # --text!='asdf'
      # --slice=time,path (E.g. log filter  --path='mirror/pull' --slice=path,time )
      # --except: Exclude specific fields (except multiple with comma)

      # def self.search_help
      #   puts "\u2500".pastel(:cyan) * 20
      #   puts 'Log Search'.pastel(:yellow)
      #   puts "\u2500".pastel(:cyan) * 20

      #   puts 'Search will do a full line include or exclude text search'

      #   puts 'Options'.pastel(:blue)
      #   puts '--text'.pastel(:green)
      #   puts '  Primary parameter for searching. Include or ! to exclude'
      #   puts '  Ex: --text=BuildHooksWorker --text!=start sidekiq/current'
      #   puts

      #   puts '--total'.pastel(:green)
      #   puts '  Print only total count of matching entries'
      #   puts

      #   puts '--slice'.pastel(:green)
      #   puts '  Extract specific fields from entries (slice multiple with comma)'
      #   puts '  Ex: --slice=path or --slice=path,params'
      #   puts

      #   puts '--except'.pastel(:green)
      #   puts '  Exclude specific fields (except multiple with comma)'
      #   puts '  Ex: --except=params --except=params,path'
      #   puts

      #   puts '--archive'.pastel(:green)
      #   puts '  Limit to specific archive name (inclusive). Matching SOS tar.gz name'
      #   puts '  Ex: --archive=dev-gitlab_20210622154626, --archive=202106,202107'
      #   puts

      #   puts '--limit'.pastel(:green)
      #   puts '  Limit total number of results. Default to half total screen size'
      #   puts '  Ex: --limit; --limit=10'
      #   puts

      #   puts 'Search specific logs'.pastel(:blue)
      #   puts '  Any non dash parameters will be the log list to search from'
      #   puts "  Ex: log filter --path=api sidekiq/current (hint: use  `#{'ls'.pastel(:yellow)}` for log names)"
      #   puts

      #   puts 'Example Queries'.pastel(:blue)
      #   puts 'log search --text=BuildHooksWorker --text!=start sidekiq/current --total'
      #   puts 'log search --text=BuildHooksWorker --text!=start --slice=enqueued_at sidekiq/current'
      #   puts
      # end
      # ------------------------------------------------------------------------
    end
  end
end

module GreenHat
  module ShellHelper
    # Log Helpers
    module Log
      def self.last=(value)
        @last = value.join(' ')
      end

      def self.last
        @last
      end

      def self.no_files_warning(files)
        puts "No matching files found for pattern #{files.to_s.pastel(:yellow)}"
        puts "See #{'ls'.pastel(:blue)} for available files"
      end

      # Limit to Log Files
      def self.list
        Thing.all.select(&:log)
      end
    end

    # --------
  end
end
