module GreenHat
  # CLI Helper
  module Shell
    # Logs
    module Network
      # Easy Show All
      def self.netstat
        ShellHelper.file_output GreenHat::Network.netstat
      end
    end
  end
end
