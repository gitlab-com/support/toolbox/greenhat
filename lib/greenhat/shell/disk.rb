module GreenHat
  # CLI Helper
  module Shell
    # Logs
    module Disk
      # Alias
      def self.ls
        Cli.help
      end

      def self.help
        puts "\u2500".pastel(:cyan) * 22
        puts "#{'Disk'.pastel(:yellow)} -  Storage Helper"
        puts "\u2500".pastel(:cyan) * 22

        ShellHelper.common_opts

        puts 'Command Summary'.pastel(:blue)
        puts '  df'.pastel(:green)
        puts "    Raw #{'df'.pastel(:cyan)} output"
        puts
        puts '  free'.pastel(:green)
        puts '    Formatted / Bar Output'
        puts
      end

      # Easy Show All
      def self.df(raw = {})
        # Extract Args
        files_list, flags, _args = Args.parse(raw)

        # Collect Files
        files = ShellHelper.files(files_list, GreenHat::Disk.df, flags)

        # Output
        ShellHelper.file_output(files, flags)
      end

      def self.free(raw = {})
        # Extract Args
        files_list, flags, _args = Args.parse(raw)

        # Collect Files
        files = ShellHelper.files(files_list, GreenHat::Disk.df, flags)

        files.each do |file|
          puts GreenHat::Disk.format_output(file, true)

          # File End Loop / Break
          puts
        end
      end
      # ------------------------------------------------------------------------
    end
  end
end
