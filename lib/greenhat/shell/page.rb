module GreenHat
  module ShellHelper
    # Helper to organize page check / methods
    module Page
      # Check if paging should be skipped
      # True / False / Not Set
      def self.skip?(flags, data)
        # Pass if Explicitly Set / Inverse for skip
        return !flags[:page] if flags.key? :page

        LogBot.debug('Page Skip', count_rows(data, flags)) if ENV['DEBUG']

        count_rows(data, flags)
      end

      # Array/Hash and String pagination check. Don't unncessarily loop through everything
      def self.count_rows(data, flags)
        height = TTY::Screen.height
        size = 0

        data.each do |entry|
          size += case entry
                  when Hash then entry.keys.count
                  when Array then entry.count
                  else
                    # Each Boxed Entry is 3 Lines
                    flags.key?(:row_size) ? flags[:row_size] : 1

                  end

          break if size > height
        end

        size < height
      end
    end
    # ----
  end
end
