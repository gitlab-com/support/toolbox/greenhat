# Deprecated in favor of reports module
# TODO: Remove / Replace for markdown report
# module GreenHat
#   # Root Level Shell / Report Helper
#   module Shell
#     def self.markdown_report(raw)
#       _files, flags, _args = Args.parse(raw)

#       archives = if flags.archive
#                    Archive.all.select do |archive|
#                      flags.archive.any? { |x| archive.name.include? x.to_s }
#                    end
#                  else
#                    Archive.all
#                  end

#       ShellHelper.show(archives.map(&:report_markdown).map(&:show).flatten, flags)
#     end
#   end
# end

# module GreenHat
#   # Report Generator Helper
#
#   class ReportMarkdown
#     include ActionView::Helpers::NumberHelper

#     attr_accessor :archive, :host, :os_release, :selinux_status, :cpu, :uname,
#                   :timedatectl, :uptime, :meminfo, :gitlab_manifest, :gitlab_status,
#                   :production_log, :api_log, :application_log, :sidekiq_log,
#                   :exceptions_log, :gitaly_log, :free_m, :disk_free

#     # Find Needed Files for Report
#
#     def initialize(archive)
#       self.archive = archive
#       self.host = archive.things.find { |x| x.name == 'hostname' }
#       self.os_release = archive.things.find { |x| x.name == 'etc/os-release' }
#       self.selinux_status = archive.things.find { |x| x.name == 'sestatus' }
#       self.cpu = archive.things.find { |x| x.name == 'lscpu' }
#       self.uname = archive.things.find { |x| x.name == 'uname' }
#       self.timedatectl = archive.things.find { |x| x.name == 'timedatectl' }
#       self.uptime = archive.things.find { |x| x.name == 'uptime' }
#       self.meminfo = archive.things.find { |x| x.name == 'meminfo' }
#       self.free_m = archive.things.find { |x| x.name == 'free_m' }
#       self.gitlab_manifest = archive.things.find { |x| x.name == 'gitlab/version-manifest.json' }
#       self.gitlab_status = archive.things.find { |x| x.name == 'gitlab_status' }
#       self.production_log = archive.things.find { |x| x.name == 'gitlab-rails/production_json.log' }
#       self.api_log = archive.things.find { |x| x.name == 'gitlab-rails/api_json.log' }
#       self.application_log = archive.things.find { |x| x.name == 'gitlab-rails/application_json.log' }
#       self.exceptions_log = archive.things.find { |x| x.name == 'gitlab-rails/exceptions_json.log' }
#       self.gitaly_log = archive.things.find { |x| x.name == 'gitaly/current' }
#       self.sidekiq_log = archive.things.find { |x| x.name == 'sidekiq/current' }
#       self.disk_free = archive.things.find { |x| x.name == 'df_hT' }
#     end

#     def show
#       output = [
#         archive.friendly_name,
#         ''
#       ]

#       # GitLab Version
#       output << "**GitLab #{gitlab_version}**\n" if gitlab_manifest || gitlab_status

#       # OS
#       output << "**OS**\n"

#       output << collect_host
#       output << ''

#       # Memory
#       if meminfo || free_m
#         output << "**Memory**\n"
#         # output << memory_perc if meminfo
#         output << memory_free if free_m
#         output << ''
#       end

#       # Disk
#       if disk_free
#         output << disks
#         output << ''
#       end

#       # Gitlab
#       output << gitlab_services if gitlab_status

#       output << ''

#       output << "**Errors**\n" if production_log || api_log || application_log || sidekiq_log
#       output << collect_errors

#       # Final Space / Return
#       output << ''
#       output
#     end
#     # rubocop:enable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity

#     def collect_host
#       output = []
#       output << hostname if host
#       output << distro if os_release
#       output << selinux if selinux_status
#       # output << arch if cpu
#       output << kernel if uname
#       output << sys_time if timedatectl
#       output << sys_uptime if uptime
#       output << load_average if uptime && cpu

#       groups = output.each_slice((output.size / 2.to_f).round).to_a

#       table = TTY::Table.new do |t|
#         loop do
#           break if groups.all?(&:empty?)

#           t << groups.map(&:shift)
#         end
#       end

#       "```\n#{table.render(:basic, padding: [0, 2, 0, 0])}\n```"
#     end

#     def collect_errors
#       output = []
#       output << production_errors if production_log
#       output << application_errors if application_log
#       output << sidekiq_errors if sidekiq_log
#       output << api_errors if api_log
#       output << exception_errors if exceptions_log
#       output << gitaly_errors if gitaly_log

#       # Keep Alphabetical Sort / Allow for only one
#       slice_size = (output.size / 3.to_f).round
#       slice_size = 1 unless slice_size.positive?

#       groups = output.each_slice(slice_size).to_a

#       table = TTY::Table.new do |t|
#         loop do
#           break if groups.all?(&:empty?)

#           t << groups.map(&:shift)
#         end
#       end

#       "```\n#{table.render(:basic, padding: [0, 2, 0, 0])}\n```"
#     end

#     def exception_errors
#       count = exceptions_log.data.count

#       "Exception: #{count}"
#     end

#     def gitaly_errors
#       count = gitaly_log.data.count { |x| x.level == 'error' }

#       "Gitaly: #{count}"
#     end

#     def production_errors
#       count = production_log.data.count { |x| x.status == 500 }

#       "Production: #{count}"
#     end

#     def api_errors
#       count = api_log.data.count { |x| x.status == 500 }

#       "API: #{count}"
#     end

#     def application_errors
#       results = ShellHelper.filter_internal([
#         'gitlab-rails/application_json.log',
#         '--message!="Cannot obtain an exclusive lease"',
#         '--severity=error',
#         "--archive=#{archive.name}"
#       ].join(' '))

#       "Application: #{results.count}"
#     end

#     def sidekiq_errors
#       count = sidekiq_log.data.count { |x| x&.severity == 'ERROR' }

#       "Sidekiq: #{count}"
#     end

#     def gitlab_services
#       [
#         "**Services**\n",
#         "\n",
#         GreenHat::GitLab.services_markdown(archive)
#       ].join
#     rescue StandardError => e
#       LogBot.fatal('GitLab Services', message: e.message, backtrace: e.backtrace.first)
#     end

#     def gitlab_version
#       txt = gitlab_manifest.data.dig(:software,
# :'gitlab-rails', :display_version) || gitlab_manifest.data.build_version

#       if txt.include? '-ce'
#         txt += ' - [😱 CE](https://about.gitlab.com/support/statement-of-support.html#free-and-community-edition-users)!'
#       end

#       "Version: #{txt}"
#     end

#     def hostname
#       "Hostname: #{host.data.first}"
#     end

#     def distro
#       [
#         "Distro: [#{os_release.data.ID}] ",
#         os_release.data.PRETTY_NAME
#       ].join
#     end

#     def selinux
#       status = selinux_status.data['SELinux status']

#       [
#         'SeLinux: ',
#         status,
#         ' (',
#         selinux_status.data['Current mode'],
#         ')'
#       ].join
#     end

#     def arch
#       [
#         'Arch: ',
#         cpu.data.Architecture
#       ].join
#     end

#     def kernel
#       # TODO: Better way to consistently get uname info?
#       value, build = uname.data.first.split[2].split('-')
#       [
#         'Kernel: ',
#         value,
#         " (#{build})"
#       ].join
#     end

#     # Helper for finding if NTP is enabled
#     def ntp_keys
#       [
#         'Network time on', 'NTP enabled', 'NTP service', 'System clock synchronized'
#       ]
#     end

#     def sys_time
#       # Ignore if Empty
#       return false if timedatectl.data.nil?

#       ntp_statuses = timedatectl.data.slice(*ntp_keys).values.compact

#       ntp_status = ntp_statuses.first

#       # Fall Back
#       ntp_status ||= 'unknown'

#       [
#         'Sys Time: ',
#         timedatectl.data['Local time'],
#         "(ntp: #{ntp_status})"
#       ].join
#     end

#     # Strip/Simplify Uptime
#     def sys_uptime
#       init = uptime.data.first.split(',  load average').first.strip

#       "Uptime: #{init.split('up ', 2).last}"
#     end

#     def load_average
#       cpu_count = cpu.data['CPU(s)'].to_i
#       intervals = uptime.data.first.split('load average: ', 2).last.split(', ').map(&:to_f)

#       # Generate Colorized Text for Output
#       intervals_text = intervals.map do |interval|
#         value = percent(interval, cpu_count)

#         "#{interval} (#{value}%)"
#       end

#       [
#         'LoadAvg: ',
#         "[CPU #{cpu_count}] ",
#         intervals_text.join(', ')
#       ].join
#     end

#     def memory_free
#       free = free_m.data.find { |x| x.kind == 'Mem' }

#       return unless free

#       pad = 6
#       list = [
#         "#{title('Total', pad)} #{number_to_human_size(free.total.to_i * (1024**2))}",
#         "#{title('Used', pad)} #{number_to_human_size(free.used.to_i * (1024**2))}",
#         "#{title('Free', pad)} #{number_to_human_size(free.free.to_i * (1024**2))}",
#         "#{title('Avail', pad)} #{number_to_human_size(free.available.to_i * (1024**2))}"
#       ]

#       # Keep Alphabetical Sort
#       groups = list.each_slice((list.size / 2.to_f).round).to_a

#       table = TTY::Table.new do |t|
#         loop do
#           break if groups.all?(&:empty?)

#           t << groups.map(&:shift)
#         end
#       end

#       "```\n#{table.render(:basic, padding: [0, 2, 0, 0])}\n```"
#     end

#     def disks
#       # GreenHat::Disk.df({archive: []})
#       file = GreenHat::Disk.df({ archive: [archive.name] })

#       disk_list = GreenHat::Disk.markdown_format(file.first, false, 3)

#       # Preapre / Indent List
#       [
#         '**Disks**',
#         "\n\n```\n#{disk_list.join("\n")}\n```"
#       ].join
#     end

#     # ----------------------------
#     # Helpers
#     # ----------------------------
#     def percent(value, total)
#       ((value / total.to_f) * 100).round
#     end

#     # Helper to Make Cyan Titles
#     def title(name, ljust = 16)
#       "#{name}:".ljust(ljust)
#     end
#   end
#   # rubocop:enable Metrics/ClassLength
# end
