module GreenHat
  # CLI Helper
  module Shell
    # Common File Reader File
    module Cat
      def self.help
        puts "\u2500".pastel(:cyan) * 20
        puts "#{'Cat'.pastel(:yellow)} All the files"
        puts "\u2500".pastel(:cyan) * 20
        puts 'Print raw file by just entering the file name'
        puts

        ShellHelper.common_opts

        puts 'Commands'.pastel(:blue)
        puts ShellHelper::List.help

        puts '  <file names+>'.pastel(:green)
        puts '    Print any file names'
        puts '    Ex: `free_m`'
        puts '    Ex: `ps mount --raw`'
        puts

        puts "  #{'show'.pastel(:green)} <file names>"
        puts '    Attempt to print formatted output'
        puts '    Ex: show `free_m`'
      end

      # ========================================================================
      # Default
      # ========================================================================
      def self.default(raw)
        # Extract Args
        files_list, flags, _args = Args.parse(raw)

        # Collect Files
        files = ShellHelper.files(files_list, Thing.all, flags)

        results = ShellHelper.file_process(files) do |file|
          [
            file.friendly_name,
            file.output(false),
            "\n"
          ]
        end

        ShellHelper.show(results.flatten, flags)
      end

      # ========================================================================
      # Show Attempted Formatting
      # ========================================================================
      def self.show(raw)
        # Extract Args
        files_list, flags, _args = Args.parse(raw)

        # Collect Files
        files = ShellHelper.files(files_list, Thing.all, flags)

        results = ShellHelper.file_process(files) do |file|
          [
            file.friendly_name,
            file.data,
            "\n"
          ]
        end

        ShellHelper.show(results.flatten, flags)
      end

      def self.ls(args = [])
        ShellHelper::List.list(args, Thing.all)
      end
      # ------------------------------------------------------------------------
    end
  end
end
