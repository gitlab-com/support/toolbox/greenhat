module GreenHat
  # Output Helper
  class Paper
    include PaperHelpers
    include PaperFlagHelper
    attr_accessor :task, :output, :flags, :data, :done, :mutex

    def initialize(data:, flags:)
      self.mutex = Mutex.new
      self.data = data
      self.flags = flags
    end

    def async
      return if done # Don't attempt lock if already done

      mutex.synchronize do
        return if done # Turn if was waiting on lock

        process

        self.output ||= '' # If Empty

        self.done = true
      end
    end

    def render
      async unless done

      output
    end
  end
end
