# Top level namespace
module GreenHat
  # Entrypoint Methods
  # Command Line Args / File Parsing etc
  module Entrypoint
    # If no arguments Supplied Print and quit - rather than nasty exception
    def self.start(raw, clear = true, cli = true)
      @clear = clear # Reset screen by default
      Settings.start
      files, flags, args = Args.parse(raw)
      pre_args(flags, files)
      load_files files
      Cli.run_command(args) if args.any? { |arg| Cli.run_command?(arg) }

      report_runner(args:, files:, raw:, flags:)
      post_args(flags)
      post_setup(flags)

      Motd.start
      Cli.start if cli # Kick over to CLI
    end

    # Arguments before general processing
    def self.pre_args(flags, files)
      # Help
      if flags?(%i[help h], flags) && !ENV['GREENHAT_REPORT']
        cli_help
        exit 0
      end

      # Version
      if flags?(%i[version v], flags)
        Shell.version
        exit 0
      end

      # Assume Raw Setting
      Settings.settings[:assume_raw] = true if flags?(%i[no-prompt np], flags)
      Settings.settings[:assume_raw] = false if flags?(%i[prompt p], flags)

      # Quiet Flag
      Cli.quiet! if flags?(%i[quiet q], flags)

      if flags?(%i[load-local ll], flags)
        # Hook in loading local
        Shell.load_local(false)
      elsif files.empty? || files.count { |x| File.exist? x }.zero?
        # MIA Files
        puts "No arguments or files don't exist".pastel(:red)
        puts 'Usage: greenhat  <sos-archive.tgz> <sos-archive2.tgz>'
        puts 'See `--help`'

        @clear = false # Don't hide messages

        Shell.version
      end
    end

    # Helper to allow running any kind of report with arguments directly with greenhat
    def self.report_runner(args:, files:, raw:, flags:)
      if flags.report || flags.r
        GreenHat::Shell::Reports.default(['full'])
        exit 0
      end

      # See if Param was passed
      report = args.find { |x| %i[report r].include? x.field }
      return false unless report

      Cli.quiet! # Hush

      # Filter for additional params for Reports
      raw_filter = raw.reject do |entry|
        entry.include?('--report') || files.include?(entry)
      end

      # Prepend report name
      raw_filter.unshift(report.value)

      GreenHat::Shell::Reports.default([report.value])

      exit 0
    end

    # Arguments to be handled after general processing
    def self.post_args(flags)
      # Supress Color
      GreenHat::Settings.settings[:color] = false if flags?(%i[no-color n], flags)
      Cli.clear_screen if @clear
    end

    def self.post_setup(flags)
      # CTL Tails need to be parsed for new 'things'
      Thing.where(kind: :gitlab_tail)&.map(&:process)
      Thing.where(kind: :kube_webservice)&.map(&:process)

      Thing.all.each(&:process) if flags?(%i[load l], flags)

      Shell.web if flags?(%i[web w], flags)
    end

    # Helper to Simplify checking flags
    def self.flags?(list = [], flags = {})
      list.any? { |x| flags.key? x }
    end

    def self.load_files(files)
      # Don't double up on archives / Only Existing files
      puts 'Loading Archives'.pastel(:blue) unless Cli.quiet
      files.uniq.each do |file|
        next unless File.exist?(file)

        puts "- #{file}".pastel(:magenta) unless Cli.quiet
        ArchiveLoader.load file
      end
    end

    # rubocop:disable Metrics/MethodLength
    def self.cli_help
      Shell.version
      puts
      puts 'Usage'.pastel(:yellow)
      puts '  greenhat  <sos-archive.tgz> <sos-archive2.tgz> '
      puts '  greenhat  <sos-archive.tgz> -q --command=df'
      puts

      puts 'Options'.pastel(:yellow)
      puts '  --report, -r'.pastel(:green)
      puts '    Run default or specified `report` against archives and exit'
      puts '    Default report "full", --report, --report=demo, --report=errors'
      puts '    See reportable.md/wiki or the `reports` submodule'
      puts

      puts '  --quiet, -r'.pastel(:green)
      puts '    Surpress GreenHat logging output'
      puts

      puts '  --load, -l'.pastel(:green)
      puts '    Automatically attempt to read/parse/preload all included files'
      puts

      puts '  --load-local, -ll'.pastel(:green)
      puts '    Load log files from locations on an Omnibus install'
      puts

      puts '  --prompt, -p'.pastel(:green)
      puts '    Override settings / Assume Prompt'
      puts

      puts '  --no-prompt, -np'.pastel(:green)
      puts '    Do not prompt for file types / Assume Raw or JSON'
      puts

      puts '  --command, -c'.pastel(:green)
      puts '    Run and then exit a GreenHat Shell command'
      puts

      puts '  --web, -w'.pastel(:green)
      puts '    Start Sinatra Webservice on load (4567)'
      puts

      puts '  --no-color, -n'.pastel(:green)
      puts '    Disable color output'
      puts

      puts '  --version, -v'.pastel(:green)
      puts '    Print version and exit'
      puts
    end
    # rubocop:enable Metrics/MethodLength

    # ==========================================================
  end
end
