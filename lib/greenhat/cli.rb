# Top level namespace
module GreenHat
  # CLI Methods / Shell Specific COmmands
  # rubocop:disable Metrics/ModuleLength
  module Cli
    # CLI Start
    def self.start
      value ||= '' # Empty Start

      loop do
        line = reader.read_line(readline_notch, value:)
        value = '' # Remove Afterwards

        if reader.breaker
          value = line
          puts ''
          next
        end

        break if line =~ /^exit/i

        Settings.cmd_add(line) unless line.blank?

        process
        run
      end
    end

    def self.cursor
      @cursor = TTY::Cursor
    end

    # Input Loop Listener
    def self.reader
      @reader ||= reader_setup
    end

    # Context matters on which ones to autocomplete / Not saving in memory to allow better tab / auto complete
    def self.files
      case current_location
      when GreenHat::Shell::Log
        Thing.all.select(&:log).map(&:name.uniq)
      else
        Thing.all.map(&:name).uniq
      end
    end

    def self.reader_setup
      reader = TTY::Reader.new(history_duplicates: false, interrupt: -> { back })

      Settings.cmd_history_clean.each do |line|
        reader.add_to_history(line)
      end

      # Blank?
      reader.add_to_history ''

      # Remove Line
      reader.on(:keyctrl_u) do |_event|
        reader.line.remove reader.line.text.size
      end

      # Navigate Word Left
      reader.on(:keyctrl_left) { reader.line.move_word_left }

      # Navigate Word Right
      reader.on(:keyctrl_right) { reader.line.move_word_right }

      # Navigate Beginning
      reader.on(:keyctrl_a) { reader.line.move_to_start }

      # Navigate End
      reader.on(:keyctrl_e) { reader.line.move_to_end }

      reader.on(:keyback_tab) { back }

      reader.on(:keytab) do
        process
        auto
      end

      reader.instance_variable_get(:@history)

      # DEBUG PRY
      reader.on(:keyctrl_p) do |event|
        # rubocop:disable Lint/Debugger
        binding.pry
        # rubocop:enable Lint/Debugger
      end
      reader
    end

    # =========================================================
    # Auto Complete
    # =========================================================
    def self.auto
      word = @list.last
      # This is being weird with auto complete
      # word = reader.line.word

      if word.blank?
        help
      else
        auto_match(current_methods + current_submodules + cmd_list, word)
      end
    end

    def self.file_matches(word)
      files.select { |x| x[/^#{Regexp.escape(word)}/] }
    end

    def self.auto_match(matches, word)
      matches.select! { |x| x[/^#{Regexp.escape(word)}/] }

      if submodule?
        submodule!
        reader.breaker = true
        # Only one Match!
      elsif matches.count == 1
        auto_update(matches.first, word)

        # Print List of Options
      elsif matches.count > 1
        puts matches.join("\t").pastel(:bright_green)

        # General Filename Matching
      elsif !file_matches(word).empty?
        auto_files(file_matches(word), word)

        # Submodule Autocompletion
      elsif current_methods.include?('auto_complete')
        update_text = current_location.auto_complete(@list, word)
        auto_update(update_text, word) unless update_text.blank?
      end
    end

    # Auto Complete File Names
    def self.auto_files(matches, word)
      if matches.count == 1
        auto_update(matches.first, word)

        # Print List of Options
      elsif matches.count > 1
        auto_update(common_substr(matches), word)
        puts matches.join("\t").pastel(:bright_green)
      end
    end

    # Handle Updates to Reader
    def self.auto_update(match, word)
      add = match.split(word, 2).last
      reader.line.insert add unless add.nil?
    end

    # Complete as much as possible comparison
    # https://stackoverflow.com/a/2158481/1678507
    def self.common_substr(strings)
      shortest = strings.min_by(&:length)
      maxlen = shortest.length
      maxlen.downto(0) do |len|
        0.upto(maxlen - len) do |start|
          substr = shortest[start, len]
          return substr if strings.all? { |str| str.include? substr }
        end
      end
    end
    # =========================================================

    def self.did_you_mean
      dictionary = current_methods + current_submodules + cmd_list

      all = DidYouMean::SpellChecker.new(dictionary:).correct(cmd)

      if all.empty?
        puts [
          'Command not found '.pastel(:red),
          cmd.pastel(:bright_yellow),
          ' ('.pastel(:bright_black),
          'help'.pastel(:blue),
          ' to show available commands'.pastel(:bright_black),
          ')'.pastel(:bright_black)
        ].join
      else
        puts "#{'Did you mean?'.pastel(:cyan)}  #{all.join("\t").pastel(:green)}"
      end
    end

    # Auto/Run Process - Populate and parse: @list
    def self.process
      line = reader.line
      @list = Shellwords.split line.text
    rescue StandardError => e
      puts "#{'Invalid Command'.pastel(:red)}: #{e.message.pastel(:green)}"
    end

    def self.available(list)
      list.map(&:to_s).map(&:downcase)
    end

    # Reader helper
    def self.cmd
      @list.first
    end

    def self.run?
      current_methods.include? cmd
    end

    # Special Command Overrides
    def self.cmd_list
      ['help', '~', '..', 'back', 'clear']
    end

    def self.cmd?
      cmd_list.any? cmd
    end

    def self.submodule?
      current_submodules.include? @list.first
    end

    def self.cmd_run
      case cmd
      when 'help' then help
      when '~' then home
      when '..', 'back' then back
      when 'clear' then clear_screen
      end

      @list.shift
    end

    # Final Command Execution
    def self.run!
      # Shift to prevent duplicate Runs / Arity for identifying if Method has params
      if current_location.method(@list.first).arity.zero?
        current_location.send(@list.shift.clone)
      else
        current_location.send(@list.shift.clone, @list.clone)
      end
    end

    def self.run
      return true if @list.blank?

      if cmd?
        cmd_run
      elsif run?
        run!

        return true # End Early
      elsif current_submodule?
        @list.shift
      elsif submodule?
        submodule!
        # Prepend Default if exists
      elsif default?
        @list.unshift 'default'
      else
        did_you_mean
        @list.shift
      end

      run # Loop Back
    rescue StandardError => e
      LogBot.fatal('CLI Run', e.message)
      puts e.backtrace[0..4].join("\n").pastel(:red)
    end

    # Check for `default` method and files
    def self.default?
      @list.any? { |x| x.include?(cmd) } && current_methods.include?('default')
    end

    def self.current_commands
      current_location.methods(false).map(&:to_s).sort - %w[auto_complete]
    end

    # General Helper
    def self.help(long = true)
      if current_location.methods(false).count.zero?
        puts 'No Commands'.pastel(:red)
      else
        puts 'Commands: '
        current_commands.each do |item|
          next if %w[default help].any? { |x| x == item }

          puts "=> #{item.to_s.pastel(:blue)}"
        end
      end

      puts ''

      if current_location.constants.count.zero?
        puts 'No Submodules'.pastel(:red)
      else
        puts 'Submodules'
        current_location.constants.each do |item|
          puts "-> #{item.to_s.demodulize.downcase.pastel(:yellow)}"
        end
      end

      # Execute local help last if exists
      if current_methods.include?('help') && long
        puts
        current_location.send(:help)
      end

      puts ''
    end

    def self.location
      @location ||= [Shell]
    end

    # Replace Location
    def self.move(spot)
      @location = [spot]
    end

    # Shell Context Location
    def self.move_shell(spot)
      @location = [GreenHat::Shell, spot]
    end

    # Append to Location
    def self.submodule!
      spot = @list.shift
      reader.line.replace @list.join(' ')
      @location << current_location.const_get(spot.capitalize.to_sym)
    end

    def self.home
      move Shell
    end

    def self.back
      move location[-2] if location.count > 1
    end

    # Reader to Get Last Location's Available Methods / Keep Helper Methods
    def self.current_methods
      current_location.methods(false).map(&:to_s)
    end

    def self.current_submodules
      current_location.constants.map(&:to_s).map(&:downcase)
    end

    # Full Commands at the Submodule `disk summary` from `disk`
    def self.current_submodule?
      current_location.to_s.demodulize.downcase == @list.first
    end

    def self.current_location
      location.last
    end

    def self.location_reader
      location.map(&:to_s).map(&:downcase).map(&:demodulize).join('/').gsub('shell', '~').pastel(:blue)
    end

    def self.readline_notch
      "#{'greenhat'.pastel(:bright_black)} #{location_reader} » "
    end

    def self.clear_screen
      print cursor.clear_screen + cursor.move_to(0, 0)
    end

    def self.quiet
      @quiet
    end

    def self.quiet!
      @quiet = true
    end

    # Toggle Quiet Settings
    def self.quiet_toggle
      @quiet = !@quiet
    end

    def self.run_command?(arg)
      %i[command c].any? do |x|
        arg[:field] == x
      end
    end

    # Run and Exit Command Helper
    def self.run_command(args)
      args.each do |arg|
        # Quick Validation
        next unless run_command?(arg) && !arg.value.empty?

        reader.line = ''
        @list = Shellwords.split arg.value
        run
      end
      exit 0
    end

    def self.suppress_output
      original_stderr = $stderr.clone
      original_stdout = $stdout.clone
      $stderr.reopen(File.new('/dev/null', 'w'))
      $stdout.reopen(File.new('/dev/null', 'w'))
      yield
    ensure
      $stdout.reopen(original_stdout)
      $stderr.reopen(original_stderr)
    end

    def self.template
      "#{__dir__}/views/index.slim"
    end

    def self.output
      'greenhat.html'
    end

    def self.prompt
      TTY::Prompt.new(active_color: :cyan)
    end

    # TODO
    def self.menu
      prompt.select('Wat do?') do |menu|
        menu.choice name: 'exit'
        menu.choice name: 'Export Kibana Dashboard', value: 'export'
        menu.choice name: 'console (pry)', value: 'console'
      end
    end

    def self.bad_file(archive)
      puts color("Cannot find archive: #{archive}", :red)
      exit 1
    end
  end

  # rubocop:enable Metrics/ModuleLength
end
