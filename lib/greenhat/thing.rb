# Top Level what everything is
class Thing < Teron
  include GreenHat::Formatters
  include GreenHat::ThingHelpers
  include GreenHat::Spinner
  include GreenHat::Kind
  include GreenHat::FileTypes
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::NumberHelper

  belongs_to :host
  belongs_to :archive

  # File
  field :file # Full File Path / File Load
  field :name # Base File Name
  field :path # Symbol Name Helper
  field :size # Total File Size

  # Classification
  field :kind # File Information
  field :type # Shared Naming Convention (Based off of SOS)
  field :log # Parsable / Include in log formats

  # Preloader Flags
  field :parsed # Flag for Parsing
  field :result, dump: false # Processed Data
  field :result_fields # All available fields

  def friendly_name
    "#{archive.friendly_name.pastel(:blue)} #{name.pastel(:green)}"
  end

  def setup
    self.name = build_path('/')
    self.path = build_path.gsub(/[^\w\s]/, '_')
    self.size = number_to_human_size File.size(file)

    kind_collect # Set Kind and Type
    kind_setup

    save!
  end

  # Helper to make it easier to query through hashed / parsed objects
  def hash_to_a_query
    data.to_a.map { |x| x.join(': ') }
  end

  def grep(term)
    selectable = if query?
                   data
                 elsif process?
                   hash_to_a_query
                 else
                   raw_full
                 end

    selectable.select { |r| r.to_s.include?(term) }
  end

  # Processor
  def data
    process unless parsed

    result
  end

  # Processor
  def fields
    process unless parsed

    result_fields
  end

  # Readlines rather than putting whole thing into memory
  def raw
    File.foreach(file)
  end

  # Where full read is needed
  def raw_full
    File.read(file).split("\n")
  end

  # Filter / Archive Pattern Matching
  def archive?(archive_names)
    archive_names.map(&:to_s).any? { |x| archive.name.include? x }
  end

  # Helper for empty logs
  def blank?
    data.blank?
  end

  def output(print_it = true)
    if print_it
      puts raw.map(&:to_s).map(&:chomp)
    else
      raw.map(&:to_s).map(&:chomp)
    end
  end

  # Hashed values searching
  def process?
    kind != :raw && methods.include?(formatter)
  end

  # Things that can be queried (Query Helper)
  def query?
    data.instance_of?(Array) || data.instance_of?(Enumerator)
  end

  # Helper for all things that can be hash/value filtered
  def self.list
    all.select.select(&:process?)
  end

  # Helper Formatter Method
  def formatter
    "format_#{kind}".to_sym
  end

  def process(quiet = false)
    if methods.include? formatter
      spin_start("Parse #{name.pastel(:blue)} #{kind.to_s.pastel(:bright_black)} ") unless quiet
      begin
        send(formatter)
      rescue StandardError => e
        LogBot.fatal('Process', message: e.message, backtrace: e.backtrace.first)
      end
      spin_done unless quiet
    else
      LogBot.fatal('Thing', "No Formatter for #{formatter}")
    end

    self.parsed = true

    self.result_fields = field_processing

    save!
  end

  def field_processing
    if data.instance_of?(Array)
      data.select { |x| x.instance_of?(Hash) }.map(&:keys).flatten.uniq
    else
      []
    end
  rescue StandardError => e
    LogBot.fatal('Process', message: e.message, backtrace: e.backtrace.first)

    []
  end

  def query_save(results, name)
    self.archive = Archive.first
    self.result = results
    self.name = name
    self.log = true
    self.parsed = true
    self.type = 'json'
    self.kind = :json
    self.result_fields = field_processing
    save!
  end
end
