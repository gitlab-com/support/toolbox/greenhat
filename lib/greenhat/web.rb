require 'sinatra/base'

# Chart Shims
# https://github.com/ankane/chartkick/blob/master/lib/chartkick.rb
require 'chartkick/enumerable'
require 'chartkick/helper'
require 'chartkick/version'
require 'chartkick/sinatra'

# Load Web Helpers
require_all "#{File.dirname(__FILE__)}/web"

# Helper
module GreenHat
  # Web Helper
  module Web
    # Template Helper
    class MyApp < Sinatra::Base
      helpers Chartkick::Helper
      helpers ::WebHelpers
      helpers ::TimeHelpers

      configure do
        set :server, :puma
        set :port, 4567
        set :quiet, true
        set :server_settings, Silent: true
      end

      get '/' do
        slim :index
      end

      get '/chart' do
        slim :chart
      end

      get '/charts/:chart_type' do
        # TODO: Better validation?
        slim params[:chart_type].to_sym
      end

      get '/chart/time' do
        @index = time_series unless params[:query].empty?
        slim :time
      end

      get '/chart/chartkick.js' do
        content_type 'application/javascript'
        File.read("#{File.dirname(__FILE__)}/views/chartkick.js")
      end
    end

    def self.setup
      Archive.all.map(&:host_create).map(&:save!)
    end

    def self.toggle
      if alive?
        LogBot.info('Stopping Web')
        stop
      else
        start
      end
    end

    def self.start
      setup if Host.count.zero?

      return true if alive? # Prevent Dupes

      LogBot.info('Starting Web')

      @thread = Thread.new { MyApp.run! }
      # MyApp.run! # Debugging
    end

    def self.thread
      @thread
    end

    def self.alive?
      !thread.nil? && thread.alive?
    end

    def self.stop
      thread.kill if alive?
    end
  end
end
