module GreenHat
  # Helpers for Paper Render
  module PaperFlagHelper
    # --------------------------------------------------------------------------
    def truncate
      flags[:truncate]
    end

    def table_style
      if flags[:table_style] == true
        :basic
      else
        flags[:table_style]&.to_sym || :unicode
      end
    end

    def stats
      flags[:stats]
    end
    # --------------------------------------------------------------------------
  end
end
