module GreenHat
  # Helpers for Paper Render
  module PaperHelpers
    # =========================================================================
    # Entrypoint
    # =========================================================================
    def process
      LogBot.debug('Entry Show', data.class) if ENV['DEBUG']
      case data
      when Hash then render_table
      when Float, Integer
        self.output = format_table_entry(data)
        # Ignore Special Formatting for Strings / Usually already formatted
      when String
        self.output = data
      when Array
        # Check for Nested Arrays (Array Table)
        if data.first.instance_of? Array
          return if data.flatten.empty? # Don't try to render empty arrays

          render_array_table(data)
        else
          self.output = data
        end
      else
        LogBot.warn('Shell Show', "Unknown #{data.class}")
        self.output = data
      end
    end

    # --------------------------------------------------------------------------
    def render_table
      formatted = data.to_h { |key, entry| [key, format_table_entry(entry, key)] }

      # Pre-format Entry
      table = TTY::Table.new(header: formatted.keys, rows: [formatted], orientation: :vertical)

      LogBot.debug('Rendering Entries') if ENV['DEBUG']
      self.output = table.render(table_style, padding: [0, 1, 0, 1], multiline: true) do |renderer|
        renderer.border.style = :cyan
      end

      # Line breaks for basic tables
      self.output += "\n\n" if table_style == :basic
    rescue StandardError => e
      if ENV['DEBUG']
        LogBot.warn('Table', message: e.message)
        ap e.backtrace
      end

      self.output = [
        data.ai,
        ('_' * (TTY::Screen.width / 3)).pastel(:cyan),
        "\n"
      ].join("\n")
    end

    # --------------------------------------------------------------------------
    # Format Table Entries
    def format_table_entry(entry, key = nil)
      formatted_entry = case entry
                        # Rounding
                        when Float, Integer || entry.numeric?
                          flags.key?(:round) ? entry.to_f.round(flags.round).ai : entry.ai

                        # General Inspecting
                        when Hash then entry.ai(ruby19_syntax: true)

                        # Arrays often contain Hashes. Dangerous Recursive?
                        when Array
                          entry.map { |x| format_table_entry(x) }.join("\n")
                        when Time
                          entry.to_s.pastel(:bright_white)

                        # Default String Formatting
                        else
                          ShellHelper::StringColor.do(key, entry)
                        end

      # Stats truncation handled separately
      if truncate && !stats
        entry_truncate(formatted_entry)
      else
        formatted_entry
      end
    rescue StandardError => e
      if ENV['DEBUG']
        LogBot.warn('Table Format Entry', message: e.message)
        ap e.backtrace
      end
    end

    # --------------------------------------------------------------------------
    def entry_truncate(entry)
      # Ignore if Truncation Off
      return entry if truncate.zero?

      # Only truncate large strings
      return entry unless entry.instance_of?(String) && entry.size > truncate

      # Include  '...' to indicate truncation
      "#{entry.to_s[0..truncate]} #{'...'.pastel(:bright_blue)}"
    end

    # --------------------------------------------------------------------------
    def render_array_table(entry)
      header, rows = entry
      table = TTY::Table.new(header:, rows:)

      LogBot.debug('Rendering Entries') if ENV['DEBUG']
      self.output = table.render(table_style, padding: [0, 1, 0, 1], multiline: true) do |renderer|
        renderer.border.style = :cyan
      end

      # Line breaks for basic tables
      self.output += "\n" if table_style == :basic
    end
    # --------------------------------------------------------------------------
  end
end
