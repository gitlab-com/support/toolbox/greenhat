module GreenHat
  # Root Level Shell
  module Shell
    def self.pry
      # rubocop:disable Lint/Debugger
      binding.pry
      # rubocop:enable Lint/Debugger
    end

    def self.settings
      Settings.configure_settings
    end

    def self.web
      # Load Required Files
      require 'greenhat/web'

      GreenHat::Web.toggle
    end

    # Append Log Location so follow up commands are executed there
    def self.default(raw_list = [])
      Cli.move_shell Shell::Log
      Log.default(raw_list)
    end

    def self.grep(raw_list = [])
      Grep.grep(raw_list)
    end

    def self.df
      Disk.df
    end

    def self.quit(_raw = {})
      exit 0
    end

    def self.ps(raw = {})
      Process.ps raw
    end

    def self.netstat
      Network.netstat
    end

    def self.free
      Memory.free
    end

    def self.load_local(prompt = true)
      return false if prompt && !TTY::Prompt.new.yes?('Load local Omnibus GitLab Instance files?')

      archive_path = "#{$TMP}/#{Time.now.to_i}_local"
      Dir.mkdir(archive_path)

      archive = Archive.new(name: archive_path, path: archive_path)

      file_list = Dir['/var/log/gitlab/*/current'] + Dir['/var/log/gitlab/*/*.log']

      file_list.each do |file|
        next if File.empty?(file)

        puts "- Loading #{file.pastel(:green)}"

        archive.things_create(file:).setup
      end
    end

    def self.debug
      ENV['DEBUG'] = if ENV['DEBUG']
                       puts "GreenHat Debug Logging #{'Off'.pastel(:red)}"
                       nil
                     else
                       puts "GreenHat Debug Logging #{'On'.pastel(:green)}"
                       'true'
                     end
    end

    def self.quiet
      Cli.quiet_toggle

      if Cli.quiet
        puts "GreenHat Quiet Logging #{'Off'.pastel(:red)}"
        nil
      else
        puts "GreenHat Quiet Logging #{'On'.pastel(:green)}"
        'true'
      end
    end

    def self.uptime
      Shell::Cat.default ['uptime']
    end

    def self.ip_address
      Shell::Cat.default ['ip_address']
    end

    def self.ifconfig
      Shell::Cat.default ['ifconfig']
    end

    def self.gitlab_rb
      Shell::Cat.default ['gitlab/gitlab.rb', '--page']
    end

    def self.hostname
      Shell::Cat.default ['hostname']
    end

    def self.uname
      Shell::Cat.default ['uname']
    end

    def self.history_clear
      Settings.cmd_history_clear
    end

    def self.history
      Settings.cmd_history_clean.each_with_index do |line, i|
        puts "#{i.to_s.ljust(3).pastel(:magenta)} #{line}"
      end
    end

    def self.ls(raw = [])
      if raw.empty?
        GreenHat::Cli.help(false)
      else
        Cli.move_shell Shell::Log
        Log.ls(raw)
      end
    end

    def self.ll
      GreenHat::Cli.help(false)
    end

    def self.version
      puts "#{'GreenHat'.pastel(:green)}: #{GreenHat::VERSION.pastel(:blue)}"
      puts ' - https://gitlab.com/gitlab-com/support/toolbox/greenhat'.pastel(:cyan)
    end
  end
end

# Load All Sub Files
require_all "#{File.dirname(__FILE__)}/accessors"
require_all "#{File.dirname(__FILE__)}/shell"
