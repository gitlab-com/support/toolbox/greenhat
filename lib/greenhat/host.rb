# Individual Host Helper
class Host < Teron
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::NumberHelper

  field :name

  belongs_to :archive

  # def load(file)
  #   @file = file
  #   self.files = Archive.load(file)
  #   save!
  # end

  def archive_name
    archive.friendly_name
  end

  def find_file(file_name)
    files.find { |x| x.name == file_name }
  end

  def things
    archive.things
  end

  def find_thing(param)
    things.find { |x| x.name.include? param }
  end

  def icon
    release = find_thing('etc/os-release').data['ID_LIKE']

    if release.include? 'ubuntu'
      'fa-ubuntu'
    elsif release.include? 'suse'
      'fa-suse'
    elsif release.include? 'redhat'
      'fa-redhat'
    elsif release.include? 'centos'
      'fa-centos'
    else
      'fa-linux'
    end
  end

  # ----------------------------
  # Helpers
  # ----------------------------
  def percent(value, total)
    (value.to_i / total.to_f).round(2) * 100
  end

  def systemctl_color(entry)
    case entry.status
    when 'enabled'  then :green
    when 'static'   then :orange
    when 'disabled' then :red
    else
      :grey
    end
  end
  # ---------------------------

  def gitlab_version
    file = find_thing 'gitlab/version-manifest.json'
    return nil unless file

    file.data.build_version
  end

  def uptime
    file = find_thing 'uptime'
    return nil unless file

    file.raw
  end

  def uname
    file = find_thing 'uname'
    return nil unless file

    file.raw.join
  end

  def cpuinfo
    file = find_thing 'lscpu'
    return nil unless file

    file.data
  end

  def cpu_speed
    file = find_thing 'lscpu'
    return nil unless file

    details = file.raw.find { |x| x.include? 'MHz' }
    details.reverse.split(' ', 2).map(&:reverse).reverse
  end

  def total_memory
    file = find_thing 'free_m'
    return nil unless file

    value = file.raw.dig(1, 1).to_i
    number_to_human_size(value * 1024 * 1024)
  end

  def free_m
    file = find_thing 'free_m'
    return nil unless file

    file.raw
  end

  def df_h
    file = find_thing 'df_hT'
    return nil unless file

    file.raw
  end

  def netstat
    file = find_thing 'netstat'
    return nil unless file

    file.raw
  end

  def ulimit
    file = find_thing 'ulimit'
    return nil unless file

    results = file.raw.map do |entry|
      {
        value: entry.split[-1],
        details: entry.split('  ').first
      }
    end

    results.sort_by { |x| x[:details].downcase }
  end

  def processes
    file = find_thing 'ps'
    return nil unless file

    headers = file.raw.first.split(' ', 11)
    list = file.raw[1..].each.map do |row|
      row.split(' ', 11).each_with_index.with_object({}) do |(v, i), obj|
        obj[headers[i]] = v
      end
    end
    { headers:, list: }
  end

  def systemctl_unit_files
    file = find_thing 'systemctl_unit_files'
    return nil unless file

    all = file.raw[1..-2].map do |x|
      unit, status = x.split
      { unit:, status: }
    end

    all.reject! { |x| x[:unit].nil? }
    all.sort_by(&:unit)
  end
end
