# Top level namespace
module GreenHat
  # Helper for all things user environment / settings / history
  module Settings
    def self.settings
      # round: [2],
      # page: [:true] Automatic,
      @settings ||= {
        assume_json: true,
        fuzzy_file_match: true,
        assume_raw: false,
        truncate: TTY::Screen.width * 4, # Dynamic Setting
        color: true
      }
    end

    def self.configure_choices
      [

        { name: 'Assume JSON - auto injest json', value: :assume_json },
        { name: 'Assume Raw - do not attempt unknown parsing', value: :assume_raw },
        { name: 'Fuzzy File Match - all partial matching', value: :fuzzy_file_match },
        { name: 'Color Output', value: :color }
      ]
    end

    def self.configure_settings
      puts 'Current Settings'.pastel(:green)
      ap @settings.except(:truncate)
      puts

      prompt = TTY::Prompt.new
      choice = prompt.select('Change Which Setting?', configure_choices, filter: true)

      setting = configure_choices.find { |x| x.value == choice }
      value = prompt.yes?("#{setting[:name]} (Y/n)")
      @settings[choice] = value

      File.write(settings_file, Oj.dump(@settings.except(:truncate)))
    end

    def self.assume_json?
      settings.assume_json
    end

    def self.assume_raw?
      settings.assume_raw
    end

    # Allow for future disabling of color output
    def self.color?
      settings.color
    end

    # Load User Settings and drop them into settings
    def self.settings_load
      return true unless File.exist?(settings_file)

      Oj.load(File.read(settings_file)).each do |key, value|
        settings[key] = value
      end
    end

    def self.settings_file
      "#{dir}/settings.json"
    end

    # Set any Log Arguments that weren't set otherwise | Conditional assign applied on bool
    # rubocop:disable Style/GuardClause
    def self.default_log_flags(flags, skip_flags)
      flags[:round] = settings.round if !(skip_flags.include?(:round) || flags.key?(:round)) && settings.round

      flags[:page] = settings.page if !(skip_flags.include?(:page) || flags.key?(:page)) && settings.page

      flags[:truncate] = settings.truncate unless skip_flags.include?(:truncate) || flags.key?(:truncate)

      # Fuzzy File Match
      unless skip_flags.include?(:fuzzy_file_match) || flags.key?(:fuzzy_file_match)
        flags[:fuzzy_file_match] = settings.fuzzy_file_match
      end
    end
    # rubocop:enable Style/GuardClause

    def self.start
      FileUtils.mkdir_p dir
      FileUtils.mkdir_p reports_dir

      # Load User Settings
      settings_load

      # CMD History Loading / Tracking
      File.write(cmd_file, "\n") unless File.exist? cmd_file
    end

    def self.dir
      "#{Dir.home}/.greenhat"
    end

    def self.reports_dir
      "#{dir}/reports"
    end

    # ----------------------------------
    # Command History
    def self.cmd_history
      "#{dir}/cmd_history"
    end

    def self.cmd_file
      "#{dir}/cmd_history"
    end

    def self.cmd_history_clean
      File.read(cmd_file).split("\n")
    end

    def self.cmd_history_clear
      File.write(cmd_file, "\n")
    end

    def self.cmd_add(line)
      File.write(cmd_file, line, File.size(cmd_file), mode: 'a')
    end

    # ----------------------------------

    # File Load History
    def self.history_file
      "#{dir}/file_history"
    end
  end
end
