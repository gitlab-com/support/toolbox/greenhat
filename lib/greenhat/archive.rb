# Top level namespace
module GreenHat
  # Archive Manipulator
  module ArchiveLoader
    def self.load(archive_path)
      path = "#{$TMP}/#{SecureRandom.uuid}"
      Dir.mkdir path

      # Initially Copy file into tmpdir
      FileUtils.cp_r(archive_path.to_s, "#{path}/")

      # Extract Everything
      loop do
        archive_list = Find.find(path).reject { |x| File.directory? x }.select { |y| archive? y }
        break if archive_list.empty?

        archive_list.each do |archive|
          # Different File Type Handler
          unpack(archive, path)
        end
      end

      # Ignore Directories and Symlinks
      list = Find.find(path).reject { |x| File.directory?(x) || File.symlink?(x) }

      # Ignore Empty Files
      list.reject! { |x| File.empty?(x) }

      archive = Archive.new(name: archive_path, path:)
      archive.save

      list.each do |file|
        # Ensure Valid Content
        next if missing_command(file)

        # Thread.new do
        thing = archive.things_create(file:)
        thing.setup
        # end
      end
    end

    # Ignore No Commands
    # rubocop:disable Style/SymbolProc
    def self.missing_command(file)
      first_line = File.open(file) { |f| f.readline }
      ['command not found', ': not found'].any? { |x| first_line.include? x }
    end
    # rubocop:enable Style/SymbolProc

    # Handle Different Types of Archives
    def self.unpack(archive_path, path)
      decompressed_successfully = false

      case File.extname archive_path
      when '.tgz', '.tar'
        base_path = archive_path.gsub(File.basename(archive_path), '')
        `bsdtar -xf "#{archive_path}" -C #{base_path}`

        decompressed_successfully = $CHILD_STATUS.success?
        FileUtils.rm(archive_path) if decompressed_successfully
      when '.gz'
        `gzip -d "#{archive_path}"`

        decompressed_successfully = $CHILD_STATUS.success?
      when '.zip'
        base_path = archive_path.gsub(File.basename(archive_path), '')
        `unzip -o -d "#{base_path}" #{archive_path}`

        decompressed_successfully = $CHILD_STATUS.success?
        FileUtils.rm(archive_path) if decompressed_successfully
      when '.bz2'
        `bzip2 -d #{archive_path}`

        decompressed_successfully = $CHILD_STATUS.success?
      when '.s'
        # Find Original Directory, Split Path, Rename to .gz
        base_path = archive_path.gsub(File.basename(archive_path), '')
        FileUtils.mv(archive_path, "#{base_path}/#{File.basename(archive_path, '.s')}.gz")
      else
        FileUtils.cp(archive_path, "#{path}/#{archive_path}")
      end

      # if the archive is corrupt and doesn't uncompress properly, greenhat will keep looping on it endlessly,
      #  trying to decompress it. let's rename it on failure so it stops trying, but the SE can still try and
      #  extract it themselves.
      return if decompressed_successfully

      puts "#{'Decompress failed'.pastel(:red)}: #{archive_path.pastel(:green)}"
      puts 'Renaming file so we can continue.'.pastel(:yellow)
      FileUtils.mv(archive_path, "#{archive_path}.broken")
    end

    def self.archive?(file_name)
      archive_types.any? do |type|
        file_name.match?(/(\.#{type})$/)
      end
    end

    def self.archive_types
      %w[s gz tar tgz zip bz2]
    end
    # -- Archive Loader ----------------------------------------------
  end
  # Archive End
end

# Archive Parent
class Archive < Teron
  has_one :host
  has_many :things

  field :path
  field :name

  # TODO: Fix from Number of Files / Needed?
  def friendly_name
    # Difficult with multiple Archives
    # File.basename(name, '.tar.gz').gsub('gitlabsos.', '').split('_', 2).first

    File.basename(name, File.extname(name)).gsub('gitlabsos.', '')
  end

  def inspect
    "#<Archive name: '#{name}'>"
  end

  def report(flags)
    GreenHat::Report.new(self, flags)
  end

  def report_markdown
    GreenHat::ReportMarkdown.new(self)
  end

  # Helper for finding thing
  def thing?(thing_name = 'gitlab-workhorse/current')
    things.find { |x| x.name == thing_name }
  end
end
