#  gitlab/gitlab.rb

# Top level namespace
module GreenHat
  # General Info Helpers at the beginning of the shell
  module Motd
    def self.start
      show_version
      gitlab_rb
      fast_stats

      puts
    end

    def self.check(color = :green)
      '✔'.pastel(color)
    end

    def self.show_version
      version = Thing.find_by(type: 'gitlab/version-manifest.json')

      return if version.nil?

      version.process(true)
      puts "#{'GitLab Version'.ljust(24, ' ').pastel(:cyan)} #{version.data.build_version.pastel(:yellow)}"
    end

    def self.gitlab_rb
      file = Thing.find_by(name: 'gitlab/gitlab.rb')

      return if file.nil?
      return unless file.raw_full.length > 3

      puts 'gitlab/gitlab.rb'.ljust(25, ' ').pastel(:bright_black) + check
    end

    def self.fast_stats
      return if Thing.all.select { |x| x.name.include? 'fast-stats' }.count.zero?

      puts 'FastStats Summaries'.ljust(25, ' ').pastel(:bright_black) + check
    end

    # =============================================================
  end
  # =============================================================
end
