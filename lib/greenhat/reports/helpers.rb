# Disable for executor
# rubocop:disable Style/MixinUsage
include ActionView::Helpers::DateHelper
include ActionView::Helpers::NumberHelper
include GreenHat::Reports::Shared
# rubocop:enable Style/MixinUsage

def add(*params)
  GreenHat::Reports.add(*params)
end

# ==============================================================================
# General Report Helpers
# ==============================================================================
# Supress output
def quiet!
  GreenHat::Cli.quiet!
end

# Supress output
def page(value = true)
  ENV['GREENHAT_PAGE'] = value == true ? 'true' : 'false'
end

# Attempt Autoload raw/json
def no_prompt!
  Settings.settings[:assume_raw] = true
end

# Default Header
def header(text, color = :bright_yellow)
  add(:header, text, color)
end

# Archive Header
def archive_header(color = :blue)
  add(:archive_header, color)
end

# Raw
def cat(name, show_output = true, &block)
  add(:cat, name, show_output, block)
end

# Parsed
def info(name, show_output = true, &block)
  add(:info, name, show_output, block)
end

# Log Query
def query(query, show_output = true, &block)
  add(:query, query, show_output, block)
end

def query_if_exists(query, show_output = true, &block)
  add(:query_if_exists, query, show_output, block)
end

# Log Query / Assume Format
def query_format(query, show_output = true, &block)
  add(:query_format, query, show_output, block)
end

# FastStats Helper
def faststats(query, subcmd = '')
  add(:faststats, query, subcmd)
end

# New Line Helper
def br
  add :br
end

def puts(text = '')
  add :puts, text
end

def show(text = '')
  add :show, text
end

# Help
def help(&block)
  add(:help, block)
end

# Helper to Print GitLab Version
def gitlab_version
  add :gitlab_version
end

# =================================================================
# These are here to allow flags/args to be used outside of blocks
# =================================================================
def args
  _file_list, _flags, args = GreenHat::Args.parse(ARGV)
  args
end

def flags
  _file_list, flags, _args = GreenHat::Args.parse(ARGV)

  flags
end
# =================================================================
