module GreenHat
  module ShellHelper
    # Log Helpers
    module Reports
      # Make Running more consistent
      def self.run(file:, args:, flags:, raw_args:)
        report = GreenHat::Reports::Builder.new(file:, args:, flags:, raw_args:)

        # Fuzzy Match Archive Names
        archives = Archive.all
        if flags[:archive] && !flags[:archive].empty?
          archives.select! do |a|
            flags[:archive].any? do |x|
              a.name.include? x.to_s
            end
          end
        end

        output = archives.map do |archive|
          runner = GreenHat::Reports::Runner.new(
            archive:,
            store: report.store.clone,
            flags:,
            args:,
            raw_args:
          )

          runner.run!

          runner
        end

        # Check for Pagination
        flags[:page] = ENV['GREENHAT_PAGE'] == 'true' if ENV['GREENHAT_PAGE']

        ShellHelper.show(output.flat_map(&:output), flags)
      end

      # Collect everything from Built-in and local reports
      def self.list
        path = "#{File.dirname(__FILE__)}/reports"
        Dir["#{path}/*.rb"] + Dir["#{GreenHat::Settings.reports_dir}/*.rb"]
      end

      # Collect everything from Built-in and local reports
      def self.auto_complete_list
        path = "#{File.dirname(__FILE__)}/reports"
        list = Dir["#{path}/*.rb"] + Dir["#{GreenHat::Settings.reports_dir}/*.rb"]

        list.map do |entry|
          File.basename(entry, '.rb')
        end
      end
      # --
    end
  end
end
