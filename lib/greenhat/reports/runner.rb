# GreenHat Namespace
module GreenHat
  # Reports NameSpace
  module Reports
    # This guy actually runs the reports
    class Runner
      include ActionView::Helpers::DateHelper
      include ActionView::Helpers::NumberHelper

      include InternalMethods
      include Shared
      attr_accessor :store, :archive, :flags, :args, :raw_args, :output

      # Make Pry Easier
      def inspect
        "#<Runner archive: '#{archive}'>"
      end

      def initialize(archive:, store:, flags:, args:, raw_args:)
        self.store = store
        self.archive = archive
        self.flags = flags
        self.args = args
        self.raw_args = raw_args
        self.output = []

        # Don't render paper new lines
        # flags[:skip_new_line] = true
      end

      # Main Entrypoint for Triggering Reports Run
      def run!
        if flags.help
          # Check for Help Flag
          help_flag = store.find { |x| x.first == :help }
          help_flag ? run_entry(help_flag) : puts('No Help :(')
          return true
        end

        store.each do |entry|
          next if entry.first == :help # Skip if no flags

          run_entry(entry)
        end

        puts
      rescue StandardError => e
        puts e.message
        puts e.backtrace
      end

      # Helper to evaluate Entry
      def run_entry(entry)
        method = entry[0]
        send(method, *entry[1..])
      end
    end
  end
end
