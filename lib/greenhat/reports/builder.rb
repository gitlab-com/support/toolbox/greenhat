# GreenHat Namespace
module GreenHat
  # Reports Parent Class
  module Reports
    # Used for within GreenHat Use
    class Builder
      include GreenHat::Reports::Shared

      attr_accessor :store, :flags, :args, :raw_args

      def initialize(file:, flags:, args:, raw_args:)
        self.store = []
        self.flags = flags
        self.args = args
        self.raw_args = raw_args
        instance_eval File.read(file)
      rescue StandardError => e
        print "#{e.message}\n"
        print e.backtrace[0..4].join("\n").pastel(:red)
      end

      # Adding Methods
      def add(*params)
        store.push params
      end

      # ------------------------------------------------------------------------
      # Store Methods
      # ------------------------------------------------------------------------

      def quiet!
        GreenHat::Cli.quiet!
      end

      def no_prompt!
        # TODO: Should this do anything?
      end

      # Default Header
      def header(text, color = :bright_yellow)
        add(:header, text, color)
      end

      # Archive Header
      def archive_header(color = :blue)
        add(:archive_header, color)
      end

      # Raw
      def cat(name, show_output = true, &block)
        add(:cat, name, show_output, block)
      end

      # Parsed
      def info(name, show_output = true, &block)
        add(:info, name, show_output, block)
      end

      # New Line Helepr
      def br
        add :br
      end

      def puts(text = '')
        add :puts, text
      end

      # Helper to Print GitLab Version
      def gitlab_version
        add :gitlab_version
      end

      # Help
      def help(&block)
        add(:help, block)
      end

      def show(text = '')
        add :show, text
      end

      # Log Query
      def query(query, show_output = true, &block)
        add(:query, query, show_output, block)
      end

      # Log Query / Assume Format
      def query_format(query, show_output = true, &block)
        add(:query_format, query, show_output, block)
      end

      def query_if_exists(query, show_output = true, &block)
        add(:query_if_exists, query, show_output, block)
      end

      # FastStats Helper
      def faststats(query, subcmd = '')
        add(:faststats, query, subcmd)
      end
      # ------------------------------------------------------------------------
    end
  end
end
