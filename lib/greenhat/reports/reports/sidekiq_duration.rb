quiet!
archive_header

fields = %w[
  duration_s
  db_duration_s
  redis_duration_s
  gitaly_duration_s
  db_main_duration_s
  db_replica_duration_s
  redis_queues_duration_s
  elasticsearch_duration_s
]

query_format "sidekiq/current --percentile --slice=#{fields.join(',')} --round"
