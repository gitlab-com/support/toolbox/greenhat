quiet!
archive_header
header 'OS'

cat 'hostname' do |data|
  indent "#{'Hostname:'.pastel(:cyan)} #{data.join}"
end

info 'etc/os-release' do |data|
  ident = "[#{data.ID}] ".pastel(:bright_black)
  pretty_name = data.PRETTY_NAME
  "  #{'Distro'.pastel(:cyan)} #{ident} #{pretty_name}"
end

cat 'uname' do |data|
  value, build = data.first.split[2].split('-')
  build = "(#{build})".pastel(:bright_black)
  "  #{'Kernel'.pastel(:cyan)} #{value} #{build}"
end

cat 'uptime' do |data|
  init = data.join.split(',  load average').first.strip.split('up ', 2).last
  "  #{'Uptime'.pastel(:cyan)} #{init}"
end

# Load Average
info 'lscpu' do |data|
  uptime = archive.thing? 'uptime'
  return unless uptime

  cpu_count = data['CPU(s)'].to_i
  intervals = uptime.data.first.split('load average: ', 2).last.split(', ').map(&:to_f)

  intervals_text = intervals.map do |interval|
    value = percent(interval, cpu_count)
    color = value > 100 ? :red : :green
    [
      interval,
      ' (',
      "#{value}%".pastel(color),
      ')'
    ].join
  end

  "  #{'LoadAvg'.pastel(:cyan)} #{"[CPU #{cpu_count}] ".pastel(:bright_white)} #{intervals_text.join(', ')}"
end

br
header 'Memory'

info('meminfo') do |data|
  total = human_size_to_number(data['MemTotal'])
  free = human_size_to_number(data['MemFree'])
  used = percent((total - free), total)

  [
    '  ',
    ljust('Usage', 13, :yellow),
    '['.pastel(:bright_black),
    '='.pastel(:green) * (used / 2),
    ' ' * (50 - (used / 2)),
    ']'.pastel(:bright_black),
    " #{100 - percent(free, total)}%".pastel(:green) # Inverse
  ].join
end

info('free_m', false) do |data|
  free = data.first

  unless free.total.blank?
    size = number_to_human_size(free.total.to_i * (1024**2))
    puts "  #{ljust('Total', 12, :cyan)} #{size}"
  end

  unless free.used.blank?
    size = number_to_human_size(free.used.to_i * (1024**2))

    puts "  #{ljust('Used', 12, :yellow)} #{size}"
  end

  unless free.free.blank?
    size = number_to_human_size(free.free.to_i * (1024**2))
    puts "  #{ljust('Free', 12, :blue)} #{size}"
  end

  unless free.available.blank?
    size = number_to_human_size(free.available.to_i * (1024**2))
    puts "  #{ljust('Available', 12, :green)} #{size}"
  end

  br
  data.map { |x| GreenHat::Memory.memory_row x }.each do |row|
    puts indent(row)
  end
end

br
header 'Disk'

info 'df_hT' do |data|
  indent GreenHat::Disk.report_output(data)
end

br
header 'GitLab'
info 'gitlab/version-manifest.json' do |data|
  indent("#{'Version'.pastel(:cyan)}: #{data.build_version}")
end

info 'gitlab_status' do
  indent(GreenHat::GitLab.services(archive, 3), 3)
end

br
puts indent('Errors'.pastel(:cyan))

query_if_exists 'gitlab-rails/production_json.log --status=500' do |data|
  color = data.count.zero? ? :green : :red
  indent("#{ljust('Production:', 14, :magenta)} #{data.count.to_s.pastel(color)}", 4)
end

app_query = 'gitlab-rails/application_json.log --message!="Cannot obtain an exclusive lease" --severity=error'
query_if_exists app_query do |data|
  color = data.count.zero? ? :green : :red
  indent("#{ljust('Application:', 14, :magenta)} #{data.count.to_s.pastel(color)}", 4)
end

query_if_exists 'sidekiq/current --severity=error' do |data|
  color = data.count.zero? ? :green : :red
  indent("#{ljust('Sidekiq:', 14, :magenta)} #{data.count.to_s.pastel(color)}", 4)
end

query_if_exists 'gitlab-rails/api_json.log --status=500' do |data|
  color = data.count.zero? ? :green : :red
  indent("#{ljust('API:', 14, :magenta)} #{data.count.to_s.pastel(color)}", 4)
end

query_if_exists 'gitaly/current --level=error' do |data|
  color = data.count.zero? ? :green : :red
  indent("#{ljust('Gitaly:', 14, :magenta)} #{data.count.to_s.pastel(color)}", 4)
end

query_if_exists 'gitlab-workhorse/current --level=error' do |data|
  color = data.count.zero? ? :green : :red
  indent("#{ljust('Workhorse:', 14, :magenta)} #{data.count.to_s.pastel(color)}", 4)
end

query_if_exists 'gitlab-rails/exceptions_json.log' do |data|
  color = data.count.zero? ? :green : :red
  indent("#{ljust('Exceptions:', 14, :magenta)} #{data.count.to_s.pastel(color)}", 4)
end

query_if_exists('praefect/current --level=error') do |data|
  color = data.count.zero? ? :green : :red
  indent("#{ljust('Praefect:', 14, :magenta)} #{data.count.to_s.pastel(color)}", 4)
end

query_if_exists('gitlab-pages/current --level=error') do |data|
  color = data.count.zero? ? :green : :red
  indent("#{ljust('Pages:', 14, :magenta)} #{data.count.to_s.pastel(color)}", 4)
end

query_if_exists 'gitlab-rails/geo.log --severity=error' do |data|
  color = data.count.zero? ? :green : :red
  indent("#{ljust('Geo:', 14, :magenta)} #{data.count.to_s.pastel(color)}", 4)
end

query_if_exists 'gitlab-rails/elasticsearch.log --severity=error' do |data|
  color = data.count.zero? ? :green : :red
  indent("#{ljust('Elasticsearch:', 14, :magenta)} #{data.count.to_s.pastel(color)}", 4)
end
