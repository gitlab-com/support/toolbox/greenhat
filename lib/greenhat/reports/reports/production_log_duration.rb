quiet!
archive_header

fields = %w[
  duration_s
  cpu_s
  db_duration_s
  gitaly_duration_s
  redis_duration_s
  target_duration_s
  view_duration_s
  db_main_duration_s
  db_main_replica_duration_s
  db_primary_duration_s
  db_replica_duration_s
  external_http_duration_s
  redis_cache_duration_s
  queue_duration_s
  redis_shared_state_duration_s
]

query_format "production_json.log --percentile --slice=#{fields.join(',')} --round"
