quiet!

puts '**OS**'
br

puts '```'
cat 'hostname' do |data|
  "Hostname: #{data.join}"
end

info 'etc/os-release' do |data|
  ident = "[#{data.ID}] "
  pretty_name = data.PRETTY_NAME
  "  Distro: #{ident} #{pretty_name}"
end

cat 'uname' do |data|
  value, build = data.first.split[2].split('-')
  build = "(#{build})"
  " Kernel: #{value} #{build}"
end

cat 'uptime' do |data|
  init = data.join.split(',  load average').first.strip.split('up ', 2).last
  "  Uptime: #{init}"
end

# Load Average
info 'lscpu' do |data|
  uptime = archive.thing? 'uptime'
  return unless uptime

  cpu_count = data['CPU(s)'].to_i
  intervals = uptime.data.first.split('load average: ', 2).last.split(', ').map(&:to_f)

  intervals_text = intervals.map do |interval|
    value = percent(interval, cpu_count)
    [
      interval,
      ' (',
      "#{value}%",
      ')'
    ].join
  end

  "  LoadAvg: [CPU #{cpu_count}] #{intervals_text.join(', ')}"
end
puts '```'

br
puts '**Memory**'
br

puts '```'
info('free_m', false) do |data|
  free = data.first

  unless free.total.blank?
    size = number_to_human_size(free.total.to_i * (1024**2))
    puts "  #{ljust('Total', 12)} #{size}"
  end

  unless free.used.blank?
    size = number_to_human_size(free.used.to_i * (1024**2))

    puts "  #{ljust('Used', 12)} #{size}"
  end

  unless free.free.blank?
    size = number_to_human_size(free.free.to_i * (1024**2))
    puts "  #{ljust('Free', 12)} #{size}"
  end

  unless free.available.blank?
    size = number_to_human_size(free.available.to_i * (1024**2))
    puts "  #{ljust('Available', 12)} #{size}"
  end

  # br
  # data.map { |x| GreenHat::Memory.memory_row x }.each do |row|
  #   puts row
  # end
end
puts '```'

br
puts '**Disks**'
br

puts '```'
info 'df_hT' do |data|
  GreenHat::Disk.markdown_format(data, false, 3)
end
puts '```'

br
puts '**GitLab**'
info 'gitlab/version-manifest.json' do |data|
  "Version: #{data.build_version}"
end

br

info 'gitlab_status' do
  GreenHat::GitLab.services_markdown(archive)
end

br
puts '**Errors**'
br

puts '```'
query 'gitlab-rails/production_json.log --status=500' do |data|
  ljust('Production', 14) + data.count.to_s
end

query 'gitlab-rails/application_json.log --message!="Cannot obtain an exclusive lease" --severity=error' do |data|
  ljust('Application', 14) + data.count.to_s
end

query 'sidekiq/current --severity=error' do |data|
  ljust('Sidekiq', 14) + data.count.to_s
end

query 'gitlab-rails/api_json.log --status=500' do |data|
  ljust('API', 14) + data.count.to_s
end

query 'gitaly/current --level=error' do |data|
  ljust('Gitaly', 14) + data.count.to_s
end

query 'gitlab-workhorse/current --level=error' do |data|
  ljust('Workhorse', 14) + data.count.to_s
end

query 'gitlab-rails/exceptions_json.log' do |data|
  ljust('Exceptions', 14) + data.count.to_s
end
puts '```'
