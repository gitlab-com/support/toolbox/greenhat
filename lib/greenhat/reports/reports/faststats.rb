archive_header

help do
  header 'FastStats'
  puts 'Run against all the things'
  br
  puts indent("#{'--filter'.pastel(:green)}  Filter for specific files")
  puts indent('Ex: --filter=gitaly,production'.pastel(:cyan), 4)
  puts indent('Ex: --filter!=api'.pastel(:cyan), 4)
  br
  puts indent("#{'--top'.pastel(:green)} switch to `fast-stats top [filename]`")
  puts indent("#{'--errors'.pastel(:green)} switch to `fast-stats errors [filename]`")
  br
  puts indent("#{'--all'.pastel(:green)} to attempt to faststats everything")
  br
  puts 'Combine all the args: `faststats --filter=sidekiq --top --errors`'
end

cmds = []
cmds.push 'top' if flags.top
cmds.push 'errors' if flags.errors
cmds.push nil if cmds.empty? # Assume Default

filters = args_select(:filter).flat_map { |x| x.value.split(',') }

if filters.empty?
  # Add Defaults
  filters = [
    'gitaly/current',
    'gitlab-rails/api_json.log',
    'gitlab-rails/production_json.log',
    'sidekiq/current'
  ]
end

filters = ['*'] if flags.all

filters.each do |filter|
  cmds.each do |cmd|
    faststats filter, cmd
  end
end
