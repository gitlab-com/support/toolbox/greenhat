quiet!
archive_header

fields = %w[
  duration_ms
  time_ms
  grpc.time_ms
  command.cpu_time_ms
  command.real_time_ms
  command.system_time_ms
  command.user_time_ms
]

query_format "gitaly/current --percentile --slice=#{fields.join(',')} --round"
