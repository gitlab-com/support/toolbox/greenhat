archive_header

help do
  header 'Error Reports'
  puts 'This will collect errors from the exceptions, production, application, api, workhorse, and sidekiq logs'
  puts 'Without arguments it will count and unique the error messages'
  br
  puts indent("#{'--verbose'.pastel(:green)} Print all of the error messages")
  puts indent("#{'--filter'.pastel(:green)}  Filter Error messages")
  puts indent('Ex: --filter=exception,production'.pastel(:cyan), 4)
  puts indent('Ex: --filter!=api'.pastel(:cyan), 4)
end

entries = [
  { header: 'Exceptions', base: 'gitlab-rails/exceptions_json.log', stats: 'exception.message,exception.class' },
  { header: 'Production', base: 'gitlab-rails/production_json.log --status=500 ',
    stats: 'exception.message,exception.class' },
  {
    header: 'Application',
    base: 'gitlab-rails/application_json.log --message!="Cannot obtain an exclusive lease" --severity=error',
    stats: 'message'
  },
  { header: 'Workhorse', base: 'gitlab-workhorse/current --level=error', stats: 'error' },
  { header: 'Sidekiq', base: 'sidekiq/current --severity=error', stats: 'message' },
  { header: 'API', base: 'gitlab-rails/api_json.log --severity=error',
    stats: 'exception.class,exception.message,status' },
  { header: 'Gitaly', base: 'gitaly/current --level=error --truncate=99', stats: 'error' },
  { header: 'Praefect', base: 'praefect/current --level=error --truncate=99', stats: 'error' },
  { header: 'Pages', base: 'gitlab-pages/current --level=error', stats: 'error' },
  { header: 'Geo', base: 'gitlab-rails/geo.log --severity=error', stats: 'message' },
  { header: 'Elasticsearch', base: 'gitlab-rails/elasticsearch.log --severity=error', stats: 'message' }
]

# Filter Logic
filters = args_select(:filter)

unless filters.empty?
  entries.select! do |entry|
    filters.any? do |filter|
      !filter.bang if filter.value.split(',').any? { |x| entry.header.downcase.include? x }
    end
  end
end

# Fuzzy Filter Match
unless raw_args.empty?
  entries.select! do |entry|
    # Create a pattern with the file and title
    globby = [entry.header.downcase, entry.base.split(' ', 2).first].join(' ')

    # Check any Matches
    raw_args.any? { |x| globby.include? x }
  end
end

# Return output
entries.each do |entry|
  query_string = flags[:verbose] ? entry.base : entry.base + " --stats=#{entry.stats}"
  query(query_string, false) do |results|
    next if results.empty?
    next if !flags[:verbose] && results.map(&:values).map(&:first).sum.zero?

    header entry.header

    show results
  end
end
