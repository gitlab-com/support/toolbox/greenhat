# GreenHat Namespace
module GreenHat
  # Reports Parent Class
  module Reports
    # Method Helper Module
    module InternalMethods
      # Archive Header Printer
      def archive_header(color = :blue)
        puts archive.friendly_name.pastel(color)
      end

      # Header Printer
      def header(text, color = :bright_yellow, bold = :bold)
        puts text.pastel(color, bold)
      end

      # Raw Print
      def cat(name, show_output, block)
        thing = archive.thing? name
        return unless thing

        output = instance_exec(thing.raw_full, &block)
        show output if show_output
      end

      # Parsed Print
      def info(name, show_output, block)
        thing = archive.thing? name
        return unless thing

        output = instance_exec(thing.data, &block)
        show output if show_output
      end

      # Help
      def help(block)
        instance_exec(&block)
      end

      # Log Query Ignore if no Files
      def query_if_exists(query, show_output, block)
        files, query_flags, query_args = GreenHat::Args.parse(Shellwords.split(query))

        query_flags[:archive] = [archive.name] # Limit query to archive
        query_flags[:combine] = true

        # Default to everything
        files = archive.things if files.empty?

        # Ignore if no files are found
        return unless ShellHelper.any_things?(files, query_flags)

        results = Query.start(files, query_flags, query_args)

        # Print and Exit of No Block / Show
        return show(results) if block.nil? && show_output

        output = instance_exec(results, &block)
        show output if show_output
      end

      # Log Query
      def query(query, show_output, block)
        files, query_flags, query_args = GreenHat::Args.parse(Shellwords.split(query))

        query_flags[:archive] = [archive.name] # Limit query to archive
        query_flags[:combine] = true

        # Default to everything
        files = archive.things if files.empty?

        results = Query.start(files, query_flags, query_args)

        # Print and Exit of No Block / Show
        return show(results) if block.nil? && show_output

        output = instance_exec(results, &block)
        show output if show_output
      end

      # Log Query / Assume Format
      def query_format(query, show_output, block)
        files, query_flags, query_args = GreenHat::Args.parse(Shellwords.split(query))
        query_flags[:archive] = [archive.name] # Limit query to archive
        query_flags[:combine] = true

        # Default to everything
        query_flags = archive.things if files.empty?

        # Filter
        results = GreenHat::Query.start(files, query_flags, query_args)

        # Print and Exit of No Block / Show
        return show(results) if block.nil? && show_output

        block_output = instance_exec(results, &block)
        show block_output if show_output
      end

      def faststats(raw, subcmd)
        return true unless TTY::Which.exist? 'fast-stats'

        files, _flags, cmd = ShellHelper::Faststats.parse(Shellwords.split(raw))

        results = ShellHelper.file_process(files) do |file|
          output = `fast-stats #{subcmd} #{cmd} #{file.file} 2>&1`
          result = $CHILD_STATUS.success?

          next unless result

          [
            file.name.pastel(:green),
            output.split("\n"),
            "\n"
          ]
        end

        show results.compact.flatten
      end

      # TODO
      # results = GreenHat::ShellHelper::Faststats.run(thing, 'top') # JSON FORMAT

      # Paper Render Helper
      def show(value)
        if value.instance_of?(Array)
          value.each do |x|
            output.push GreenHat::Paper.new(data: x, flags:).render
          end
        else
          output.push GreenHat::Paper.new(data: value, flags:).render
        end
      end

      # Line Break Helper
      def br(_ = nil)
        puts
      end

      # Store for potential Paginated Response
      def puts(text = '')
        output.push text
      end

      # Accessor Helper
      def archive
        archive
      end

      def gitlab_version
        thing = archive.thing? 'gitlab/version-manifest.json'
        return unless thing

        puts "#{'GitLab Version'.pastel(:cyan)}: #{thing.data.build_version}"
      end

      # ==============================================================================
      # General Helpers
      # ==============================================================================
      # Indentation Helper
      def indent(text, indent_value = 2)
        if indent_value.zero?
          text
        else
          "#{' ' * indent_value}#{text}"
        end
      end

      def percent(value, total)
        ((value / total.to_f) * 100).round
      end

      # Number Helper
      def human_size_to_number(value)
        GreenHat::ShellHelper.human_size_to_number value
      end
    end
  end
end
