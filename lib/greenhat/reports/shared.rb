# GreenHat Namespace
module GreenHat
  # Reports Parent Class
  module Reports
    # Method Helper Module
    module Shared
      # Indentation Helper
      def indent(text, indent_value = 2)
        if indent_value.zero?
          text
        else
          "#{' ' * indent_value}#{text}"
        end
      end

      def percent(value, total)
        ((value / total.to_f) * 100).round
      end

      # Justify Text Helper / With Color!
      def ljust(text, value = 14, color = nil)
        color ? text.ljust(value).pastel(color) : text.ljust(value)
      end

      # Number Helper
      def human_size_to_number(value)
        GreenHat::ShellHelper.human_size_to_number value
      end

      # Helper to filter arg selection
      def args_select(field)
        args.select { |x| x.field == field.to_sym }
      end

      # -=-=-=-=-=-
    end
  end
end
