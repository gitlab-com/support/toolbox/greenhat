# Top level namespace
module GreenHat
  # Replace Colorize with Pastel Monkey Patch String Shim
  # https://github.com/piotrmurach/pastel#features

  # Looping in GreenHat to allow disabling of Color
  module Color
    def self.pastel
      @pastel ||= Pastel.new
    end

    def self.decorate(args)
      Settings.color? ? pastel.decorate(*args) : args.first
    end
  end
end

# Monkey Patch
class String
  def pastel(*args)
    GreenHat::Color.decorate(args.unshift(self))
  end

  def unpastel
    GreenHat::Color.pastel.strip self
  end
end
