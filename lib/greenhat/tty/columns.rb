# frozen_string_literal: true

module TTY
  class Table
    # A module for calculating table data column widths
    #
    # Used by {Table} to manage column sizing.
    #
    # @api private
    module Columns
      # Converts column widths to array format or infers default widths
      #
      # @param [TTY::Table] table
      #
      # @param [Array, Numeric, NilClass] column_widths
      #
      # @return [Array[Integer]]
      #
      # @api public
      def widths_from(table, column_widths = nil)
        case column_widths
        when Array
          assert_widths(column_widths, table.columns_size)
          Array(column_widths).map(&:to_i)
        when Numeric
          Array.new(table.columns_size, column_widths)
        when NilClass
          # THE SHIM! Strings that are too large fail to render correctly to do an empty result
          # Set the maximum table width to half the screen size. Can't be full size due to the table headers
          LogBot.debug('TTY Column Width') if ENV['DEBUG']

          # TODO: Check Empty Results
          return [0] if table.data.empty?

          extract_widths(table.data).map { |x| x >= TTY::Screen.width ? (TTY::Screen.width * 3 / 4) : x }

        else
          raise TypeError, 'Invalid type for column widths'
        end
      end
      module_function :widths_from
    end
  end
end
