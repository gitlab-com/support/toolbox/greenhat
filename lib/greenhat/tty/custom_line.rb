module TTY
  class Reader
    # Shim Helper
    class Line
      def move_word_left
        loop do
          # Don't go past beginning
          break if @cursor.zero?

          left 1

          break if self[@cursor].blank?
        end
      end

      def move_word_right
        loop do
          right 1
          break if self[@cursor].blank?
        end
      end

      # No Tabs
      def insert(chars)
        self[@cursor] = chars unless ["\t", "\e[Z"].include?(chars)
      end
    end
  end
end
