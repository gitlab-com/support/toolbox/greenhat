# General Web Helper
module WebHelpers
  # ======================================================
  # == [ API ]
  # ======================================================
  def workhorse_request_total
    query = [
      'gitlab-workhorse/current',
      '--slice=time,status',
      '--exists=status'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    build_group_time_total(results, :status)
  end

  def workhorse_client_total
    query = [
      'gitlab-workhorse/current',
      '--slice=user_agent',
      '--exists=user_agent'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    build_count_by_occurance(results, :user_agent, 5)
  end

  def workhorse_duration
    query = [
      'gitlab-workhorse/current',
      '--slice=time,duration_ms',
      '--exists=duration_ms',
      '--duration_ms!=0',
      '--exact'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    build_percentile_list(results, :duration_ms)
  end
end
