# General Web Helper
module WebHelpers
  # ======================================================
  # == [ Production Log ]
  # gitlab-rails/production_json.log
  # ======================================================
  # def prod_avg_path_duration_series
  #   query = [
  #     'gitlab-rails/production_json.log',
  #     '--slice=time,path,duration_s'
  #   ].join(' ')

  #   results = GreenHat::ShellHelper.filter_internal(query)

  #   method_key = :path
  #   duration_key = :duration_s

  #   # Collect Total by Duration
  #   top = results.each_with_object({}) do |v, obj|
  #     obj[v[method_key]] ||= []
  #     obj[v[method_key]] << v[duration_key]

  #     obj
  #   end

  #   # Average / Round
  #   top.transform_values! { |x| (x.sum / x.size.to_f).round(1) }

  #   # Only top by duration
  #   method_calls = top_method_calls(top)
  #   results.select! { |x| method_calls.any? x[method_key] }

  #   build_group_time_avg(results, method_key, duration_key) # 5 Minutes
  # end

  def prod_status_series
    query = [
      'gitlab-rails/production_json.log',
      '--slice=time,status',
      '--exists=status'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    build_group_time_total(results, :status)
  end

  def prod_duration_series
    query = [
      'gitlab-rails/production_json.log',
      '--slice=time,path,duration_s',
      '--exists=duration_s'
    ].join(' ')

    # MS is hard
    results = GreenHat::ShellHelper.filter_internal(query)
    results.each do |r|
      r[:duration_s] = r[:duration_s] * 1000
    end

    # build_time_avg(results, :duration_s).transform_values { |x| x.round(1) }
    build_percentile_list(results, :duration_s)
  end

  def prod_duration_series_stacked
    stacks = %i[
      redis_duration_s
      view_duration_s
      gitaly_duration_s
      redis_cache_duration_s
      duration_s
      db_duration_s
      redis_shared_state_duration_s
      queue_duration_s
      db_duration_s
    ]

    query = [
      'gitlab-rails/production_json.log',
      "--slice=time,#{stacks.join(',')}"
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    output = build_stack_time_avg(results, stacks)

    # MS are hard
    output.each do |dataset|
      dataset[:data].transform_values! { |x| (x * 1000).round(1) }
    end

    output
  end

  def prod_duration_pie
    all = faststats_run('gitlab-rails/production_json.log', 'top')[:totals]

    # Grab Specifics
    totals = all.except(:count, :fails).transform_values(&:round)

    # Organize
    totals.sort_by(&:last).reverse.to_h
  end
end
