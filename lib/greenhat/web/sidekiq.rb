# General Web Helper
module WebHelpers
  # ======================================================
  # == [ Sidekiq ]
  # ======================================================
  def sidekiq_avg_duration_series
    query = [
      'sidekiq/current',
      '--slice=time,class,duration_s',
      '--duration_s>=1'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    method_key = :class
    duration_key = :duration_s

    # Collect Total by Duration
    top = results.each_with_object({}) do |v, obj|
      obj[v[method_key]] ||= []
      obj[v[method_key]] << v[duration_key]

      obj
    end

    # Average / Round
    top.transform_values! { |x| (x.sum / x.size.to_f).round(1) }

    # Only top by duration
    method_calls = top_method_calls(top)
    results.select! { |x| method_calls.any? x[method_key] }

    build_group_time_avg(results, method_key, duration_key) # 5 Minutes
  end

  def sidekiq_duration
    query = [
      'sidekiq/current',
      '--slice=time,duration_s',
      '--exists=duration_s',
      '--duration_s!=0',
      '--exact'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    build_percentile_list(results, :duration_s)
  end

  def sidekiq_errors_series
    query = [
      'sidekiq/current',
      '--slice=time,class',
      '--exists=errors'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    build_group_time_total(results, :class)
  end

  def sidekiq_job_latency
    query = [
      'sidekiq/current',
      '--slice=time,class,scheduling_latency_s',
      '--exists=scheduling_latency_s'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    build_percentile_list(results, :scheduling_latency_s)
  end
end
