# General Web Helper
module WebHelpers
  # ======================================================
  # == [ Gitaly ]
  # ======================================================
  def gitaly_avg_method_series
    query = [
      'gitaly/current',
      '--slice=time,grpc.time_ms,grpc.method',
      '--exists=grpc.time_ms',
      '--exists=time',
      '--grpc.time_ms>=1'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    method_key = :'grpc.method'
    duration_key = :'grpc.time_ms'

    # Collect Total by Duration
    top = results.each_with_object({}) do |v, obj|
      obj[v[method_key]] ||= []
      obj[v[method_key]] << v[duration_key]

      obj
    end

    # Average / Round
    top.transform_values! { |x| (x.sum / x.size.to_f).round(1) }

    # Only top by duration
    method_calls = top.sort_by(&:last)[-10..].map(&:first)
    results.select! { |x| method_calls.any? x[method_key] }

    build_group_time_avg(results, method_key, duration_key) # 5 Minutes
  end

  def gitaly_errors_series
    query = [
      'gitaly/current',
      '--slice=time,grpc.method,level',
      '--exists=time,level,time',
      '--level=error'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    build_group_time_total(results, :'grpc.method')
  end

  def gitaly_duration
    query = [
      'gitaly/current',
      '--slice=time,grpc.time_ms',
      '--exists=grpc.time_ms',
      '--exists=time',
      '--grpc.time_ms!=0',
      '--exact'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    build_percentile_list(results, :'grpc.time_ms')
  end
end
