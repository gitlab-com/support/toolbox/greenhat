# Fast Stat Helpers
module WebHelpers
  def faststats_field(file, field = :count)
    results = faststats_run(file).stats[0..10].map do |x|
      [x[:prim_field], x[field]]
    end

    # Sort and Round
    results = results.sort_by(&:last).reverse.to_h
    results.transform_values! { |v| v.round(1) }

    results
  end

  def faststats_run(file = 'sidekiq/current', cmd = '')
    GreenHat::ShellHelper::Faststats.run(Thing.find_by(type: file), cmd)
  end

  def faststats_top(file = 'sidekiq/current', kind = :project_stats)
    thing = Thing.find_by(type: file)
    results = GreenHat::ShellHelper::Faststats.run(thing, 'top')

    # -------------------------------------------------------
    # Calculate Total and Percentages
    list = results.dig(kind, :stats).sort_by { |_k, v| v[:duration] }.to_h
    count = list.values.sum { |x| x[:count] }
    # duration = list.values.sum { |x| x[:duration] }
    # -------------------------------------------------------

    # Handle Short Results
    top = results.dig(kind, :stats).sort_by { |_k, v| v[:duration] }
    top = top[-10..] if top.size >= 10

    top.to_h.map do |k, v|
      count_perc = percent(v[:count], count)
      {
        name: k,
        data: [
          [v[:duration].round, count_perc, v[:count]]
        ]
      }
    end
  end
end
