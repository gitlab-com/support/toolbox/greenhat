# General Web Helper
module WebHelpers
  # Time / Percentile without Grouping
  def build_time_percentile(results, sum, percentile = 0.90, interval = 5.minutes)
    index = build_time_index(results, interval)

    index.transform_values! { |_y| [] }

    results.each do |r|
      index[r.time.floor(interval)] << r[sum]
    end

    index.transform_values! do |l|
      l.percentile(percentile)
    end

    index
  end

  # Time / Min/Max without Grouping
  def build_time_method(results, sum, method = :max, interval = 5.minutes)
    index = build_time_index(results, interval)

    index.transform_values! { |_y| [] }

    results.each do |r|
      index[r.time.floor(interval)] << r[sum]
    end

    index.transform_values! do |l|
      next if l.empty?

      l.send(method)
    end

    index
  end

  # Helper to make rounding Values Easier
  def round_values(results, round_value = 1)
    results.transform_values { |x| x.round(round_value) }
  end

  # Helper to grab the most common desired results
  def build_percentile_list(results, key, round_value = 1)
    p99 = build_time_percentile(results, key, 0.99)
    p95 = build_time_percentile(results, key, 0.95)
    max = build_time_method(results, key, :max)
    mean = build_time_method(results, key, :mean)
    [
      { name: 'p99', data: round_values(p99, round_value) },
      { name: 'p95', data: round_values(p95, round_value) },
      { name: 'mean', data: round_values(mean, round_value) },
      { name: 'max', data: round_values(max, round_value) }
    ]
  end
end

# May be worth adding other stats at some point
# p99 = build_time_percentile(results, :duration_ms, 0.99)
# p95 = build_time_percentile(results, :duration_ms, 0.95)
# min = build_time_method(results, :duration_ms, :min)
# max = build_time_method(results, :duration_ms, :max)
# median = build_time_method(results, :duration_ms, :median)
# mean = build_time_method(results, :duration_ms, :mean)

# [
#   { name: 'p99', data: round_values(p99) },
#   { name: 'p95', data: round_values(p95) },
#   { name: 'median', data: median },
#   { name: 'mean', data: round_values(mean) },
#   { name: 'min', data: min },
#   { name: 'max', data: max }
# ]
