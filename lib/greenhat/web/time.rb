# Time Series Helpers
module TimeHelpers
  def time_series
    results = GreenHat::ShellHelper.filter_internal(params[:query])

    # Prevent exceptions on empty results
    return [] if results.empty?

    # Ensure Time
    results.select! { |x| x.key?(:time) && !x[:time].nil? }

    if !params[:avg].blank?
      build_group_time_avg(results, group_sym, avg_sym, param_interval)
    elsif !params[:group].blank?
      build_group_time_total(results, group_sym, param_interval)
    else
      build_time_total(results, param_interval)
    end
  end

  def param_interval
    if params[:interval]
      params[:interval].to_i.minutes
    else
      5.minutes
    end
  end

  def group_sym
    params[:group].strip.to_sym
  end

  def avg_sym
    params[:avg].strip.to_sym
  end
end
