# Shim
module Chartkick
  class << self
    attr_accessor :content_for, :options
  end
  self.options = {}

  # Shim Chart Types
  module Helper
    def bubble_chart(data_source, options = {})
      chartkick_chart 'BubbleChart', data_source, options
    end
  end
end
