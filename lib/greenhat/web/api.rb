# General Web Helper
module WebHelpers
  # ======================================================
  # == [ API ]
  # ======================================================
  def api_avg_path_duration_series
    query = [
      'gitlab-rails/api_json.log',
      '--slice=time,path,duration_s'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    method_key = :path
    duration_key = :duration_s

    # Collect Total by Duration
    top = results.each_with_object({}) do |v, obj|
      obj[v[method_key]] ||= []
      obj[v[method_key]] << v[duration_key]

      obj
    end

    # Average / Round
    top.transform_values! { |x| (x.sum / x.size.to_f).round(1) }

    # Only top by duration
    method_calls = top_method_calls(top)
    results.select! { |x| method_calls.any? x[method_key] }

    build_group_time_avg(results, method_key, duration_key) # 5 Minutes
  end

  def api_avg_duration_series
    query = [
      'gitlab-rails/api_json.log',
      '--slice=time,path,duration_s',
      '--exists=duration_s',
      '--duration_s!=0',
      '--exact'
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)

    # MS is hard
    results.each do |r|
      r[:duration_s] = r[:duration_s] * 1000
    end

    build_percentile_list(results, :duration_s)

    # build_time_avg(results, :duration_s).transform_values { |x| x.round(1) }
  end

  def api_duration_series_stacked
    stacks = %i[
      redis_duration_s
      view_duration_s
      gitaly_duration_s
      redis_cache_duration_s
      duration_s
      db_duration_s
      redis_shared_state_duration_s
      queue_duration_s
      db_duration_s
    ]

    query = [
      'gitlab-rails/api_json.log',
      "--slice=time,path,#{stacks.join(',')}"
    ].join(' ')

    results = GreenHat::ShellHelper.filter_internal(query)
    output = build_stack_time_avg(results, stacks)

    # MS are hard
    output.each do |dataset|
      dataset[:data].transform_values! { |x| (x * 1000).round(1) }
    end

    output
  end

  def api_duration_pie
    all = faststats_run('gitlab-rails/api_json.log', 'top')[:totals]

    # Grab Specifics
    totals = all.except(:count, :fails).transform_values(&:round)

    # Organize
    totals.sort_by(&:last).reverse.to_h
  end
end
