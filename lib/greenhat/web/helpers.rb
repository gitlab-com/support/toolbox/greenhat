# General Web Helper
module WebHelpers
  def arg_parse
    @files, @flags, @args = GreenHat::Args.parse Shellwords.split(params[:query])
  end

  def percent(value, total)
    ((value.to_i / total.to_f) * 100).round(2)
  end

  def build_time_index(results, interval = 5.minutes)
    index = {}
    start = results.min_by(&:time).time.floor(interval)
    finish = results.max_by(&:time).time.floor(interval)

    loop do
      index[start] = 0
      start += interval
      break if start > finish
    end

    index
  rescue StandardError => e
    LogBot.fatal('Index Error', e.message)
  ensure
    {}
  end

  # Without Grouping Just total count
  def build_time_total(results, interval = 5.minutes)
    index = build_time_index(results, interval)

    results.each do |r|
      index[r.time.floor(interval)] += 1
    end

    index
  end

  # Without Grouping Avg
  def build_time_avg(results, sum, interval = 5.minutes)
    index = build_time_index(results, interval)

    index.transform_values! { |_y| [] }

    results.each do |r|
      index[r.time.floor(interval)] << r[sum]
    end

    index.transform_values! do |l|
      (l.sum / l.size.to_f)
    end

    index
  end

  # by Occurance/Count / Key to String
  def build_group_time_total(results, group, interval = 5.minutes)
    default_index = build_time_index(results, interval)
    list = {}

    results.map { |x| x[group] }.uniq.each do |key|
      key = 'None' if key.nil?
      list[key.to_s] = {
        name: key.to_s,
        data: default_index.clone
      }
    end

    results.each do |r|
      key = r[group].nil? ? 'None' : r[group]
      list[key.to_s].data[r.time.floor(interval)] += 1
    end

    list.values
  end

  # by Sum
  def build_group_time_sum(results, group, sum, interval = 5.minutes)
    default_index = build_time_index(results, interval)
    list = {}

    results.map { |x| x[group] }.uniq.each do |key|
      key = 'None' if key.nil?
      list[key] = {
        name: key,
        data: default_index.clone.transform_values { |_y| [] }
      }
    end

    results.each do |r|
      key = r[group].nil? ? 'None' : r[group]
      list[key].data[r.time.floor(interval)] << r[sum]
    end

    # Transform / Calculate
    list.each do |_k, v|
      v.data.transform_values! do |l|
        l.empty? ? 0 : l.sum
      end
    end

    list.values
  end

  # by Avg
  def build_group_time_avg(results, group, sum, interval = 5.minutes)
    default_index = build_time_index(results, interval)
    list = {}

    results.map { |x| x[group] }.uniq.each do |key|
      key = 'None' if key.nil?
      list[key] = {
        name: key,
        data: default_index.clone.transform_values { |_y| [] }
      }
    end

    results.each do |r|
      key = r[group].nil? ? 'None' : r[group]
      list[key].data[r.time.floor(interval)] << r[sum]
    end

    # Transform / Calculate
    list.each do |_k, v|
      v.data.transform_values! do |l|
        l.empty? ? 0 : (l.sum / l.size.to_f) # .round(1)
      end
    end

    list.values
  end

  # Stack AVG Helper
  def build_stack_time_avg(results, stacks, interval = 5.minutes)
    default_index = build_time_index(results, interval)
    list = {}

    # MS are Hard
    # results.each do |r|
    #   stacks.each do |stack|
    #     next unless r[stack]

    #     r[stack] = r[stack] * 1000
    #   end
    # end

    # Build List
    stacks.each do |stack|
      data = default_index.clone
      data.transform_values! { |_y| [] } # Ensure Uniq
      list[stack] = {
        name: stack, data:
      }
    end

    # Collect Stack Data
    results.each do |r|
      stacks.each do |stack|
        next unless r.key?(stack)

        list[stack].data[r.time.floor(interval)] << r[stack]
      end
    end

    # Transform / Calculate
    list.each do |_k, v|
      v.data.transform_values! do |l|
        l.empty? ? 0 : (l.sum / l.size.to_f)
      end
    end

    list.values
  end

  # Helper to get the bottom Ten
  def top_method_calls(list)
    sorted = list.sort_by(&:last)

    all = if sorted.length < 10
            sorted
          else
            sorted[-10..]
          end

    all.map(&:first)
  end

  def build_count_by_occurance(results, key, limit = 10)
    output = results.each_with_object(Hash.new(0)) do |result, counts|
      counts[result[key]] += 1
    end

    return output if limit.zero?

    output.sort_by(&:last).reverse[0..(limit - 1)].to_h
  end
end
