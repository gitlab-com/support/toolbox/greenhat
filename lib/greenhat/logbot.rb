# Log Helper
module LogBot
  # For Amazing Print
  def self.settings
    { ruby19_syntax: true, multiline: false }
  end

  def self.output(name, message, level, duration = nil)
    time = Time.now.to_s.pastel(:bright_black)
    output = time
    output += "  #{color_level(level)}"
    output += " - #{name.to_s.pastel(:bright_blue)} "
    output += "(#{duration.round(2)}) ".pastel(:magenta) if duration

    output += ' '
    if message
      output += case message
                when String then message
                else
                  message.ai(settings)
                end
    end

    puts output unless ENV.fetch('TESTING') { GreenHat::Cli.quiet }
  end

  def self.color_level(level)
    case level
    when :debug
      level.to_s.upcase.pastel(:bright_cyan)
    when :info
      level.to_s.upcase.pastel(:cyan)
    when :warn
      level.to_s.upcase.pastel(:yellow)
    when :error
      level.to_s.upcase.pastel(:bright_red)
    when :fatal
      level.to_s.upcase.pastel(:red)
    end
  end

  def self.info(name, message = nil, level = :info)
    output(name, message, level)
  end

  def self.debug(name, message = nil, level = :debug)
    output(name, message, level)
  end

  def self.warn(name, message = nil, level = :warn)
    output(name, message, level)
  end

  def self.error(name, message = nil, level = :error)
    output(name, message, level)
  end

  def self.fatal(name, message = nil, level = :fatal)
    output(name, message, level)
  end

  def self.do(name, message = nil, level = nil)
    began_at = clock_time

    level ||= :info
    result = yield

    duration = (clock_time - began_at).round(3)

    output(name, message, level, duration)

    result
  end

  def self.measure
    began_at = clock_time
    result = yield
    duration = (clock_time - began_at).round(3)
    [result, duration]
  end

  # From Rack Time
  def self.clock_time
    Process.clock_gettime(Process::CLOCK_MONOTONIC)
  end
end
