module GreenHat
  # Overall Type Parsing
  module Kind
    # rubobop:disable *
    def kind_collect
      # If Direct Match
      if types.key? name
        self.type = name

        return true
      end

      # Check Pattern Matches
      matches = types.select do |_k, v|
        v.pattern.any? { |x| x =~ name }
      end

      # If there is only one match
      if matches.keys.count == 1
        self.type = matches.keys.first

        true

      # TODO: Prompt for smaller selection
      elsif matches.keys.count > 1
        puts 'Multiple!'
      # History Match
      elsif ThingHistory.match? name
        self.type = ThingHistory.match(name)
      else
        prompt_for_kind
      end
    end

    # File Identifier
    def kind_setup
      self.kind = types.dig(type, :format)
      self.log = types.dig(type, :log)
    end

    def prompt
      # Quiet Exit
      @prompt ||= TTY::Prompt.new(interrupt: :exit)
    end

    def check_oj_parse?(first_line)
      result = Oj.load(first_line)

      # Extra Validation (Oj is being more generous than we want)
      return false if result.is_a? Integer

      true
    rescue StandardError
      false
    end

    # rubocop:disable Style/SymbolProc
    def prompt_for_kind
      # rubocop:disable Lint/Debugger
      binding.pry if ENV['DEBUG']
      # rubocop:enable Lint/Debugger

      # Default to everything
      prompt_list = types.clone

      first_line = File.open(file) { |f| f.readline }
      json = check_oj_parse?(first_line)

      if json
        if Settings.assume_json?
          self.type = 'json'
          return true
        end

        prompt_list.select! do |_k, v|
          v.to_s.include? 'json'
        end
      end

      if Settings.assume_raw?
        self.type = 'raw'
        return true
      end

      puts "Unable to determine file type for #{name.pastel(:yellow)}"
      puts "Use '#{'json'.pastel(:cyan)}' or '#{'raw'.pastel(:cyan)}' if there are no matches (see file_types.rb)"

      option = prompt.select('Wat is this?', prompt_list.keys.sort_by(&:length), filter: true)

      # Store for later
      ThingHistory.add(name.to_sym, option)

      self.type = option
    end
    # rubocop:enable Style/SymbolProc

    # Pattern Match / Look for `match_` patterns and then strip name for kind type
    def kind_pattern_match(name)
      pattern_match = methods.grep(/^match_/).find do |pattern|
        send(pattern).any? { |x| name =~ x }
      end

      if pattern_match
        pattern_match.to_s.split('match_', 2).last.to_sym
      else
        false
      end
    end
  end
end

# Load All Formatters
require_all "#{File.dirname(__FILE__)}/formatters"
