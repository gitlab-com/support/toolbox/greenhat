module GreenHat
  # -------------------------------------
  # File Types - Overall Type Parsing!
  # -------------------------------------
  # --- Adding a new File ----
  # Duplicate one of the blocks and then update the name and pattern/regex to identify it
  #
  # The 'key' or 'name' is used to generically identify a type of file to be used in other collection tools.
  #   - Looking for errors in `gitlab-workhorse/current` will also include any rotated files via `gitlab-workhorse/@.*`
  #
  # Formatters: See `thing/formmaters` for the different kinds of formatters
  #   - `json` for plain json
  #   - `raw` to ignore/just read (e.g. for cat)
  #
  # log true/false to be included in the log/filter fields

  # rubocop:disable Metrics/MethodLength, Metrics/ModuleLength
  module FileTypes
    def types
      {
        'gitlab_rb' => {
          format: :raw,
          pattern: [
            %r{gitlab/gitlab.rb}
          ]
        },
        'cpuinfo' => {
          format: :raw,
          pattern: [
            /cpuinfo/
          ]
        },
        'df_h' => {
          format: :table,
          pattern: [
            /df_h/,
            /df_hT/
          ]
        },
        'df_inodes' => {
          format: :table,
          pattern: [
            /df_inodes/
          ]
        },
        'dmesg' => {
          format: :bracket_log,
          log: true,
          pattern: [
            /dmesg/
          ]
        },
        'lets-encrypt/renewal' => {
          format: :bracket_log,
          log: true,
          pattern: [
            %r{lets-encrypt/renewal}
          ]
        },
        'repmgrd/current' => {
          format: :bracket_log,
          log: true,
          pattern: [
            %r{repmgrd/current}
          ]
        },
        'log/syslog' => {
          format: :syslog,
          log: true,
          pattern: [
            %r{log/syslog}
          ]
        },
        'gitlab-runner.log' => {
          format: :runner_log,
          log: true,
          pattern: [
            /gitlab-runner.log/
          ]
        },
        'etc/fstab' => {
          format: :clean_raw,
          pattern: [
            %r{etc/fstab},
            /fstab/
          ]
        },
        'etc/os-release' => {
          format: :dotenv,
          pattern: [
            %r{etc/os-release}
          ]
        },
        'sysctl_a' => {
          format: :dotenv,
          pattern: [
            /sysctl_a/
          ]
        },
        'free_m' => {
          format: :free_m,
          pattern: [
            /free_m/
          ]
        },
        'getenforce' => {
          format: :raw,
          pattern: [
            /getenforce/
          ]
        },
        'geo-logcursor/current' => {
          format: :raw,
          pattern: [
            %r{geo-logcursor/current}
          ]
        },
        'elastic_info' => {
          format: :raw,
          pattern: [
            /elastic_info/
          ]
        },
        'fast-stats/api' => {
          format: :raw,
          pattern: [
            %r{fast-stats/api}
          ]
        },
        'fast-stats/gitaly' => {
          format: :raw,
          pattern: [
            %r{fast-stats/gitaly}
          ]
        },
        'fast-stats/production' => {
          format: :raw,
          pattern: [
            %r{fast-stats/production}
          ]
        },
        'fast-stats/sidekiq' => {
          format: :raw,
          pattern: [
            %r{fast-stats/sidekiq}
          ]
        },
        'non_analyzed_tables' => {
          format: :raw,
          pattern: [
            /non_analyzed_tables/
          ]
        },
        'schema_dump_result' => {
          format: :raw,
          pattern: [
            /schema_dump_result/
          ]
        },
        'log/mail.log' => {
          format: :raw,
          pattern: [
            %r{log/mail.log}
          ]
        },
        'gitaly/current' => {
          format: :json_shell,
          log: true,
          pattern: [
            %r{gitaly/current},
            %r{gitaly/@.*}
          ]
        },
        'gitaly/gitaly_hooks.log' => {
          format: :shellwords,
          log: true,
          pattern: [
            %r{gitaly/gitaly_hooks\.log}
          ]
        },
        'consul/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{consul/current}
          ]
        },
        'consul/identify_primary_db_node.log' => {
          format: :identify_db,
          log: true,
          pattern: [
            %r{consul/identify_primary_db_node.log}
          ]
        },
        'gitlab-kas/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{gitlab-kas/current}
          ]
        },
        'sentinel/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{sentinel/current}
          ]
        },
        'consul/failover_pgbouncer.log' => {
          format: :raw,
          log: true,
          pattern: [
            %r{consul/failover_pgbouncer.log}
          ]
        },
        'pgbouncer/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{pgbouncer/current}
          ]
        },
        'mailroom/mail_room_json.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{mailroom/mail_room_json.log}
          ]
        },
        'gitlab_migrations' => {
          format: :raw,
          log: true,
          pattern: [
            /gitlab_migrations/
          ]
        },
        'gitlab-rails/git_json.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/git_json.log}
          ]
        },
        'gitaly/gitaly_ruby_json.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitaly/gitaly_ruby_json.log}
          ]
        },
        'gitaly/gitaly_lfs_smudge.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitaly/gitaly_lfs_smudge.log}
          ]
        },
        'gitlab_status' => {
          format: :gitlab_status,
          pattern: [
            /gitlab_status/
          ]
        },
        'postgresql/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{postgresql/current},
            %r{postgresql/@.*}
          ]
        },
        'gitlab-ctl/tail' => {
          format: :gitlab_tail,
          log: true,
          pattern: [
            /gitlab-ctl-tail.log/
          ]
        },
        'patroni/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{patroni/current}
          ]
        },
        'gitlab-exporter/current' => {
          format: :exporters,
          log: true,
          pattern: [
            %r{gitlab-exporter/current}
          ]
        },
        'gitlab-rails/web_exporter' => {
          format: :exporters,
          log: true,
          pattern: [
            %r{gitlab-rails/web_exporter}
          ]
        },
        'gitlab-monitor/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{gitlab-monitor/current}
          ]
        },
        'logrotate/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{logrotate/current}
          ]
        },
        'gitlab-rails/grpc.log' => {
          format: :raw,
          log: true,
          pattern: [
            %r{gitlab-rails/grpc.log}
          ]
        },
        'gitlab-rails/api_json.log' => {
          format: :api_json,
          log: true,
          pattern: [
            %r{gitlab-rails/api_json.log}
          ]
        },
        'gitlab-rails/application_json.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/application_json.log}
          ]
        },
        'gitlab-rails/audit_json.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/audit_json.log},
            %r{webservice.log/audit_json.log}
          ]
        },
        'gitlab-rails/auth.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/auth.log}
          ]
        },
        'gitlab-rails/exceptions_json.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/exceptions_json.log}
          ]
        },
        'gitlab-rails/graphql_json.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/graphql_json.log}
          ]
        },
        'gitlab-rails/importer.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/importer.log}
          ]
        },
        'gitlab-rails/integrations_json.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/integrations_json.log}
          ]
        },
        'gitlab-rails/production_json.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/production_json.log}
          ]
        },
        'gitlab-rails/sidekiq_client.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/sidekiq_client.log}
          ]
        },
        'gitlab-shell/gitlab-shell.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-shell/gitlab-shell.log}
          ]
        },
        'gitlab-workhorse/current' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-workhorse/current},
            %r{gitlab-workhorse/@.*}
          ]
        },
        'gitlab/version-manifest.json' => {
          format: :multiline_json,
          pattern: [
            %r{gitlab/version-manifest.json}
          ]
        },
        'gitlabsos.log' => {
          format: :bracket_log,
          log: true,
          pattern: [
            /gitlabsos.log/
          ]
        },
        'hostname' => {
          format: :raw,
          pattern: [
            /hostname/
          ]
        },
        'ifconfig' => {
          format: :raw,
          pattern: [
            /ifconfig/
          ]
        },
        'ip_address' => {
          format: :raw,
          pattern: [
            /ip_address/
          ]
        },
        'lscpu' => {
          format: :colon_split_strip,
          pattern: [
            /lscpu/
          ]
        },
        'meminfo' => {
          format: :colon_split_strip,
          pattern: [
            /meminfo/
          ]
        },
        'mount' => {
          format: :raw,
          pattern: [
            /mount/
          ]
        },
        'nginx/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{nginx/current}
          ]
        },
        'nginx-ingress.log' => {
          format: :kube_nginx,
          log: true,
          pattern: [
            /nginx-ingress.log/
          ]
        },
        'nginx/access.log' => {
          format: :nginx,
          log: true,
          pattern: [
            %r{nginx/access.log}
          ]
        },
        'nginx/gitlab_pages_access.log' => {
          format: :nginx,
          log: true,
          pattern: [
            %r{nginx/gitlab_pages_access.log}
          ]
        },
        'nginx/gitlab_pages_error.log' => {
          format: :raw,
          log: true,
          pattern: [
            %r{nginx/gitlab_pages_error.log}
          ]
        },
        'nginx/gitlab_access.log' => {
          format: :nginx,
          log: true,
          pattern: [
            %r{nginx/gitlab_access.log}
          ]
        },
        'gitlab-rails/sidekiq_exporter.log' => {
          format: :raw,
          log: true,
          pattern: [
            %r{gitlab-rails/sidekiq_exporter.log}
          ]
        },
        'crond/current' => {
          format: :time_shellwords,
          log: true,
          pattern: [
            %r{crond/current}
          ]
        },
        'grafana/current' => {
          format: :time_shellwords,
          log: true,
          pattern: [
            %r{grafana/current}
          ]
        },
        'pgbouncer-exporter/current' => {
          format: :time_shellwords,
          log: true,
          pattern: [
            %r{pgbouncer-exporter/current}
          ]
        },
        'puma/current' => {
          format: :time_json,
          log: true,
          pattern: [
            %r{puma/current}
          ]
        },
        'reconfigure.log' => {
          format: :bracket_log,
          log: true,
          pattern: [
            %r{^reconfigure/.*log$}
          ]
        },
        'node-exporter/current' => {
          format: :time_shellwords,
          log: true,
          pattern: [
            %r{node-exporter/current}
          ]
        },
        'postgres-exporter/current' => {
          format: :time_shellwords,
          log: true,
          pattern: [
            %r{postgres-exporter/current}
          ]
        },
        'redis-exporter/current' => {
          format: :time_shellwords,
          log: true,
          pattern: [
            %r{redis-exporter/current},
            %r{redis-exporter/@.*}
          ]
        },
        'prometheus/current' => {
          format: :time_shellwords,
          log: true,
          pattern: [
            %r{prometheus/current}
          ]
        },
        'registry/current' => {
          format: :time_registry,
          log: true,
          pattern: [
            %r{registry/current},
            %r{registry/@.*}
          ]
        },
        'alertmanager/current' => {
          format: :time_shellwords,
          log: true,
          pattern: [
            %r{alertmanager/current}
          ]
        },
        'ps' => {
          format: :table,
          pattern: [
            /^ps/
          ]
        },
        'pressure_cpu.txt' => {
          format: :raw,
          log: false,
          pattern: [
            /pressure_cpu.txt/
          ]
        },
        'pressure_io.txt' => {
          format: :raw,
          log: false,
          pattern: [
            /pressure_io.txt/
          ]
        },
        'pressure_mem.txt' => {
          format: :raw,
          log: false,
          pattern: [
            /pressure_mem.txt/
          ]
        },
        'puma/puma_stderr.log' => {
          format: :raw,
          log: false,
          pattern: [
            %r{puma/puma_stderr.log},
            %r{webservice.log/puma.stderr.log}
          ]
        },

        'puma/puma_stdout.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{puma/puma_stdout.log},
            %r{webservice.log/puma.stdout.log}
          ]
        },
        'gitlab-pages/current' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-pages/current}
          ]
        },

        # General Purpose Raw
        'raw' => {
          format: :raw,
          pattern: [
            %r{alertmanager/config},
            %r{gitaly/config},
            %r{gitlab-exporter/config},
            %r{gitlab-pages/config},
            %r{gitlab-workhorse/config},
            %r{grafana/config},
            %r{logrotate/config},
            %r{nginx/config},
            %r{node-exporter/config},
            %r{postgres-exporter/config},
            %r{postgresql/config},
            %r{prometheus/config},
            %r{puma/config},
            %r{redis/config},
            %r{redis-exporter/config},
            %r{registry/config},
            %r{sidekiq/config},
            %r{pgbouncer/config},
            %r{pgbouncer-exporter/config},
            %r{repmgrd/config},
            %r{sentinel/config},
            %r{consul/config},
            %r{mailroom/current}
          ]
        },
        'redis/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{redis/current},
            %r{redis/@.*}
          ]
        },
        'nginx/gitlab_error.log' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{nginx/gitlab_error.log}
          ]
        },
        'nginx/error.log' => {
          format: :raw,
          log: false,
          pattern: [
            %r{nginx/error.log}
          ]
        },
        'nginx/gitlab_registry_error.log' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{nginx/gitlab_registry_error.log}
          ]
        },
        'unicorn/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{unicorn/current}
          ]
        },
        'unicorn_stats' => {
          format: :time_space,
          log: true,
          pattern: [
            /unicorn_stats/
          ]
        },
        'unicorn/unicorn_stdout.log' => {
          format: :raw,
          log: true,
          pattern: [
            %r{unicorn/unicorn_stdout.log}
          ]
        },
        'running_swappiness' => {
          format: :raw,
          pattern: [
            /running_swappiness/
          ]
        },
        'nginx/gitlab_registry_access.log' => {
          format: :nginx,
          log: true,
          pattern: [
            %r{nginx/gitlab_registry_access.log}
          ]
        },
        'security/limits.conf' => {
          format: :clean_raw,
          pattern: [
            %r{security/limits.conf}
          ]
        },
        'unicorn/unicorn_stderr.log' => {
          format: :raw,
          log: true,
          pattern: [
            %r{unicorn/unicorn_stderr.log}
          ]
        },
        'selinux/config' => {
          format: :dotenv,
          pattern: [
            %r{selinux/config}
          ]
        },
        'sidekiq/current' => {
          format: :json,
          log: true,
          pattern: [
            %r{sidekiq/current},
            /sidekiq_current/,
            %r{sidekiq/@.*}
          ]
        },
        'gitlab-rails/service_measurement.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/service_measurement.log}
          ]
        },
        # General Purpose JSON
        'json' => {
          format: :json,
          log: true,
          pattern: []
        },

        'gitlab-rails/elasticsearch.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/elasticsearch.log}
          ]
        },
        'sshd/current' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{sshd/current}
          ]
        },
        'gitlab-rails/sidekiq.log' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{gitlab-rails/sidekiq.log}
          ]
        },
        'gitlab-rails/gitlab-rails-db-migrate' => {
          format: :time_space,
          log: true,
          pattern: [
            %r{gitlab-rails/gitlab-rails-db-migrate.*}
          ]
        },
        'tainted' => {
          format: :raw,
          pattern: [
            /tainted/
          ]
        },
        'ulimit' => {
          format: :raw,
          pattern: [
            /ulimit/
          ]
        },
        'uname' => {
          format: :raw,
          pattern: [
            /uname/
          ]
        },
        'gitlab_socket_stats' => {
          format: :gitlab_socket_stats,
          pattern: [
            /gitlab_socket_stats/
          ]
        },
        'uptime' => {
          format: :raw,
          pattern: [
            /uptime/
          ]
        },
        'mpstat' => {
          format: :raw,
          pattern: [
            /mpstat/
          ]
        },
        'isostat' => {
          format: :raw,
          pattern: [
            /isostat/
          ]
        },
        'netstat' => {
          format: :netstat,
          pattern: [
            /^netstat$/
          ]
        },
        'netstat_i' => {
          format: :netstat_i,
          pattern: [
            /^netstat_i$/
          ]
        },
        'iostat' => {
          format: :raw,
          pattern: [/^iostat/]
        },
        'nfsiostat' => {
          format: :raw,
          pattern: [
            /^nfsiostat/
          ]
        },
        'nfsstat' => {
          format: :raw,
          pattern: [
            /nfsstat/
          ]
        },
        'pidstat' => {
          format: :pidstat,
          pattern: [
            /pidstat/
          ]
        },
        'sar_dev' => {
          format: :sar_dev,
          pattern: [
            /sar_dev/
          ]
        },
        'sar_tcp' => {
          format: :sar_tcp,
          pattern: [
            /sar_tcp/
          ]
        },
        'sestatus' => {
          format: :colon_split_strip,
          pattern: [
            /sestatus/
          ]
        },
        'sockstat' => {
          format: :raw,
          pattern: [
            /sockstat/
          ]
        },
        'systemctl_unit_files' => {
          format: :table,
          pattern: [
            /systemctl_unit_files/
          ]
        },
        'timedatectl' => {
          format: :colon_split_strip,
          pattern: [
            /timedatectl/
          ]
        },
        'log/messages' => {
          format: :syslog,
          log: true,
          pattern: [
            %r{log/messages}
          ]
        },
        'vmstat' => {
          format: :raw,
          pattern: [
            /vmstat/
          ]
        },
        'date' => {
          format: :raw,
          pattern: [
            /date/
          ]
        },
        'gitlab-kas/config' => {
          format: :raw,
          pattern: [
            %r{gitlab-kas/config}
          ]
        },
        'mailroom/config' => {
          format: :raw,
          pattern: [
            %r{mailroom/config}
          ]
        },
        'iotop' => {
          format: :raw,
          pattern: [
            /iotop/
          ]
        },
        'top_res' => {
          format: :raw,
          pattern: [
            /top_res/
          ]
        },
        'top_cpu' => {
          format: :raw,
          pattern: [
            /top_cpu/
          ]
        },
        'ntpq' => {
          format: :raw,
          pattern: [
            /ntpq/
          ]
        },
        'rpm_verify' => {
          format: :raw,
          pattern: [
            /rpm_verify/
          ]
        },
        'gitlab_system_status' => {
          format: :raw,
          pattern: [
            /gitlab_system_status/
          ]
        },
        'gitlab-rails/application.log' => {
          format: :raw,
          pattern: [
            %r{gitlab-rails/application.log},
            %r{webservice.log/application.log}
          ]
        },
        'gitlab-rails/production.log' => {
          format: :raw,
          pattern: [
            /production.log/,
            %r{gitlab-rails/production.log},
            %r{webservice.log/production.log}
          ]
        },
        'gitlab/version-manifest.txt' => {
          format: :raw,
          pattern: [
            %r{gitlab/version-manifest.txt}
          ]
        },
        'ignore' => {
          format: :ignore,
          pattern: [
            %r{sidekiq/perf.data}
          ]
        },
        'gitlab-rails/geo.log' => {
          format: :json,
          log: true,
          pattern: [
            %r{gitlab-rails/geo.log}
          ]
        },

        # ======================================================================
        # KubeSoS TODO Section
        # Things I am going to shortcut and set to raw for now
        # ======================================================================
        # Attempted Parsing
        'kubesos_json' => {
          log: true,
          format: :kube_json,
          pattern: [
            /gitaly.log/
          ]
        },

        'kube_webservice' => {
          log: false,
          format: :kube_webservice,
          pattern: [
            /^webservice\.log$/
          ]
        },

        'kubesos' => {
          format: :raw,
          pattern: [
            /license_info/,
            %r{etc/sshd_config},
            /btmp_size/,
            /user_uid/,
            /all_values.yaml/,
            %r{webservice.log/sidekiq_client.log},
            /chart-version/,
            /configmaps/,
            /describe_deployments/,
            /describe_ingress/,
            /describe_nodes/,
            /describe_pods/,
            /describe_pv/,
            /describe_pvc/,
            /events/,
            /get_deployments/,
            /get_endpoints/,
            /get_jobs/,
            /get_pods/,
            /get_pv/,
            /get_pvc/,
            /get_services/,
            /gitaly.log/,
            /gitlab-exporter.log/,
            /gitlab-pages.log/,
            /grafana.log/,
            /helm-version/,
            /kubectl-check/,
            /migrations.log/,
            /minio.log/,
            /nfs-client-provisioner.log/,
            /operator.log/,
            /postgresql.log/,
            /prometheus.log/,
            /redis.log/,
            /registry.log/,
            /secrets/,
            /sidekiq.log/,
            /task-runner.log/,
            /top_nodes/,
            /top_pods/,
            /user_supplied_values.yaml/
          ]
        }
      }
    end
  end
  # rubocop:enable Metrics/MethodLength, Metrics/ModuleLength
end
