module GreenHat
  # Info Formatter
  module InfoFormat
    # Is this something that can be formatted by info?
    def info?
      methods.include? "format_#{path}".to_sym
    end

    # Handle Info Formatting
    def info_format
      self.result = send("format_#{path}")
    end

    def format_headers_n_lines
      # Headers to Readable Symbol
      headers = raw.first.split(' ', 6).map(&:downcase).map do |x|
        x.gsub(/\s+/, '_').gsub(/[^0-9A-Za-z_]/, '')
      end.map(&:to_sym)

      output = []

      # Put fields into a Hash based on Location/Key
      raw[1..].map(&:split).each do |row|
        result = {}
        row.each_with_index do |detail, i|
          result[headers[i]] = detail
        end
        output.push result
      end

      self.result = output
    end

    # All Similar to Header and Lines
    alias format_df_inodes format_headers_n_lines
    alias format_df_h format_headers_n_lines

    def format_mount(data)
      data.select { |x| x.include? ':' }
    end

    def format_free_m
      split_white_space raw
    end

    def format_meminfo
      raw.map { |x| x.split(' ', 2) }
    end

    def format_netstat_conn_headers
      [
        'Proto', 'Recv-Q', 'Send-Q', 'Local Address', 'Foreign Address', 'State', 'PID/Program name'
      ]
    end

    def format_netstat_socket_headers
      [
        'Proto', 'RefCnt', 'Flags', 'Type', 'State', 'I-Node', 'PID/Program name', 'Path'
      ]
    end

    def format_netstat
      # Data can return blank or single item array
      conns, sockets = raw.split { |x| x.include? 'Active' }.reject(&:empty?)

      formatted_conns = conns[1..].map do |entry|
        entry.split(' ', 7).map(&:strip).each_with_index.to_h do |field, idx|
          [format_netstat_conn_headers[idx], field]
        end
      end

      formatted_sockets = sockets[1..].map do |entry|
        entry.split('  ').map(&:strip).reject(&:blank?).each_with_index.to_h do |field, idx|
          [format_netstat_socket_headers[idx], field]
        end
      end

      {
        connections: formatted_conns,
        sockets: formatted_sockets
      }
    end

    def format_netstat_i
      headers = raw[1].split
      result = raw[2..].map do |row|
        row.split.each_with_index.map do |x, idx|
          [headers[idx], x]
        end
      end

      result.map(&:to_h)
    end

    def split_white_space(data)
      data.map(&:split)
    end

    def ps_format
      headers = info.ps.first.split(' ', 11)
      list = info.ps[1..].each.map do |row|
        row.split(' ', 11).each_with_index.with_object({}) do |(v, i), obj|
          obj[headers[i]] = v
        end
      end
      { headers:, list: }
    end

    def manifest_json_format
      Oj.load info[:gitlab_version_manifest_json].join
    end

    def cpuinfo_format
      info.cpuinfo.join("\n").split("\n\n").map do |cpu|
        all = cpu.split("\n").map do |row|
          row.delete("\t").split(': ')
        end
        { details: all[1..], order: all[0].last }
      end
    end

    def cpu_speed
      return nil unless data? :lscpu

      info.lscpu.find { |x| x.include? 'MHz' }.split('               ')
    end

    def total_memory
      return nil unless data? :free_m

      value = info.free_m.dig(1, 1).to_i
      number_to_human_size(value * 1024 * 1024)
    end

    def ulimit
      return nil unless data? :ulimit

      results = info.ulimit.map do |entry|
        {
          value: entry.split[-1],
          details: entry.split('  ').first
        }
      end

      results.sort_by { |x| x[:details].downcase }
    end

    def systemctl_format
      return nil unless data? :systemctl_unit_files

      all = info.systemctl_unit_files[1..-2].map do |x|
        unit, status = x.split
        { unit:, status: }
      end
      all.reject! { |x| x[:unit].nil? }
      all.sort_by(&:unit)
    end

    # Helper to color the status files
    def systemctl_color(entry)
      case entry.status
      when 'enabled'  then :green
      when 'static'   then :orange
      when 'disabled' then :red
      else
        :grey
      end
    end

    def vmstat_format
      return nil unless data? :vmstat

      info.vmstat[2..].map(&:split)
    end

    def uptime
      info.uptime.join.split(',', 4).map(&:lstrip) if info.key? :uptime
    end

    def mount
      return nil unless info.key? :mount
      return nil if info.mount.empty?

      MountFormat.parse(info.mount)
    end

    def format_single_json
      Oj.load raw.join("\n")
    end

    alias format_gitlab_version_manifest_json format_single_json
  end
end
