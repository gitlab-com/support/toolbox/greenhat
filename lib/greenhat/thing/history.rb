module GreenHat
  # Helper for Remembering what files were
  module ThingHistory
    def self.files
      @files ||= read

      @files
    end

    # Read File / Remove Old Entries
    def self.read
      if File.exist? file
        results = Oj.load File.read(file)

        results.reject { |_k, v| Time.at(v.time) < Time.now }
      else
        {}
      end
    ensure
      {}
    end

    def self.file
      Settings.history_file
    end

    # Initial Entry Point
    def self.match?(name)
      files.key? name.to_sym
    end

    def self.match(name)
      files.dig(name.to_sym, :type)
    end

    def self.add(name, type)
      files[name] = { type:, time: expire }

      write
    end

    # Date to Expire
    def self.expire
      2.weeks.from_now.to_i
    end

    def self.write
      File.write(file, Oj.dump(files))
    end
  end
end
