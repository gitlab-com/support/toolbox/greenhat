# Global Namespace
module GreenHat
  # Accessors / Readers
  module ThingHelpers
    # Console Helper
    def inspect
      [
        'Thing'.pastel(:bright_black),
        kind&.to_s&.pastel(:blue),
        type&.pastel(:bright_yellow),
        name&.pastel(:cyan)
      ].compact.join(' ')
    end

    # Format the name of this thing
    def build_path(divider = '_')
      tmp_path = file.gsub("#{archive.path}/", '')

      case tmp_path.count('/')
      when 0
        tmp_path
      when 1
        tmp_path.split('/').last
      else
        tmp_path.split('/').last(2).join(divider)
      end
    end
  end
end

# Pretty sure this is a bad idea to patch string.
# https://mentalized.net/journal/2011/04/14/ruby-how-to-check-if-a-string-is-numeric/
class String
  def numeric?
    !Float(self).nil?
  rescue StandardError
    false
  end
end

# Shims
class Float
  def numeric?
    true
  end
end

# Shims
class TrueClass
  def to_i
    1
  end
end

# Shims
class FalseClass
  def to_i
    0
  end
end

# https://stackoverflow.com/a/449322/1678507
class Time
  # Time#round already exists with different meaning in Ruby 1.9
  def round_off(seconds = 60)
    Time.at((to_f / seconds).round * seconds).utc
  end

  def floor(seconds = 60)
    Time.at((to_f / seconds).floor * seconds).utc
  end
end
