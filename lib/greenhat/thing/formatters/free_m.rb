# Top
module GreenHat
  # Log
  module Formatters
    # Get Split Memory Table
    def format_free_m
      # Headers to Readable Symbol
      headers = raw_full.first.split.map(&:downcase).map do |x|
        x.gsub(/\s+/, '_').gsub(/[^0-9A-Za-z_]/, '')
      end.map(&:to_sym)

      # Add Kind
      headers.unshift(:kind)

      final = []

      # Put fields into a Hash based on Location/Key
      raw_full[1..].map(&:split).each do |row|
        result = {}
        row.each_with_index do |detail, i|
          result[headers[i]] = detail.split(':').first
        end
        final.push result
      end

      self.result = final
    end
  end
end
