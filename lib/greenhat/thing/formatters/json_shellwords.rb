# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # Formatters
    # ==========================================================================
    def format_json_shell
      self.result = raw.map do |row|
        result = format_json_row(row)

        # Parsing Time
        format_time(result)

        result
      rescue StandardError => e
        LogBot.warn('JSON Parse', e.message)
        next
      end

      :ok
    end

    # Simplify Map Loop / Short Circuit for weird entries
    def format_json_row(row)
      return { message: row } if row[0] != '{' && !row.include?('{')

      begin
        Oj.load row
      rescue EncodingError
        json_shellwords_fallback row
      end
    end

    def json_shellwords_fallback(row)
      result = Shellwords.split(row).each_with_object({}) do |x, h|
        key, value = x.split('=')
        next if value.nil?

        h[key] = value.numeric? ? value.to_f : value
      end
      result.time = Time.parse result.time if result.key? 'time'

      result
    end

    # ==========================================================================
  end
end
