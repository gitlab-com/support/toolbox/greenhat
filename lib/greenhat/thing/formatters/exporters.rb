# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # General Exporter Parser
    # https://gitlab.com/gitlab-com/support/toolbox/greenhat/-/issues/30
    # [YYYY-MM-DDTHH:MM:SS.sss-HHMM] …ip… - - [DD/Mon/YYYY:HH:MM:SS ZON]
    # "GET /metrics HTTP/1.1" 200 12345 "-" "Prometheus/2.25.0"
    # ==========================================================================
    def format_exporters
      self.result = raw.map do |row|
        result = {}
        time, output = row.split(' ', 2)
        result[:time] = Time.parse(time)

        # Weird Edge Cases for when IP is not included
        msg = ''
        if output.include? '- - '
          ip, msg = output.split('- -', 2)
          result[:ip] = ip unless ip.empty?
        else
          msg = output
        end

        # Styrip Extra Time / Get Call Details
        if msg&.include? ']'
          _time, request = msg.split('] ', 2)
          result[:msg] = request # Pass full request

          # Breakout
          method, path, protocol, status, bytes = request.gsub('"', '').split(' ', 5)

          result.merge!(method:, path:, protocol:, status:, bytes:)

        else
          result[:msg] = msg
        end

        result
      rescue StandardError => e
        # TODO: Background logger
        LogBot.warn('Exporters', "Unable to Parse, #{row}:#{e.message}")
      end
    end
    # ==========================================================================
  end
end
