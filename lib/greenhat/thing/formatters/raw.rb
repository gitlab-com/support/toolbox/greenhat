# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # Formatters Not Handled
    # ==========================================================================
    def mia
      # TODO: Background Logger
      LogBot.warn('Log Format', "No Formatter for #{name}::#{path}")
    end

    def format_raw
      self.result = raw
    end
    # ==========================================================================
  end
end
