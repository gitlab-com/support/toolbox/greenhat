# Top
module GreenHat
  # Log
  module Formatters
    # Formatters for single json blobs in entire file
    def format_colon_split_strip
      self.result = raw_full.to_h do |row|
        row.split(':', 2).map(&:strip)
      end
    end
  end
end
