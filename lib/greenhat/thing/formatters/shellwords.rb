# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # Formatters Not Handled
    # ==========================================================================
    def format_shellwords
      self.result = raw.map do |row|
        result = Shellwords.split(row).each_with_object({}) do |x, h|
          key, value = x.split('=')
          next if value.nil?

          h[key] = value.numeric? ? value.to_f : value
        end
        result.time = Time.parse result.time if result.key? 'time'

        result
      end
    end
    # ==========================================================================
  end
end
