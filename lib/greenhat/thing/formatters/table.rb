# Top
module GreenHat
  # Log
  module Formatters
    # Looks like this may be different per Type
    def header_count
      case name
      when 'df_hT' then 7
      end
    end

    # Table Formatting
    def format_table
      # Default to split by space unless specifically needed
      headers_raw = if header_count
                      raw_full.first.split(' ', header_count)
                    else
                      raw_full.first.split
                    end

      # Headers to Readable Symbol
      headers = headers_raw.map(&:downcase).map do |x|
        x.gsub(/\s+/, '_').gsub(/[^0-9A-Za-z_]/, '')
      end.map(&:to_sym)

      final = []

      # Put fields into a Hash based on Location/Key
      raw_full.map(&:split)[1..].each do |row|
        result = {}
        row.each_with_index do |detail, i|
          result[headers[i]] = detail
        end
        final.push result
      end

      self.result = final
    end
  end
end
