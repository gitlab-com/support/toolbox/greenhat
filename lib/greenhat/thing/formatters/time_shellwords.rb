# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # Formatters Not Handled
    # ==========================================================================
    def format_time_shellwords
      self.result = raw.map do |row|
        time, msg = row.split(' ', 2)

        result = Shellwords.split(msg).each_with_object({}) do |x, h|
          key, value = x.split('=')
          next if value.nil?

          h[key] = value.numeric? ? value.to_f : value
        end

        # Timestamp Parsing
        result.ts = Time.parse result.ts if result.key? 'ts'
        result.time = Time.parse time

        result
      end
    end
    # ==========================================================================
  end
end
