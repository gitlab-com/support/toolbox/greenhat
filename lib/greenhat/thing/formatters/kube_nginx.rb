# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # Formatters
    # ==========================================================================
    def format_kube_nginx
      self.result = raw.map do |row|
        ip, _sym, remote_user, rest = row.split(' ', 4)

        time, rest = rest.split(']', 2)
        time = Time.strptime(time, '[%d/%b/%Y:%H:%M:%S %z')

        verb, status, bytes, http_referer, http_user_agent, gzip_ratio = Shellwords.split(rest)

        method, path, http_version = verb.split

        {
          remote_addr: ip,
          remote_user:,
          method:,
          path:,
          status:,
          bytes:,
          http_version:,
          http_referer:,
          http_user_agent:,
          gzip_ratio:,
          time:
        }

        # Fall back for malformed logs
      rescue StandardError
        { message: row }
      end

      :ok
    end

    # rubocop:disable Layout/LineLength
    # NGINX Conf: /var/opt/gitlab/nginx/conf/nginx.conf
    # log_format gitlab_access '$remote_addr - $remote_user [$time_local] "$request_method $filtered_request_uri $server_protocol" $status $body_bytes_sent "$filtered_http_referer" "$http_user_agent" $gzip_ratio';
    # rubocop:enable Layout/LineLength

    # ==========================================================================
  end
end
