# Top
module GreenHat
  # Log
  module Formatters
    # consul/identify_primary_db_node.log
    # https://gitlab.com/gitlab-com/support/toolbox/greenhat/-/issues/33
    # I, [YYYY-MM-DDThh:mm:ss.ssssss #12345]  INFO -- : Found master: sub.domain.tld
    def format_identify_db
      self.result = raw.map do |row|
        next if row.empty? || row == "\n"

        level_abrv, row = row.split(', ', 2)

        time, output = row.split('] ', 2)
        time = Time.parse time.split('[', 2).last.strip

        level, _a, _b, msg = output.split(' ', 4)
        {
          time:,
          i: level_abrv,
          level:,
          message: msg
        }
      rescue StandardError => e
        # TODO: Background logger
        LogBot.warn('identify_db', "Unable to Parse, #{row}:#{e.message}")
      end

      result.compact!
    end
  end
end
