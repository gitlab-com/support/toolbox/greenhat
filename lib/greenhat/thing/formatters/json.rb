# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # Formatters
    # Fallback to String
    # ==========================================================================
    def format_json
      self.result = raw.map do |row|
        result = begin
          Oj.load row
        rescue EncodingError
          { message: row }
        end

        # Skip bad results
        next if result.nil?

        # Parsing Time / Recusrive is really slow
        format_time(result)

        result
      rescue StandardError => e
        # TODO: Background Logger?
        e.message
        LogBot.warn('JSON Parse', e.message)
        next
      end

      :ok
    end

    # ==========================================================================

    # Recursively Navigate -- Recusrive is really slow
    def format_json_traverse(result)
      format_time(result)

      result.each do |_key, value|
        next unless value.instance_of? Hash

        format_json_traverse(value)
      end
    end

    def format_time_fields
      %i[time timestamp created_at enqueued_at completed_at]
    end

    # Loop through potential fields and parse as needed
    def format_time(result)
      # Don't format integers
      # return if result.is_a? Integer

      format_time_fields.each do |field|
        result[field] = format_time_parse(result[field]) if result.key?(field)
      rescue StandardError => e
        LogBot.warn('JSON Format Time', e.message)
        next
      end
    end

    # Check for Common Fields
    # def format_json_time(result)
    #   if result.key? :time
    #     result.time = format_time_parse(result.time)
    #   elsif result.key?(:timestamp)
    #     result.time = format_time_parse(result.timestamp)
    #   end

    #   result.created_at = format_time_parse(result.created_at) if result.key? :created_at
    #   result.enqueued_at = format_time_parse(result.enqueued_at) if result.key? :enqueued_at
    # rescue StandardError => e
    #   LogBot.warn("JSON Time Parse", e.message)
    #   true
    # end

    def format_time_parse(time)
      Time.parse time
    rescue StandardError
      # Handle Epoch Timestamps as well as string timestamps
      format_time_numeric(time)
    end

    def format_time_numeric(time)
      if time.numeric?
        Time.at time
      else
        time
      end
    rescue StandardError => e
      LogBot.warn('JSON Time Parse', e.message)
    end
    # ---
  end
end
