# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # Formatters
    # ==========================================================================
    def format_nginx
      output = raw.map do |row|
        next if row.chomp.empty?

        conn_data, rest = row.split(' [', 2)
        ip, _sym, remote_user = conn_data.split(' ', 3)

        time, rest = rest.split(']', 2)
        time = Time.strptime(time, '%d/%b/%Y:%H:%M:%S %z')

        verb, status, bytes, http_referer, http_user_agent, gzip_ratio = Shellwords.split(rest)

        method, path, http_version = verb.split

        {
          remote_addr: ip,
          remote_user:,
          method:,
          path:,
          status:,
          bytes:,
          http_version:,
          http_referer:,
          http_user_agent:,
          gzip_ratio:,
          time:
        }
      rescue StandardError => e
        LogBot.warn('NGINX Parse', e.message)
        { msg: row }
      end

      # Remove Empty Entries
      self.result = output.compact
      :ok
    end

    # rubocop:disable Layout/LineLength
    # NGINX Conf: /var/opt/gitlab/nginx/conf/nginx.conf
    # log_format gitlab_access '$remote_addr - $remote_user [$time_local] "$request_method $filtered_request_uri $server_protocol" $status $body_bytes_sent "$filtered_http_referer" "$http_user_agent" $gzip_ratio';
    # rubocop:enable Layout/LineLength

    # ==========================================================================
  end
end
