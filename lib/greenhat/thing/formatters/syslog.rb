# Top
module GreenHat
  # Log
  module Formatters
    # Formatters for bracket logs (dmesg, sos)
    def format_syslog
      self.result = raw.map do |row|
        next if row.empty? || row == "\n"

        format_syslog_row(row)
      end

      result.compact!
    end

    # Split / Parse Time
    # TODO: Better sys log parsing? Cannot find ready-made regex/ruby parser
    def format_syslog_row(row)
      month, day, time, device, service, message = row.split(' ', 6)
      service.gsub!(':', '')
      pid = service.match(/\[(.*?)\]/)&.captures&.first

      {
        time: format_time_parse("#{month} #{day} #{time}"),
        device:,
        service:,
        message:,
        pid:
      }

    # Return everything incase of error
    rescue StandardError => e
      LogBot.warn('SysLog Parse', e.message)
      {
        message: row
      }
    end
  end
end
