# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # K8s logs seem to have a lot of non-json entries
    # Extract all json, ignore everything else
    # ==========================================================================
    def format_kube_json
      self.result = raw.map do |row|
        # Skip all non-json
        next unless row.first == '{'

        result = begin
          Oj.load row
        rescue EncodingError
          puts
          next
        end

        # Parsing Time
        format_time(result)

        result
      rescue StandardError => e
        # TODO: Background Logger?
        e.message
        LogBot.warn('JSON Parse', e.message)
        next
      end.compact

      :ok
    end

    # =========================================================================
  end
end
