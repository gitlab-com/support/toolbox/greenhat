# Top
module GreenHat
  # Log
  module Formatters
    # Remove Comments / Empty Lines
    def format_gitlab_status
      final = {}
      raw.each do |row|
        list = row.split('; ').map do |entry|
          status, service, pid_uptime = entry.split(': ')

          {
            status:,
            name: service,
            pid_uptime: pid_uptime.chomp
          }
        end

        final[list.first.name] = list
      end

      self.result = final
    end
  end
  # ----------------------------------------
end
