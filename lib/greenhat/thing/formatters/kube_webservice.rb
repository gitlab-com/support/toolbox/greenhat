# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # Gitlab Tail Formatter
    # ==========================================================================
    def format_kube_webservice
      # Revert to raw for cats
      self.kind = :raw

      output = {}
      current_log = nil

      raw.each do |line|
        next if line.blank?

        if line.include? '*** '
          current_log = /\*\*\* (.+?) \*\*\*/.match(line).captures.first
        else
          output[current_log] ||= [] unless current_log.nil?
          output[current_log].push line unless current_log.nil?
        end
      end

      # Remove Empty Entries
      output.reject { |_k, v| v.empty? }

      # Root Dir
      root_dir = "#{$TMP}/#{name}"
      Dir.mkdir(root_dir)

      # Write Files / Create Things
      output.each do |k, v|
        file_name = k.gsub('/var/log/gitlab/', '')

        dir = "#{root_dir}/gitlab-rails"
        FileUtils.mkdir_p(dir)

        File.write("#{root_dir}/#{file_name}", v.join("\n"))

        # Thing Setup
        archive.things_create(file: "#{root_dir}/#{file_name}").setup
      end

      # Link
      self.result = raw
    end
    # ==========================================================================
  end
end
