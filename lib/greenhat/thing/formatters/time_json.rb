# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # Time Space JSON
    # 2021-05-04_18:29:28.66542 {"timestamp":"2021-05-04T18:29:28.665Z","pid":3435539,"message":"Use Ctrl-C to stop"}
    # ==========================================================================
    def format_time_json
      self.result = raw.map do |row|
        time, msg = row.split(' ', 2)

        # Be mo fault tolerant
        result =  begin
          Oj.load msg
        rescue StandardError
          { msg: }
        end

        result.time = time

        result
      rescue StandardError => e
        # TODO: Background Logger?
        e.message
        LogBot.warn('Time JSON Parse', e.message)
        next
      end
    end
    # ==========================================================================
  end
end
