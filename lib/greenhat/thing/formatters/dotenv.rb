# Top
module GreenHat
  # Log
  module Formatters
    # Formatters for Dmesg
    def format_dotenv
      self.result = Dotenv::Parser.new(raw_full.join("\n")).call
    end
  end
end
