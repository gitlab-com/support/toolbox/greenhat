# Top
module GreenHat
  # Log
  module Formatters
    # Remove Comments / Empty Lines
    def format_clean_raw
      staging = raw_full.clone

      # Empty
      staging.reject!(&:empty?)

      # Commented
      staging.reject! { |x| x =~ /#.*$/ }

      self.result = if staging.empty?
                      raw
                    else
                      staging
                    end
    end
  end
  # ----------------------------------------
end
