# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # Formatters
    # ==========================================================================
    # Primarily for gitlab-rails/api_json.log
    def format_api_json
      self.result = raw.map do |row|
        # Seeing regular edge cases where there is random new non-json
        # at the beginning of a log / Attempt to save first entry
        if row[0] != '{'
          # Nothing to Escape Parse
          next unless row.include?('{')

          row = "{#{row.split('{', 2).last}"
        end

        result = Oj.load row

        # Parsing Time
        format_time(result)

        result
      rescue StandardError => e
        # TODO: Background Logger?
        LogBot.warn('[API JSON] JSON Parse', "#{e.message}: #{row.inspect}")
        next
      end.compact

      :ok
    end

    def flatten_hash(param, prefix = nil)
      param.each_pair.reduce({}) do |a, (k, v)|
        v.is_a?(Hash) ? a.merge(flatten_hash(v, "#{prefix}#{k}.")) : a.merge("#{prefix}#{k}".to_sym => v)
      end
    end

    # ==========================================================================
  end
end
