# Top
module GreenHat
  # Log
  module Formatters
    # Attempt to make runner logs easier to parse
    def format_runner_log
      self.result = raw.map do |row|
        next if row.empty? || row == "\n"
        next if row.include? '-- Logs begin at' # Skip Header

        process_runner_log_row(row)
      end

      result.compact!
    end

    # Split / Parse Time
    # TODO: Better sys log parsing? Cannot find ready-made regex/ruby parser
    def process_runner_log_row(row)
      month, day, time, device, service, message = row.split(' ', 6)
      service&.gsub!(':', '')
      pid = service&.match(/\[(.*?)\]/)&.captures&.first

      entry = {
        time: format_time_parse("#{month} #{day} #{time}"),
        device:,
        service:,
        message:,
        pid:,
        row: # Insert full row for different timestamp formats
      }

      # Remove Empty Keys
      entry.compact!

      # Shellwords split - Skip Blank
      process_runner_row_message(message, entry) unless message.blank?

      entry

    # Return everything incase of error
    rescue StandardError => e
      LogBot.warn('Runner Log Parse', e.message)
      {
        message: row
      }
    end

    def process_runner_row_message(message, entry)
      # Insert Keywords / Format Time if Possible
      Shellwords.split(message).select { |x| x.include? '=' }.each do |x|
        key, value = x.split('=', 2)

        # Don't overwrite stuff
        case key
        when 'time'
          entry["#{key}-msg".to_sym] = format_time_parse value
        when 'message', :message
          entry["#{key}-msg".to_sym] = value
        else
          entry[key.to_sym] = value
        end
      end
    rescue StandardError
      # LogBot.debug('Runner Log Parse', e.message)
      nil
    end
    # --
  end
end
