# Top
module GreenHat
  # Log
  module Formatters
    # Formatters for bracket logs (dmesg, sos)
    def format_bracket_log
      self.result = raw.map do |row|
        next if row.empty? || row == "\n"

        result = {}
        time, output = row.split(']', 2)
        result[:time] = Time.parse time.split('[', 2).last.strip

        if output.include? ': '
          category, raw = output.split(': ', 2)
          result[:category] = category.strip

          result.merge! dmesg_split(raw) if raw.include? '='

          result[:message] = raw.strip
        else
          result[:message] = output
        end

        result
      rescue StandardError => e
        # TODO: Background logger
        LogBot.warn('dmesg', "Unable to Parse, #{row}:#{e.message}")
      end

      self.result.compact!
    end

    def dmesg_split(raw)
      Shellwords.split(raw).each_with_object({}) do |x, h|
        key, value = x.split('=')
        next if value.nil?

        h[key] = value.numeric? ? value.to_f : value
        h[key] = 0.0 if h[key].numeric? && h[key].zero?
      end
    end
  end
end
