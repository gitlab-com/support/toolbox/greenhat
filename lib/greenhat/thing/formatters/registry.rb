# Top
module GreenHat
  # Log
  module Formatters
    # ==========================================================================
    # Registry Split
    # ==========================================================================
    # https://docs.docker.com/registry/configuration/#log
    # Formatters text, json
    # registry['log_formatter'] = "json"
    # TODO: Logstash (Not working in 14.3)

    def format_time_registry
      self.result = raw.map do |row|
        result = row[0] == '{' ? registry_json(row) : registry_shell_words(row)

        # Timestamp Parsing
        result.ts = Time.parse result.ts if result.key? 'ts'
        result.time = Time.parse(result.time)

        result
      end
    end

    def registry_shell_words(row)
      time, msg = row.split(' ', 2)

      output = Shellwords.split(msg).each_with_object({}) do |x, h|
        key, value = x.split('=')
        next if value.nil?

        h[key.to_sym] = value.numeric? ? value.to_f : value
      end

      output[:time] = time

      output
    end

    def registry_json(msg)
      Oj.load(msg)
    rescue EncodingError
      { message: msg }
    end
    # ==========================================================================
  end
end
