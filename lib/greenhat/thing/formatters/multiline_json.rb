# Top
module GreenHat
  # Log
  module Formatters
    # Formatters for single json blobs in entire file
    def format_multiline_json
      self.result = Oj.load raw_full.join
    end
  end
end
