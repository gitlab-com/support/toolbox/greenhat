module GreenHat
  # Info Formatter
  module Spinner
    def spin_start(title)
      # Ignore when Quiet is Set
      return true if Cli.quiet

      @spin_time = Time.now
      @spinner = TTY::Spinner.new(
        "#{time.pastel(:bright_black)} - [:spinner] :title", hide_cursor: true, success_mark: '✔'.pastel(:green)
      )
      @spinner.update(title:)

      # Don't Auto spin when debug output is happening
      @spinner.auto_spin unless ENV['DEBUG']
    end

    def spin_done
      # Ignore when Quiet is Set
      return true if Cli.quiet

      title = @spinner.tokens[:title]
      spin_end = humanize(@spin_time)

      title_update = if spin_end.blank?
                       title
                     else
                       "#{title} (#{spin_end.pastel(:blue)})"
                     end

      @spinner.update(title: title_update)
      @spinner.success
    end

    def time
      Time.now.strftime('%I:%M:%S').pastel(:bright_black)
    end

    # Replace TimeDifference with https://stackoverflow.com/a/4136485/1678507
    def humanize(time)
      miliseconds = (Time.now - time) * 1000

      [[1000, :ms], [60, :s], [60, :m], [24, :h]].map do |count, name|
        next unless miliseconds.positive?

        miliseconds, n = miliseconds.divmod(count)

        "#{n.to_i}#{name}" unless n.to_i.zero?
      end.compact.reverse.join(' ')
    end
  end
end
