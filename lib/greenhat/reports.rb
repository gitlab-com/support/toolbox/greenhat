require 'greenhat'
require 'greenhat/reports/internal_methods'
require 'greenhat/reports/helpers'
require 'greenhat/reports/runner'

GreenHat::Cli.quiet!

# GreenHat Namespace
module GreenHat
  # Reports Parent Class
  module Reports
    # Method Storing
    def self.store
      @store ||= []

      @store
    end

    def self.output
      @output ||= []
    end

    # Adding Methods
    def self.add(*params)
      store.push params
    end

    # Main Entrypoint for Triggering Reports Run
    def self.run!
      _list, flags, args = Args.parse(ARGV)

      output = Archive.all.map do |archive|
        runner = GreenHat::Reports::Runner.new(
          archive:,
          store: store.clone,
          flags:,
          args:
        )

        runner.run!

        break if flags.help # Only Run Once

        runner
      end

      # Check for Pagination
      flags[:page] = ENV['GREENHAT_PAGE'] == 'true' if ENV['GREENHAT_PAGE']

      ShellHelper.show(output.flat_map(&:output), flags)
    end

    def self.archive_run(archive)
      store.each do |entry|
        method = entry[0]
        # Skip anything for Shim'd Puts
        if method == :puts
          send(method, *entry[1..])
        else
          send(method, archive, *entry[1..])
        end
      end
    rescue StandardError => e
      puts e.message
      puts e.backtrace
    end
    # ==
  end
end

at_exit do
  Dir.mktmpdir('greenhat-sauce') do |tmp|
    $TMP = tmp
    $STDOUT_CLONE = $stdout.clone
    ENV['GREENHAT_REPORT'] = 'true' # Flag to allow skipping of different entypoints
    GreenHat::Entrypoint.start(ARGV, false, false)
    GreenHat::Reports.run!
  end
end
