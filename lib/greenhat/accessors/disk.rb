module GreenHat
  # Sidekiq Log Helpers
  module Disk
    def self.data
      df.map(&:data).flatten.compact
    end

    # Get Max Size
    def self.max_padding(disks, key = :mounted_on)
      list = disks.map do |x|
        x.slice(key)
      end

      list.map(&:values).flatten.max_by(&:size).size + 5
    end

    def self.padding(disks, keys = %i[mounted_on size used avail])
      keys.map do |key|
        max_padding(disks, key)
      end
    end

    def self.df(args = {})
      things = Thing.where(name: 'df_hT')

      # Host / Archive
      things.select! { |x| x.archive? args.archive } if args.archive

      things
    end

    def self.report_output(data, filter = %w[tmpfs loop])
      output = []

      disks = data.sort_by { |x| x.use.to_i }.reverse
      disks.reject! { |x| filter.any? { |y| x.filesystem.include? y } }
      pad_mount, pad_size, pad_used, pad_avail = GreenHat::Disk.padding(disks)

      output << [
        'Mount'.ljust(pad_mount).pastel(:blue),
        'Size'.ljust(pad_size).pastel(:magenta),
        'Used'.ljust(pad_used).pastel(:cyan),
        'Avail'.ljust(pad_avail).pastel(:white),
        '% Use'.ljust(pad_avail).pastel(:green)
      ].join

      disks.map do |disk|
        # Safety
        next if disk.nil? || disk.use.nil?

        # Pretty Disk Use
        use = [
          disk.use.rjust(5).ljust(5).pastel(:green),
          '  ['.pastel(:blue),
          ('=' * (disk.use.to_i / 2)).pastel(:green),
          ' ' * (50 - (disk.use.to_i / 2)),
          ']'.pastel(:blue)
        ].join

        # Whole Thing
        output << [
          disk.mounted_on.ljust(pad_mount).pastel(:blue),
          disk[:size].to_s.ljust(pad_size).pastel(:magenta),
          disk.used.to_s.ljust(pad_used).pastel(:cyan),
          disk.avail.to_s.ljust(pad_avail).pastel(:white),
          use
        ].join
      end

      output.join("\n  ")
    end

    # Unified Output Handler
    def self.format_output(file, name = false, limit = nil, filter = %w[tmpfs loop])
      output = []

      output << file.friendly_name if name

      # Reject TMPFS
      disks = file.data.sort_by { |x| x.use.to_i }.reverse

      # Filter
      disks.reject! { |x| filter.any? { |y| x.filesystem.include? y } }

      disks = disks[0..limit - 1] if limit

      pad_mount, pad_size, pad_used, pad_avail = GreenHat::Disk.padding(disks)

      # Headers
      output << [
        'Mount'.ljust(pad_mount).pastel(:blue),
        'Size'.ljust(pad_size).pastel(:magenta),
        'Used'.ljust(pad_used).pastel(:cyan),
        'Avail'.ljust(pad_avail).pastel(:white),
        '% Use'.ljust(pad_avail).pastel(:green)
      ].join

      # Table Summary
      disks.map do |disk|
        # Pretty Disk Use
        use = [
          disk.use.rjust(5).ljust(5).pastel(:green),
          '  ['.pastel(:blue),
          ('=' * (disk.use.to_i / 2)).pastel(:green),
          ' ' * (50 - (disk.use.to_i / 2)),
          ']'.pastel(:blue)
        ].join

        # Whole Thing
        output << [
          disk.mounted_on.ljust(pad_mount).pastel(:blue),
          disk[:size].to_s.ljust(pad_size).pastel(:magenta),
          disk.used.to_s.ljust(pad_used).pastel(:cyan),
          disk.avail.to_s.ljust(pad_avail).pastel(:white),
          use
        ].join
      end

      output
    end

    # Unified Output Handler
    def self.markdown_format(data, _name = false, limit = nil, filter = %w[tmpfs loop])
      output = []

      # Reject TMPFS
      disks = data.sort_by { |x| x.use.to_i }.reverse

      # Filter
      disks.reject! { |x| filter.any? { |y| x.filesystem.include? y } }

      disks = disks[0..limit - 1] if limit

      pad_mount, pad_size, pad_used, pad_avail = GreenHat::Disk.padding(disks)

      # Headers
      output << [
        'Mount'.ljust(pad_mount),
        'Size'.ljust(pad_size),
        'Used'.ljust(pad_used),
        'Avail'.ljust(pad_avail),
        '% Use'.ljust(pad_avail)
      ].join

      # Table Summary
      disks.map do |disk|
        # Whole Thing
        output << [
          disk.mounted_on.ljust(pad_mount),
          disk[:size].to_s.ljust(pad_size),
          disk.used.to_s.ljust(pad_used),
          disk.avail.to_s.ljust(pad_avail),
          disk.use.rjust(5).ljust(5)
        ].join
      end

      output.join("\n")
    end
    # =
  end
end
