module GreenHat
  # Sidekiq Log Helpers
  module Sidekiq
    def self.fast_stats
      things.each do |thing|
        puts `fast-stats #{thing.file}`
      end

      :ok!
    end

    def self.logs
      @logs ||= things.map(&:data).flatten.compact
    end

    def self.raw
      @raw ||= things.map(&:raw).flatten.compact
    end

    def self.things
      Thing.where(name: 'sidekiq_current')
    end

    def self.errors
      logs.select { |x| x.severity == 'ERROR' }
    end

    def self.pages
      show logs
    end

    def self.queue_duration(data = nil)
      data ||= logs
      data.select { |x| x.key? :enqueued_at }.each do |row|
        next if row.key? :queue_duration

        row[:queue_duration] = row.enqueued_at - row.created_at
      end
    end
  end
end
