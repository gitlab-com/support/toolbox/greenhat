module GreenHat
  # Pretending to do grep stuffs
  module Grep
    def self.help
      puts "\u2500".pastel(:cyan) * 20
      puts "#{'Grep'.pastel(:yellow)} - gimme... GIMME!"
      puts 'General text searching through all files (logs, parsed, and raw)'
      puts "\u2500".pastel(:cyan) * 20

      puts 'Usage'.pastel(:blue)
      puts '  <search term> <optional: files>'.pastel(:green)
      puts '  Examples:'
      puts '    grep external_url'
      puts '    grep external_url gitlab.rb'
      puts '    grep "spaced terms"'
      puts '    grep "System clock" timedatectl'
      puts '    grep "search in two files" nfsiostat nfsstat'
      puts
    end

    def self.grep(raw)
      files, flags, _args = Args.parse(raw)

      if flags.help
        help
        return
      end

      # Search Term
      term = files.shift

      things = find_things(files)

      # Renderer
      output = []
      things.each do |thing|
        results = thing.grep(term)

        next if results.empty?

        output.push("#{thing.friendly_name} #{results.count.to_s.pastel(:bright_black)}")
        output.concat results
        output.push ''
      end

      # Output
      if output.empty?
        puts 'No results'.pastel(:red)
        return
      end

      ShellHelper.show GreenHat::Paper.new(data: output, flags:).render
    end

    def self.find_things(files = [])
      if files.empty?
        Thing.all
      else
        Thing.all.select { |x| files.any? { |f| x.name.include? f } }
      end
    end

    # ------------------
  end
  # ------------------
end
