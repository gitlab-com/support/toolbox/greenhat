module GreenHat
  # Sidekiq Log Helpers
  module Ps
    def self.ps(args)
      things = Thing.where(name: 'ps')

      # Host / Archive
      things.select! { |x| x.archive? args.archive } if args.archive

      things
    end

    def self.things
      Thing.where(name: 'ps')
    end
  end
end
