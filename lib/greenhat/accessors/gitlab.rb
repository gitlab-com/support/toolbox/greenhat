module GreenHat
  # GitLab App Helpers
  module GitLab
    def self.node_types
      [
        {
          name: 'Web Service', pattern: %w[puma unicorn]
        },
        {
          name: 'Sidekiq', pattern: %w[sidekiq]
        },
        {
          name: 'Gitaly', pattern: %w[gitaly]
        },
        {
          name: 'Redis', pattern: %w[redis]
        },
        {
          name: 'PostgreSQL', pattern: %w[postgresql]
        },
        {
          name: 'PGBouncer', pattern: %w[pgbouncer]
        }
      ]
    end

    def self.identify_node(archive)
      gitlab_status = archive.things.find { |x| x.name == 'gitlab_status' }&.data&.keys
      hostname = archive.things.find { |x| x.type == 'hostname' }.data.first.chomp

      {
        host: hostname,
        services: gitlab_status || []
      }
    end

    # Show GitLab Services in a grid / include versions
    # rubocop:disable Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity
    def self.services(archive, indent = 0)
      manifest = archive.things.find { |x| x.type == 'gitlab/version-manifest.json' }
      gitlab_status = archive.things.find { |x| x.name == 'gitlab_status' }

      # Dont' show invalid
      return nil if gitlab_status.data.to_s.include? "Could not find 'bundler'"
      return nil unless gitlab_status

      list = gitlab_status.data.keys.sort.map do |service|
        color = gitlab_status.data.dig(service, 0, :status) == 'run' ? :green : :red
        status = gitlab_status.data.dig(service, 0, :status) == 'run' ? '↑' : '↓'

        # Collect Service version from manifest
        version = manifest.data.software[service.to_sym]&.display_version

        # If able to identify version use / fallback
        if version
          [
            "#{service}#{status}".pastel(color),
            "(#{version})".pastel(:bright_black)
          ].join(' ')
        else
          service.pastel(color)
        end
      end

      # Keep Alphabetical Sort
      groups = list.each_slice((list.size / 3.to_f).round).to_a

      table = TTY::Table.new do |t|
        loop do
          break if groups.all?(&:empty?)

          t << groups.map(&:shift)
        end
      end

      table.render(:unicode, padding: [0, 1, 0, 1], indent:)
    end
    # rubocop:enable Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity

    def self.services_markdown(archive)
      # manifest = archive.things.find { |x| x.type == 'gitlab/version-manifest.json' }
      gitlab_status = archive.things.find { |x| x.name == 'gitlab_status' }

      list = gitlab_status.data.keys.sort.map do |service|
        status = gitlab_status.data.dig(service, 0, :status) == 'run' ? '↑' : '↓'

        "#{service}#{status}"
      end

      # Keep Alphabetical Sort
      groups = list.each_slice((list.size / 3.to_f).round).to_a

      table = TTY::Table.new do |t|
        loop do
          break if groups.all?(&:empty?)

          t << groups.map(&:shift)
        end
      end

      "```\n#{table.render(:basic)}\n```"
    end

    # ==
  end
end
