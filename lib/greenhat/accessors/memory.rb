module GreenHat
  # Sidekiq Log Helpers
  module Memory
    def self.free
      Thing.where(name: 'free_m')
    end

    def self.percentage(used, total)
      return 0 if used.to_i.zero? || total.to_i.zero?

      # Show at least one bar if below 1%
      [1, ((used.to_i / total.to_f).round(1) * 100).round].max
    end

    def self.memory_row(mem)
      total = mem.total.to_i
      total_name = number_to_human_size(total.megabytes)
      used_name = number_to_human_size(mem.used.to_i.megabytes)

      bar = [
        '|'.pastel(:green) * percentage(mem.used, total),
        '|'.pastel(:blue) * percentage(mem.shared, total),
        '|'.pastel(:cyan) * percentage(mem.buffcache, total),
        ' ' * percentage(mem.free, total)
      ].join

      # Make Even
      padding = 125 - bar.unpastel.size
      bar += ' ' * padding if padding.positive?

      [
        mem.kind.ljust(4).pastel(:cyan),
        ' ['.pastel(:bright_black),
        bar.ljust(120),
        used_name.pastel(:magenta),
        ' / '.pastel(:bright_black),
        total_name.pastel(:blue),
        ']'.pastel(:bright_black)
      ].join
    end

    def self.number_to_human_size(num)
      ActiveSupport::NumberHelper.number_to_human_size num
    end
  end
end
