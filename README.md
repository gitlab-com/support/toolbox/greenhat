# GreenHat

Experimental SOS and Log Parser for GitLab

12.x and above only (due to structured logging)

# Requirements

`bsdtar` - Used to extract archives

### Ubuntu

`apt-get install libarchive-tools`

Ruby 3.1 or later, including the headers (such as package `ruby-dev`)

## Installation

```
gem install greenhat
```

## Usage

```
greenhat sos-archive.tar.gz
greenhat production_json.log
# launches a console
>> help # the program is self-documented through the builtin help command
```

[Watch an introductory tutorial](https://www.youtube.com/watch?v=xyVQiKFgeP4) (recorded March 5, 2024, with Green Hat v0.7.3) 

## Testing

```
bundle exec rake
```

## Release Process

- Update CHANGELOG
- Increment `version.rb`
- Create MR
- Merge to `main`

```
# Build and Push
bundle exec rake build
gem push pkg/greenhat-1.x.x.gem
```

## Dev Troubleshooting

```
docker run -it -v ${PWD}:/app ruby:3.1.4 bash
```

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/gitlab-com/support/toolbox/greenhat. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Limitations

- Shell Helpers cannot use Camel Case

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
