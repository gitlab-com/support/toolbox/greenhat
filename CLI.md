# CLI Usage

| Hotkey              | Description             |
| ------------------- | ----------------------- |
| Ctrl + U            | Clear                   |
| Ctrl + A            | Go to beginning         |
| Ctrl + E            | Go to End               |
| Ctrl + Left/Right   | Move left/right by word |
| Ctrl + D, Ctrl + Z  | Exit                    |
| Ctrl + C, Shift Tab | Up one module           |
