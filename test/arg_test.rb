require File.expand_path 'test_helper.rb', __dir__

class ArgParseTest < Minitest::Test
  # Command with Arg=Value + File
  def test_arg_value_files
    cmd = Shellwords.split '--status=500 --slice=exception.message gitlab-rails/production_json.log'
    files, flags, args = GreenHat::Args.parse(cmd)

    # Files
    assert files.include?('gitlab-rails/production_json.log')
    assert_kind_of Array, files

    # Args
    assert_kind_of Array, args
    assert(args.all? { |x| x.instance_of?(Hash) })

    status = args.first

    assert_equal status.field, :status
    assert_equal status.value, 500
    assert_kind_of Integer, status.value
    assert_equal status.bang, false

    # Flags
    assert_kind_of Hash, flags

    assert_kind_of Array, flags[:slice]
    assert Symbol, flags[:slice].first
    assert_equal :'exception.message', flags[:slice].first
  end

  def test_arg_multiple_value_files
    cmd = Shellwords.split '--duration=3 --slice=one,two sidekiq/current'
    files, flags, args = GreenHat::Args.parse(cmd)

    # Files
    assert files.include?('sidekiq/current')
    assert_kind_of Array, files

    # Args
    assert_kind_of Array, args
    assert(args.all? { |x| x.instance_of?(Hash) })

    duration = args.first

    assert_equal duration.field, :duration
    assert_equal duration.value, 3
    assert_kind_of Integer, duration.value
    assert_equal duration.bang, false

    # Flags
    assert_kind_of Hash, flags

    assert_kind_of Array, flags[:slice]
    assert Symbol, flags[:slice].first
    assert_equal %i[one two], flags[:slice]
  end

  def test_arg_bang_string_and_flag
    cmd = Shellwords.split '--severity!=INFO --exact sidekiq/current'
    files, flags, args = GreenHat::Args.parse(cmd)

    # Files
    assert files.include?('sidekiq/current')
    assert_kind_of Array, files

    # Args
    assert_kind_of Array, args
    assert(args.all? { |x| x.instance_of?(Hash) })

    severity = args.first

    assert_equal severity.field, :severity
    assert_kind_of String, severity.value
    assert_equal severity.value, 'INFO'
    assert_equal severity.bang, true

    # Flag Exact
    assert_kind_of Hash, flags
    assert flags.exact
  end

  def test_special_args_and_default_flag
    cmd = Shellwords.split '--slice=ua gitlab-rails/api_json.log --uniq=ua --truncate'
    files, flags, args = GreenHat::Args.parse(cmd)

    # Files
    assert files.include?('gitlab-rails/api_json.log')
    assert_kind_of Array, files

    # Args
    assert_kind_of Array, args
    assert(args.all? { |x| x.instance_of?(Hash) })

    # No Args here
    assert_kind_of Array, args
    assert_empty args

    # Flags
    assert_kind_of Hash, flags

    # Slice
    assert_kind_of Array, flags[:slice]
    assert Symbol, flags[:slice].first
    assert_equal :ua, flags[:slice].first

    # Uniq
    assert_kind_of Array, flags[:uniq]
    assert Symbol, flags[:uniq].first
    assert_equal :ua, flags[:uniq].first
  end

  def test_no_files_non_special_arg_and_non_default_flags
    cmd = Shellwords.split '--path=api --truncate=0 --limit=4 --round=3 gitlab-rails/application_json.log'
    files, flags, args = GreenHat::Args.parse(cmd)

    # Files
    assert files.include?('gitlab-rails/application_json.log')
    assert_kind_of Array, files

    # Args
    assert_kind_of Array, args
    assert(args.all? { |x| x.instance_of?(Hash) })

    path = args.first

    assert_equal path.field, :path
    assert_kind_of String, path.value
    assert_equal path.value, 'api'
    assert_equal path.bang, false

    # Flag Exact
    assert_kind_of Hash, flags

    assert_kind_of Integer, flags.truncate
    assert Integer, flags.truncate
    assert_equal 0, flags.truncate

    assert_kind_of Integer, flags.round
    assert Integer, flags.round
    assert_equal 3, flags.round

    assert_kind_of Integer, flags.limit
    assert Integer, flags.limit
    assert_equal 4, flags.limit
  end

  def test_logic_or
    cmd = Shellwords.split '--or'
    _files, flags, _args = GreenHat::Args.parse(cmd)

    # --or Flag
    assert_kind_of Hash, flags
    assert_equal flags.logic, :any?

    # Defaults
    _files, flags, _args = GreenHat::Args.parse([])
    assert_kind_of Hash, flags
    assert_equal flags.logic, :all?
  end

  def test_archive_flag
    # Singular
    cmd = Shellwords.split '--archive=first'
    _files, flags, _args = GreenHat::Args.parse(cmd)

    assert_kind_of Hash, flags
    assert flags.key?(:archive)
    assert_kind_of Array, flags.archive
    assert_equal :first, flags.archive.first

    # Comma
    cmd = Shellwords.split '--archive=first,second'
    _files, flags, _args = GreenHat::Args.parse(cmd)
    assert_kind_of Hash, flags
    assert flags.key?(:archive)
    assert_kind_of Array, flags.archive
    assert_equal %i[first second], flags.archive
  end

  def test_multiple_of_same_argument
    cmd = Shellwords.split '--path!=john --path!=bob'
    files, flags, args = GreenHat::Args.parse(cmd)

    # Files
    assert_empty files

    # Args
    assert_kind_of Array, args
    assert(args.all? { |x| x.instance_of?(Hash) })

    john, bob = args

    assert_equal john.field, :path
    assert_kind_of String, john.value
    assert_equal john.value, 'john'
    assert_equal john.bang, true

    assert_equal bob.field, :path
    assert_kind_of String, bob.value
    assert_equal bob.value, 'bob'
    assert_equal bob.bang, true

    # Flag Exact
    assert_kind_of Hash, flags
  end
end
