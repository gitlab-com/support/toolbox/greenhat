require 'pry'

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'minitest/reporters'
require 'minitest/autorun'

Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new(color: true), Minitest::Reporters::JUnitReporter.new]

require File.expand_path("#{File.dirname(__FILE__)}/../lib/greenhat")
