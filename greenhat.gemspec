# lib = File.expand_path('lib', __dir__)
# $LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

$LOAD_PATH.push File.expand_path('lib', __dir__)
require 'greenhat/version'

Gem::Specification.new do |spec|
  spec.name          = 'greenhat'
  spec.version       = GreenHat::VERSION
  spec.authors       = ['Davin Walker']
  spec.email         = ['dwalker@gitlab.com']

  spec.summary       = 'GitLab SOS Tool'
  spec.description   = 'Experimental SOS and Log Parser for GitLab'
  spec.homepage      = 'https://gitlab.com/gitlab-com/support/toolbox/greenhat'
  spec.license       = 'MIT'

  spec.files = Dir['{bin,lib}/**/*', 'LICENSE', 'README.md']
  spec.executables   = %w[greenhat]

  spec.require_paths = ['lib']
  spec.required_ruby_version = '>= 3'
  spec.add_runtime_dependency 'actionview', '~> 6.1'
  spec.add_runtime_dependency 'activesupport', '~> 6.1'
  spec.add_runtime_dependency 'amazing_print', '~> 1.3'
  spec.add_runtime_dependency 'chartkick', '~> 4.1'
  spec.add_runtime_dependency 'did_you_mean', '~> 1.6'
  spec.add_runtime_dependency 'dotenv', '~> 2.7'
  spec.add_runtime_dependency 'gitlab_chronic_duration', '~> 0.10.6'
  spec.add_runtime_dependency 'hash_dot', '~> 2.5'
  spec.add_runtime_dependency 'oj', '~> 3.11'
  spec.add_runtime_dependency 'pastel', '~> 0.8'
  spec.add_runtime_dependency 'pry', '~> 0.14'
  spec.add_runtime_dependency 'puma', '~> 5.6'
  spec.add_runtime_dependency 'require_all', '~> 3.0'
  spec.add_runtime_dependency 'ruby_native_statistics', '~> 0.10'
  spec.add_runtime_dependency 'semantic', '~> 1.6'
  spec.add_runtime_dependency 'sinatra', '~> 2.2'
  spec.add_runtime_dependency 'slim', '~> 4.1'
  spec.add_runtime_dependency 'teron', '~> 0.0.8'
  spec.add_runtime_dependency 'tty-cursor', '~> 0.7'
  spec.add_runtime_dependency 'tty-pager', '~> 0.14'
  spec.add_runtime_dependency 'tty-platform', '~> 0.3'
  spec.add_runtime_dependency 'tty-progressbar', '~> 0.18'
  spec.add_runtime_dependency 'tty-prompt', '~> 0.23'
  spec.add_runtime_dependency 'tty-reader', '~> 0.9'
  spec.add_runtime_dependency 'tty-spinner', '~> 0.9'
  spec.add_runtime_dependency 'tty-table', '~> 0.12'
  spec.add_runtime_dependency 'tty-which', '~> 0.5'
  spec.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
