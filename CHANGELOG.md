[[_TOC_]]

# 0.7.4

- Fixy janky results -- Skip null entries
- Rework printing of fields and field auto complete (use screen width to be more dynamic). Also make it work and not crash

## Report

- Add Elasticsearch to full and errors reports
- Fix(add) archive flag selectors

## New File Types

- Add `gitlab_system_status` file Type

# 0.7.3

- Make `report` a default top level command that will default to the full report.
- add something for database migrations
- gitlab metadata files / skip auto reconfigure / skip db?
- Additional Raw File Types (license, user_uid, bmtp_size, ssh_config)
- Fixes for Oj successfully parsing integers (things that aren't really json)
- Clean up on consistent warnings (compact / skipping integers)
- Updated Filter Help / Clarification around table_style
- Some improvement to the parsers (slower, less noise, ensure retention)
- Geo Logs included in reports

---

# 0.7.2

## General

- Array / Reports fix for rendering tables when there isn't any output

## Log

### Table Styles

- Line breaks between basic output
- Shortcut for `--table_style` flag. Defaults to `:basic` output with just the flag

```
--table_style=basic
--table_style
```

## Motd

- Fix errors when files are missing

---

# 0.7.1

- Remove `gitlab-shell.log` from Kubesos from it inadvertently being labeled raw rather than json

## Reports

- Add Pages errors to both `errors` and `full` reports.

## Motd

- Initial Helper for showing summary / useful information at the beginning of the shell
- Showing GitLab Version
- Indicate if the faststats and gitlab/gitlab.rb are included

---

# 0.7.0

## New File Types

All Raw for Now

- elastic_info, fast-stats/api, fast-stats/gitaly, fast-stats/production, fast-stats/sidekiq, non_analyzed_tables, schema_dump_result, log/mail.log

# General

- Bump to 2.7.7
- Gems Update
- Ignore Nil disks exception

# Log

- Removed old search, Use `--text=`
- Added `stats_limit` to help further filter down `stats` results.
- Timezone names to Uppercase (mst => MST)

# 0.6.7

- Skip symlinks when loading new files

---

# 0.6.6

## Grep

- Fix issues with non-processed files

# Reports

New reports that collect percentiles for the duration/ms fields for `production_json`, `sidekiq/current`, and `gitaly/current`

- sidekiq_duration
- production_log_duration
- gitaly_duration

---

# 0.6.5

## Shell/CLI

- Added a root-level `grep` command for searching across all files

## Reports

- Errors: Include Praefect Errors

## File Types

- gitlab/gitlab.rb (as raw for now)

---

# 0.6.4

- Avoid `xdg-open` on Mac. Better handling on URL opening. Show URL and return.

## General Requirements Update

- Goal: keep in sync with GitLab itself. Since I backed off of using the new async libraries I think we can put this back to using ruby 2.7x.
- Update Gems
- Add Sinatra Gem for Web components

## Log

### Raw File Types

- `ifconfig`
- `ip_address`
- `top_cpu`
- `top_res`
- `gitlab-kas/config`
- `mailroom/config`
- `pressure_cpu.txt`
- `pressure_io.txt`
- `pressure_mem.txt`

## Reports / Query

- Introducing a `query_if_exists` that will skip the block if no files were detected to query
- Add raw command args as a field in the runner

- Full Report, do not show 0 error counts for sections where the sos doesn't include data. E.g. Only show Praefect/Gitaly counts on those specific nodes

## Errors Report

Default to filter with extra args. Pivot via new `raw_args`

`errors --filter=gitaly,sidekiq` or `errors gitaly sidekiq`

# 0.6.3

- Typify `rpm_verify` as raw, see
  [gitlabsos!68](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos/-/merge_requests/68)
- File Type ifconfig/ip_address

# Shell

- Add root `ip_address` and `ifconfig` to cat new files
- Add `gitlab_rb` for new `gitlab/gitlab.rb` files

## Disk

- Fix padding around volume mount names

## Log

- Fix Total -- Continued Clean Up of Shell Helpers

## Reports

- Add Disks to Full Report
- Tab auto complete for report names!

## GitLab

- Fix unexpected new lines one ctl status
- Remove comma / replace with newline

---

# 0.6.2

## Log

- Add query used to create results in `save` and `write`
- Add type for consul/identify_primary_db_node
- Add type for general exporters. web_exporter gitlab-exporter/current

## Reports

- Errors: Add Gitaly and fix verbose / count flags

## Housekeeping

- Clean up ShellHelpers from new printer output class

---

# 0.6.0, 0.6.1

- Update CLI entrypoint to use new reports module. `--report`, `--report=errors`
- Remove `reports-full`, `markdown`
- Typify `gitlab-kas/current`
- Archive Zip: Fix issues where greenhat could loop, not properly extract a .zip

## Shell

- Root level commands default to `log` submodule.
- If Log command is issued send to log submodule
- E.g.'api' will run a `log` `api`.

## Process

- Copy over `log` function, improved help, query

## Query

- Add `to_i` to `transform` options
- `sort` to
- `combine` flag now additionally adds a `src` to the entries

## Reportable

- Fix issue with missing block on `query`
- Cleaner output on Errors report (don't show 0 results results)

## Log

- Improve `time_json` parser to be more fault tolerant (specifically on errors from `puma/current`)

## Reporting

Introducing Reporting Framework!

Framework to extend and improve pre-built reports. Make it easy to write custom reports. Allow execution of reports directly (skipping GreenHat Shell).

See [Reports](reportable.md) for more details

### Report Examples

[Errors](lib/greenhat/reports/reports/errors.rb)

- Built-in `help`
- Arg Parsing `--filter`
- Flag `--verbose` for additional error details
- Log Queries

[Full](lib/greenhat/reports/reports/full.rb)

- Rewrite of Original Full Report
- Files (hostname, uname, lscpu)
- Structured Files (`gitlab/version-manifest.json`, `gitlab_status`)
- Log Querying for stats / error counts

[Faststats](lib/greenhat/reports/reports/faststats.rb)

- Flags (top,errors) `--top`, `--errors`, and `--all`
- Arguments for file filters `--filter`
- Built-in `help`
- FastStats helper

### Highlights

- Custom reports (for within GreenHat's Shell) can be added to user's home directory `~/.greenhat/reports`
- Ruby Scripts to run report directly against input args

```
#!/usr/bin/env ruby
require 'greenhat/reports'

query_format 'gitlab-rails/api_json.log --path="/api/v4/jobs/request" --stats=ua'
```

- Report DSL
- Arg Parsing
- Custom / Built-in Help `--help`
- Take advantage of GreenHat Built-ins
  - Output Formatting (Tables, Hashes, Colorizing)
  - Log Querying. All the goodness (filtering, slicing, stats, percentile, interval, and etc)
  - File reading (`cat`, `info`, for SOS items like `df`, `hostname`)
  - Simple execution for multiple Files / archive / directories
- Output Helpers (`gitlab_version`, `percent`, `human_size_to_number`, `pastel`)
- Entrypoint `--report/-r` now accepts an argument and params for reports. E.g. `--report=full`, `--reports=faststats --top`

## FastStats

- Allow for attempting all files within Archive/Things with `*`
- `ls -a` now includes other files (which can also be run)
- Enable fuzzy file matches (gitaly => gitaly/current)

## Output / Pagination

- Adding a `Paper` class which handles rendering of output to the shell
- Allow for background processing to make pagination and scrolling much much faster
- Simplify code and consolidate formatters into a single class / modules

## Log

- Fix output render for `--total` and `--combine`
- `--total`: Don't show `duration` if empty
- Clarify Text for saving within memory and writing output to disk (`save` and `write`)
- Fix for duplicate file type prompts of the same type
- Add gitlab-runner.log to syslog parsing
- etc/fstab - Fix bug with Clean Raw using row by row reading
- Change AutoComplete/Tab to be context specific (only show logs for logs). Revert previous allow all parsing (prevent exceptions on non-log files)
- Added `percentile` helper for numeric output
- Added `interval` helper to breakdown output
- Normalize Query output (no afterward manipulation)
- Expand `total` to support `simple` param to only return count
- Remove 'total' on total title entries
- Add `transform` to copy key to another key (interval timestamp => time)

```
# Gitlab-runner output
┌──────────────────────────────────────────────────────────────────────────────────────┬──────────┐
│ repo_url                                                                             │ 1557     │
│ https://gitlab/root/pipeline-test-no-only.git                                        │ 17 1%    │
│ https://gitlab/root/always-py.git                                                    │ 18 1%    │
│ None                                                                                 │ 1467 94% │
└──────────────────────────────────────────────────────────────────────────────────────┴──────────┘

greenhat ~/log » --name="runner-gitlab" --slice=msg,time --total
gitlab-runner gitlab-runner.log 28 20m 14s
┌──────────┬───────────────────────────┐
│ total    │ 28                        │
│ duration │ 20m 14s                   │
│ start    │ 2022-03-04 17:41:10 -0700 │
│ end      │ 2022-03-04 18:01:24 -0700 │
└──────────┴───────────────────────────┘
```

---

# 0.5.1

- Humanize Time only for InstanceOf Time objects
- Save to parsable / log query
- Update `df_h` to `df_hT` (updated GitLabSOS)

---

# 0.5.0

- Interactive / Menu for settings
- Add specific `quit` command exit shell
- GC Runtime settings (more tuning needed)
- Support for zip/bz2 files
- Ignore raw types in filter
- `filter_help` centralized and paginated
- Code organization 'Query' file/class
- Performance improvements for reading files (eachline + not storing in memory)
- Remove recursive JSON parsing

## Log

- Time Parsing changed from `>/<` to `>=/=<` to allow for exact matches
- Filter Help uses index by default now (no more puts blocks)

---

# 0.4.0

## Ruby 3

Bump to Ruby 3

## General

- Add `--load-local`,`-ll` for pulling logs on gitlab instances
- Add `--no-prompt` `-n` to skip prompting of new file types
- Add Setting for `assume_raw`
- Code Organization (Entrypoint vs Cli)
- Tar output to base file path. This allows recursive archive decompress
- Allow pulling in directories
- Don't clear screen on errors
- Line breaks for basic tables
- Add support for `tgz` files
- Fix broken queries after saving results (missing type)

## Log

- Fix for duration calculation when `pluck` is used

---

# 0.3.6

## General

- New `--no-color` to suppress color output entry point flag
- New `--full-report` which includes fast-stats errors and top

## GitLab

- Add ↓↑ to the services to match markdown's report to indicate if a service is reporting up or down
- Change from ! => ↓

## Log

- New Search Logic `>= <=` for integer comparisons (technically strings too)
- Experimental `|` pipe to shell logic

- Added warning if no files were found in query
- Added `nginx/access.log`
- Improved NGINX Parsing
- Fixed Stats bug where truncate was breaking sorting
- Fixed bug with multiple archives where autocomplete duplicated matches
- Fuzzy file match now also checks against file type
- New `save` and `write` methods for adding additional searchable objects from query as well as writing results to a file
- Add results duration (start/end) to archive/log/total count
- Added Duration Start/End table format to `--total`

## Examples

- List of unique Gitaly error messages
- Workhorse Duration/Path using new integer logic

### File Types

- gitlab_socket_stats, unicorn_stats (older envs)

## Report

- ZD/Markdown move GitLab version to the top
- Warn if on CE
- Fix bug on internal search for multiple archives

Other internal changes

- Using internal search for both workhorse and application log. Helpers for thing finders, easier to read code.

# Web

- Added sinatra/web views for charting / analysis (:4567)
- Can be started with `--web` or `web` at the root
- Requires at least fast-stats 0.7.3

## Time Query

- Form / Interface for running `log` queries and displaying them in an area and column chart.
- Split by field
- Avg by field

## Charts / Dashboards

- Gitaly, Sidekiq, API, Production, Workhorse
- Show top / over time, percentile analysis

# 0.3.5

## Report

- Fixes for report where not all values are present or invalid

## Log

- Add raw for `geo-logcursor/current`

# 0.3.4

- Initial Support for KubeSOS (mostly just raw loading)

## Report

- `markdown_report` for Zendesk Friendly markdown
- Added exceptions(exceptions log), and gitaly to report error counts
- Better error filtering for application log (ignore leases)

## Log

- Improved Registry parsing
- Lets Encrypt Pattern
- `filter_help` partial match/filtering
- Internal helper for running queries

# 0.3.3

- Fix stats rounding

# 0.3.2

- Friendly errors if fast stats doesn't exist
- Add `--help` example for `--command=` like args
- Results deep cloning (potential performance impact)
- Thing default field extraction (.map(&:keys))

## Log

- Autocomplete for filter options `--tex` => `--text`
- Autocomplete for field field values. `--sev` => `--severity`. `--stats=sev` => `--stats=severity`
- Add `time_zone` for manipulating time into a specific time zone
- Add `table_style` for changing which renderer is used by tty-table
- Add `--fields` to just print available fields
- Make `limit` and `pluck` compatible. `limit` flag logic before `pluck` flattening.
- Help for `--text`
- Clarified help for `--raw`
- Stats: Use truncate for key names

# 0.3.1

- Report
  - raw, archive options
- Nginx

  - Access Log Parsing (Primary,Pages, and Registry)

- Other FileTypes
  - mailroom/current

# 0.3.0

## General

- Add `--command`/`-c` for running GreenHat Shell commands and exiting

- String Color

  - Remove Colorize in favor of Pastel (GPL vs MIT)
  - String / GreenHat patches, allow setting to disable color output

- Add/Expand command line `--help`
- Quiet: Hush `--quiet` on
  - archive loading messages
  - LogBot output. Fatal/debug messages
  - Toggle quiet on/off from top level `quiet`
- Typo Fixes
- Add top-level Help
  - First iteration short explanations
  - Include CLI Shortcuts
- Improved Process help / examples
- version/about Shell helpers

- Clean up other help commands

  - Improved `cat` help
  - Improved `ls` help (Files, e.g. `cat` & `log`)
  - Improved `process` help

- Rewrite of Argument Parsing

  - Code organization
  - Flags / Arguments
  - Better type casting
  - Less Looping / Edge case string substitution
  - Unified file searching

- Dedicated List module for `ls`

  - Allow for grep / pattern filter/listing

- If file is json parsable assume 'json' file type

- File Matching defaults to `fuzzy`/'includes'. Added `fuzzy_file_match` setting.

## Report

- Disks usage color formatting
- Include service versions where possible
- Assume Quiet with report

## Log

- Add `exists` to check if field is present
- Add `exact` flag (partial vs exact matches)
- Uniq: Respect multiples / comma split entries
- Page: Update check to included boxes (increasing sensitivity)
- Limit: Without value use screen height respecting boxes
- Help updates
  - Document `case`, `exact`,
  - Improve/clarify `limit`, `truncate` and `or`
- Show total count in archive / file title
- Don't show files that have zero entries

### Stats

- Support for multiple
- Percentage
- Support Nil => None

## Bugs

- FastStats default not parsing args correctly
- Fix issue with default settings overriding explicit values
- Fix rubocop yaml
- Fix `cat` `show` not properly using formatted output

## GitLab

- New `services` and `architecture`
- Services: Parse `gitlab-ctl status` and include software versions where possible
- Architecture: Initial identify node and show roles (Ideal with multiple SOS)

## FastStats

- Better argument support
- Expanded help examples

---

# 0.2.0

## General

- Introduce User Settings
  - Adding settings in `.greenhat/settings.json`
  - `page`, `round`, `truncate` as possible user settings
- Process
  - Filtering and Table formatting
- Disk archive filtering

## Log

### Options

- Add `page` as a filter option to specifically enable or disable paging.
- Adding `truncate` to limit field output. Dramatically speeds up printed output. Indicated with `...`
- Add `case` for filters to be case sensitive. Default to insensitive searches

### Formatting

- Multiline output for formatted values
- Colorized `severity` fields and time stamps
- Fix/Shim for failed table renders (max screen width issues)
- Use `awesome inspect` rather than colorizing Integers directly

## Other

- Improved pagination
  - Dedicated colorizer / outside of the renderer
  - Code organization for formatting and paging (moving out of helper)
  - Improve screen width / column detection / fallback to inspect less
- Updated rubocop rules Skip :true due to symbol parameters

## Bugs

- Fix / Improved Syslog Parsing (prevent skipping/ignoring entries)
- Super annoying bug where history gets last line doubled up
- Remove invalid commands for fast-stats

## Report

- Rescue missing sys_time
- Include Disk and Memory

---

# 0.1.4

- Release Notes
- Warning on no or missing files
- Move greenhat local files to `.greenhat` directory. Introduce Settings module
- Improve File type handling. Include raw/json Options
- Add initial feedback when loading archives (particularly large ones)
- FastStats
  - Add errors subcommand
  - Support all `--` arguments / pass directly to fast-stats
- Log
  - Expand `--round` to allow more precision. `--round=2` by default
  - Warn on invalid arguments. e.g. `pluck`, `slice`, `uniq`
  - Add `--start` and `--end` for Time filtering (Using `Time.parse` and the `time` field)
  - Add `--sort` for field sorting
  - Add `--reverse` to reverse all result output
  - Add `--combine` to ignore archive identifiers / combine all results (sorting/time selection)
  - Add `filter_examples` help method in `log`.
- Archive filter for `cat` and `log` with `--archive`
- Fix bug where `*.s` files weren't properly manipulated and decompressed
- Added `gitlab-ctl tail` formatter
- Better pagination detection (checking total size)
- Added `gitlab-ctl-tail.log` and `gitlab-rails/git_json.log` file types
- Initial `local_load` for running GreenHat directly on instances

---

# 0.1.3

- Additional file types for exporter configs
- Missing `did_you_mean` gem dependency

---

# 0.1.2

Binary/Command

- Added `-l`/`--load` to auto read files (rather than on demand)

Other

- Gitaly Logs have some fallback to shellwords (gitaly-ruby)
- Fallback for JSON parse to attach to 'message'
- Fallback if file cannot be read (binary / non text)
- Thing Process - Continue even if parsing fails
- Additional Linting / Code clean up

CLI / Navigation

- Ctrl-C + BackTab to submodule down shortcut
- Fix backtab not clearing line
- Add some notes in CLI.md for keyboard shortcuts
- Added Debug / ENV Toggle

Log Filtering

- Added Stats (`--stats=field`) for counting occurrences
- Round Integers (3)
- `ls` now default to uniq files `--all`/`-a`
- Filter is now log's default command. Meaning you can just put in log files to start
- Fix field rejection / clean up param parse
- Make variables unique
- Allow Raw `--raw` to prevent paging
- Added `--json` to for printing output into json formatting
- Added `--pluck` for printing single key output
- Added `--uniq` for limiting output by unique values in key

Parsing

- Better Time Parsing
  - Support for Epoc Timestamps
  - Recursive Filtering

History

- File doesn't match existing patters? Will keep it in the history remembering what you selected for it for two weeks

---

# 0.1.1

- Move all gem dependencies to gemspec
- Remove TimeDifference (Active Support Conflict)
- Update Spinner
  - use colorize instead of pastel
  - custom humanize time difference
  - remove colorize from gemspec
- Update FastStats list to properly use files
- About output on no files supplied

---

# 0.1.0

Initial Release
