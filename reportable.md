Reporting API with the goal to simplify and allow the creation of user-defined / custom reports.

- Custom reports accessible within GreenHat (`greenhat ~/reports » demo`)
- External to GreenHat Shell executable Scripts (`./report sos.tar.gz`)

[[_TOC_]]

# Example

```
#!/usr/bin/env ruby
require 'greenhat/reports'

quiet!
archive_header
cat 'hostname' do |data|
 "#{'Hostname:'.pastel(:cyan)} #{data.join}"
end

info 'gitlab/version-manifest.json' do |data|
  "GitLab Version: #{data.build_version}"
end

query 'gitlab-workhorse/current --level=error' do |data|
  color = data.count.zero? ? :green : :red
  "Workhorse Errors: #{data.count.to_s.pastel(color)}"
end
```

Example Output:

```
 ./example.rb gitlabsos.tar.gz centos8_sos.tar.gz
gitlabsos.tar.gz
Hostname: gitlab
GitLab Version: 13.11.3
Workhorse Errors: 0

centos8_sos.tar
Hostname: wc-rails.internal
GitLab Version: 13.11.3
Workhorse Errors: 18
```

# Getting Started

You can run reports in either within GreenHat, as an a executable ruby script, or with the `--report` CLI arg.

## Ruby Script

Scripts will need to be marked as executable and have the following header.

```#!/usr/bin/env ruby
#!/usr/bin/env ruby
require 'greenhat/reports'
```

This will pull in the reporting helpers / DSL. Afterwhich you can execute your script with any SOS/logs as arguments. For example take this `version.rb` (`chmod +x`)

```
#!/usr/bin/env ruby
require 'greenhat/reports'

info 'gitlab/version-manifest.json' do |data|
  puts "GitLab Version: #{data.build_version}"
end
```

Run against one or many archives

```
./version.rb gitlabsos.tar.gz
GitLab Version: 13.11.3
```

## Reports within GreenHat

Reports added GreenHat's `~/.greenhat/reports` directory will be available for execution within the `reports` submodule in the GreenHat Shell. For example `~/.greenhat/reports/sweet_report.rb`

```
greenhat archive.tar.gz
=> reports sweet_report
```

You can also use the command args to directly run these

```
greenhat -q --command="reports demo" archive.tar.gz
```

Warning! Reports within the home directory `~/.greenhat/reports` should not have the `require 'greenhat/reports'` as this will cause duplicates during runtime

## GreenHat CLI Argument

Run custom report

```
greenhat archive.tar.gz --report=demo
```

Run default `full` report

```
greenhat archive.tar.gz --report
```

# Reference

## Settings

| Method     | Params                          | Description                                                                                                                                       |
| ---------- | ------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| no_prompt! | -                               | Limit output / Enable CLI Quiet (--no-prompt)                                                                                                     |
| quiet!     | -                               | Disable loading / parsing output                                                                                                                  |
| page       | Bool: true/false, Default: True | Explicitly Disable/Enable Pagination. Without being set it will defer to automatic detection. If just `page` is passed, it will enable pagination |

## Output Helpers

| Method               | Params                                                       | Description                                                  |
| -------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| header               | Text: String/Symbol<br />Color: Symbol, default: :bright_yellow | Default header output helper `puts 'OS'.pastel(:bold, :bright_yellow) |
| archive_header       | -                                                            | Helper to Break / Show Archive                               |
| cat                  | Filename: String -- Print Output: Boolean, default: true -- Block: [data] | Return raw file read from the archive                        |
| info                 | Filename: String -- Print Output: Boolean, default: true -- Block: [data] | Return attempted parsed file from the archive                |
| show                 | Paper Render                                                 | Helper to join/print values                                  |
| puts                 | Text: String/Symbol (Shim of `puts`)                         | Helper so that output is inline with collected/parsed output |
| indent               | Text: String, Indent Value: Integer, default: 2              | Helper to make indentation easier                            |
| percent              | Value: Integer, Total: Value                                 | Helper to return percent of value                            |
| human_size_to_number | Value: String                                                | Numeric value for human text (1 kB => 123123)                |
| ljust                | Text: String, Justify: Integer, Color: Optional Pastel       | Left Justify Helper / With Color!                            |
| query                | Query: String. GreenHat Shell Log Query -- Print Output: Boolean, default: true -- Block: [data] | Run Log query and return results as Data                     |
| query_format         | Same as `query` except assume `format` on show, and assume no block means vanilla return | Helper to print query results                                |
| query_if_exists      | Same as `query` but do not default files, skip block if no files were found to Query | If any files query helper                                    |
| help                 | Block                                                        | Block to be called if `--help` is called                     |
| args_select          | Symbol                                                       | Filter for Args (--filter=value,value). Return Array         |
| gitlab_version       | -                                                            | Print GitLab Version                                         |
| faststats            | Query: String, Sub Command: String `faststats('gitaly/current', 'top')` | Helper for FastStats Module                                  |

## Color / Printing

[Pastel](https://github.com/piotrmurach/pastel) is used for color output.

With ruby itself you can use `puts`/`print`

```
puts "OS".pastel(:bold, :bright_yellow)
```

## Block Variables

Within the `cat` and `info` blocks the `archive` variable will give access to everything from that host. `archive.things` will be everything parsed/loaded from that host. You can use this to get other file information from the archive.

```archive.thing?('df_h').data
archive.thing?('df_h').data
```

## Args / Flags

Both inside of blocks and outside of blocks there are variables/methods called `args` and `flags` these can be used to pivot and pass ARGV arguments into a report

```
# ./example sos.tar.gz --sidekiq
flags[:sidekiq] => true
flags.sidekiq => true
```

```
# ./example sos.tar.gz --sidekiq=error
args => [{:field=>:sidekiq, :value=>"error", :bang=>false, :logic=>:include?}]

# ./example sos.tar.gz --sidekiq!=5
args => [{:field=>:sidekiq, :value=>5, :bang=>true, :logic=>:include?}]

# ./example sos.tar.gz -s='dew it'
args => [{:field=>:s, :value=>"dew it", :bang=>false, :logic=>:include?}]
```

# Tips

You can add a `binding.pry` within you block to debug or view data within context

```ruby
binding.pry
```

## Pastel Colors

- `black`
- `red`
- `green`
- `yellow`
- `blue`
- `magenta`
- `cyan`
- `white`
- `bright_black`
- `bright_red`
- `bright_green`
- `bright_yellow`
- `bright_blue`
- `bright_magenta`
- `bright_cyan`
- `bright_white`
